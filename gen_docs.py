"""Generate markdown pages of the individual modules in this repository."""

from lazydocs import generate_docs      # needs pip install lazydocs

# The parameters of this function correspond to the CLI options
generate_docs(
    ["src/optoboard_felix/driver", "src/optoboard_felix/register_maps", "src/optoboard_felix/additional_driver", "src/optoboard_felix/InitOpto"],
    output_path="../optoboard-system-docs/docs/software/optoboard_felix",       # to your local folder of the optoboard system docs directory
    src_base_url="https://gitlab.cern.ch/bat/optoboard_felix/-/tree/main/",        # change for specific branch
    overview_file="index",
    watermark=True
    )
