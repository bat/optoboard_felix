import json
import argparse

parser = argparse.ArgumentParser(description='Create a light version of the configuration file')
parser.add_argument('-c', '--config_name', type=str, help='Name of the configuration file', default="src/optoboard_felix/configs/30000000_test_lpgbtv1_vtrxv1_3.json")
args = parser.parse_args()

config_name = args.config_name 
with open(config_name, 'r') as f:
    config = json.load(f)

power_up_registers = sorted(list(set(config["power_up_registers_master"]) | set(config["power_up_registers_slave"])))
clock_registers = ["EPCLK5CHNCNTRH", "EPCLK5CHNCNTRL", "EPCLK2CHNCNTRH", "EPCLK2CHNCNTRL", "EPCLK26CHNCNTRH", "EPCLK26CHNCNTRL", "EPCLK19CHNCNTRH", "EPCLK19CHNCNTRL"]
i2c_master_registers = ["I2CM0CONFIG", "I2CM1CONFIG", "I2CM2CONFIG"]

unique_registers = sorted(list(set(power_up_registers) | set(clock_registers) | set(i2c_master_registers)))

config_light = {}
try:
    config_light["Optoboard"] = config["Optoboard"]
except:
    print("No Optoboard key in this configuration file!")
for x in ["power_up_registers_master", "power_up_registers_slave", "lpgbt", "gbcr", "vtrx"]:
    if x in ["power_up_registers_master", "power_up_registers_slave", "gbcr"]:
        config_light[x] = config[x]
    elif x == "vtrx":
        continue
        # config_light[x] = {"GCR": config[x]["GCR"]}
    else:
        config_light["lpgbt"] = {}
        for x in unique_registers:
            config_light["lpgbt"][x] = config["lpgbt"][x]
        

# Function to serialize lists without spaces between elements
def list_encoder(lst):
    return '[' + ','.join(map(str, lst)) + ']'


with open(config_name.split("vtrx")[0] + "default.json", 'w') as f:
    # json.dumps(config_light, separators=(',', ':'), default=list_separator, indent=4)
    # json.dump(config_light, f, indent=4)
    json.dump(config_light, f, separators=(',', ':'), default=lambda obj: list_encoder(obj) if isinstance(obj, list) else obj, indent=4)
