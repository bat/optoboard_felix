# creates a JSON config file with all writeable registers from the lpgbt_control_lib, vtrx_register_maps repos
# author: Roman Mueller

import json
import sys
import inspect

from optoboard_felix.register_maps import lpgbt_register_map_v1, lpgbt_register_map_v0
from optoboard_felix.register_maps import vtrx_register_map_v1_2, vtrx_register_map_v1_3
from optoboard_felix.register_maps import gbcr2_register_map, gbcr3_register_map

# lpgbt_reg_map = lpgbt_register_map_v0.LpgbtRegisterMapV0 
# first_read_only_lpgbt_register = 0x140    
lpgbt_reg_map = lpgbt_register_map_v1.LpgbtRegisterMapV1
first_read_only_lpgbt_register = 0x150

# vtrx_reg_map = vtrx_register_map_v1_2.VTRxplusRegisterMapV1_2	# quad LD v1.2
vtrx_reg_map = vtrx_register_map_v1_3.VTRxplusRegisterMapV1_3	# quad LD v1.3

# gbcr_register_map = gbcr2_register_map.GBCRRegisterMap
gbcr_register_map = gbcr3_register_map.GBCRRegisterMap


config = {}
config['lpgbt'] = {}

for register in dir(lpgbt_reg_map):

	if any(map(register.__contains__, ['Reg', '__'])):
		continue

	address = getattr(getattr(lpgbt_reg_map, register), 'address')

	if address < first_read_only_lpgbt_register and not 'RESERVED' in register:

		config['lpgbt'][register] = {}

		for setting in inspect.getmembers(getattr(lpgbt_reg_map, register)):

			# print(setting[0])

			if inspect.isclass(setting[1]) and not '__' in setting[0]:
				config['lpgbt'][register][setting[0]] = [0, 0, 0, 0]

config['gbcr'] = {}

for register in dir(gbcr_register_map):

	if any(map(register.__contains__, ['Reg', '__'])):
		continue

	address = getattr(getattr(gbcr_register_map, register), 'address')

	config['gbcr'][register] = {}

	for setting in inspect.getmembers(getattr(gbcr_register_map, register)):

		if inspect.isclass(setting[1]) and not '__' in setting[0]:

			default = getattr(getattr(getattr(gbcr_register_map, register), setting[0]), 'default')

			config['gbcr'][register][setting[0]] = [default, default, default, default]


config['vtrx'] = {}

for register in dir(vtrx_reg_map):

	if any(map(register.__contains__, ['Reg', '__'])):
		continue

	address = getattr(getattr(vtrx_reg_map, register), 'address')

	config['vtrx'][register] = {}

	for setting in inspect.getmembers(getattr(vtrx_reg_map, register)):

		# print(setting[0])

		if inspect.isclass(setting[1]) and not '__' in setting[0]:

			if hasattr(setting[1], 'default'):
				config['vtrx'][register][setting[0]] = getattr(getattr(getattr(vtrx_reg_map, register), setting[0]), 'default')



with open('my_new_config.json', 'w') as f:
	json.dump(config, f, indent=4)

print("Config file my_new_config.json created with:")
print(lpgbt_reg_map)
print(gbcr_register_map)
print(vtrx_reg_map)
