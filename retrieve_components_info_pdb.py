import json
import itkdb
import sys

accesscode_1 = sys.argv[1]
accesscode_2 = sys.argv[2]


def retrieve_info(client):
    list_OB = client.get('listComponents', json={"filterMap": {"project": "P", 'componentType': ["OPTOBOARD"], "state" : "ready" },
                                         #   "includeProperties":"true",
                                            "outputType": "full", 
                                            "pageInfo": {
                                            "pageIndex": 0,
                                            "pageSize": 1000000
                                                }} )
    dict_OB = {}
    not_good_OB = []
    for OB in list_OB:
        # pprint(OB)
        atlas_serial_number = OB['serialNumber'].split("20UPGOB")[-1]
        # optoboard_version = OB['properties']['OPTOBOARD_VERSION_NUMBER']
        ####
        properties = OB['properties']
        json_properties = {}
        try:
            for item in properties:
                json_properties[item['code']] = item['value']
            properties = json_properties 
            gbcr_version = properties['GBCR_VERSION_NUMBER']
            efused = properties['EFUSED']
            lpgbt_version = properties['LPGBT_VERSION']
            i2cmaster = properties['I2C_MASTER']
            lpgbt_master_address = properties['LPGBT_MASTER_ADDRESS']
            ####
            optoboard_version = properties["OPTOBOARD_VERSION_NUMBER"]
            optoboard_version = optoboard_version.split(".")[0].split("V")[-1]
            if optoboard_version == "5":
                optoboard_version = OB['type']["name"]
                optoboard_version = optoboard_version.split(" ")[-1].split("_")[0].split(".")[0][-1]
            lhep_serial_number = properties["LHEP_SERIAL"]
            lhep_serial_number = lhep_serial_number.split("20UPGOB")[-1]
        ####
            lpgbt_position= [0,0,0,0]
            gbcr_position= [0,0,0,0]
            children = OB['children']
            at_least_one_gbcr = False
            for child in children:
                if child['componentType'] == "5ecd057d9985df000a06f103" and child["component"] != None: #LpGBT
                    lpgbt_position_curr = child["order"]
                    lpgbt_position[lpgbt_position_curr] = 1
                if child['componentType'] == "60febff7c614f1000aec962f" and child["component"] != None: #GBCR
                    at_least_one_gbcr = True
                    gbcr_position_curr = child["order"]
                    gbcr_position[gbcr_position_curr] = 1
            ####
            if at_least_one_gbcr == False:
                gbcr_position = lpgbt_position
            dict_OB[lhep_serial_number] = {"optoboard_v": int(optoboard_version),
                                    "lpgbt_v": int(lpgbt_version),
                                    "lpgbt_master_addr": int(lpgbt_master_address,16),
                                    "lpgbt1": int(lpgbt_position[0]),
                                    "lpgbt2": int(lpgbt_position[1]),
                                    "lpgbt3": int(lpgbt_position[2]),
                                    "lpgbt4": int(lpgbt_position[3]),
                                    "gbcr_v": int(gbcr_version),
                                    "gbcr1": int(gbcr_position[0]),
                                    "gbcr2": int(gbcr_position[1]),
                                    "gbcr3": int(gbcr_position[2]),
                                    "gbcr4": int(gbcr_position[3]),
                                    "efused": int(efused),
                                    "I2C_master": int(i2cmaster)
                                    }
            print("The optoboard with serial number ",atlas_serial_number," was correctly fetched from the database.")
        except:
            print("The optoboard with serial number ",atlas_serial_number," was NOT correctly fetched from the database.")
            not_good_OB.append(atlas_serial_number)
    return dict_OB, not_good_OB


def create_json_file(data,filename="",filepath=""):
    with open(filepath+filename, 'w') as f:
        json.dump(data, f, indent=4)


def add_end_to_dict_OB(dict_OB):
    dict_OB["lpgbt2"]={
        "component_type": "lpgbt",
        "dev_addr": 0x75
    }
    dict_OB["lpgbt3"]={
        "component_type": "lpgbt",
        "dev_addr": 0x76
    }
    dict_OB["lpgbt4"]={
        "component_type": "lpgbt",
        "dev_addr": 0x77
    }
    dict_OB["gbcr1"]={
        "component_type": "gbcr",
        "dev_addr": 0x20
    }
    dict_OB["gbcr2"]={
        "component_type": "gbcr",
        "dev_addr": 0x21
    }
    dict_OB["gbcr3"]={
        "component_type": "gbcr",
        "dev_addr": 0x22
    }
    dict_OB["gbcr4"]={
        "component_type": "gbcr",
        "dev_addr": 0x23
    }
    dict_OB["vtrx"]={
        "component_type": "vtrx",
        "dev_addr": 0x50
    }
    return dict_OB


def create_client(accesscode_1 = "", accesscode_2 = ""):
    if accesscode_1 == "":
        accesscode_1 = input("Enter accesscode_1... \n")
        print("Access code 1 is : " + accesscode_1)
    if accesscode_2 == "":
        accesscode_2 = input("Enter accesscode_2...\n")
        print("Access code 2 is : " + accesscode_2)
    try:
        user = itkdb.core.User(access_code1=accesscode_1, access_code2=accesscode_2)
        print("The client has been created")
        return itkdb.Client(user=user)
    except:
        raise ValueError("The client could not be created. Please check the access codes.")


client = create_client(accesscode_1=accesscode_1,accesscode_2=accesscode_2)
dict_OB, not_good_OB = retrieve_info(client=client)
print("The following optobaords were correctly fetched from the itkpdb (" + str(len(dict_OB)) + ") : " + str(dict_OB.keys()))
print("The following optobaords were not fetched from the itkpdb (" + str(len(not_good_OB)) + ") : " + str(not_good_OB))
if len(dict_OB) < len(not_good_OB):
    raise ValueError("There was probably an error in the communication with the database. The number of good components is lower than of bad components")
if len(not_good_OB) > 20:
    raise ValueError("There was probably an error in the communication with the database. The number of bad components is bigger than 20...")

dict_OB = add_end_to_dict_OB(dict_OB)
create_json_file(data=dict_OB,filename="components.json",filepath="src/optoboard_felix/driver/")
