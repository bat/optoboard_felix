# Most recent changes
- The parameter "woflxcore" of the script InitOpto.py has been substituted with "commToolName" (string) that accepts "flpgbtconf", "flpgbtconfAPI", "ic-over-netio", "itk-ic-over-netio-next" or "lpgbt-com" as values.
- Communication via felixStar is now supported in the containerized version of this software! The FELIX "bus" folder needs to be mounted in the optoboard-worker to /bus.
- The optoboard_felix software is now a python package. The old version has been tagged as [0.0.0](https://gitlab.cern.ch/bat/optoboard_felix/-/tree/0.0.0?ref_type=tags) and is still available, but won't be supported.

# Optoboard FELIX Configuration Interface
A configuration tool for Optoboards.

This is the backend to the [ITk demonstrator software](https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw) but can be used stand-alone.

**Supports**:
- Optoboard V1.1 (lpGBTv0, GBCRv2)
- Optoboard V2.0 (lpGBTv0, GBCRv2)
- Optoboard V2.1 (lpGBTv1, GBCRv2)
- Optoboard V3.0 (lpGBTv1, GBCRv2/GBCRv3)
- Optoboard V4.0 (lpGBTv1, GBCRv2/GBCRv3)
(all with VTRx+ quad laser driver version 1.2 (on VTRx+ V10) or 1.3)

Please check our documentation: [https://optoboard-system.docs.cern.ch](https://optoboard-system.docs.cern.ch/). Support requests through our [Bern-Optoboard Mattermost](https://mattermost.web.cern.ch/bat-optoboard) (sign up [here](https://mattermost.web.cern.ch/signup_user_complete/?id=778hfkdwfjd5mm899qhs8m9m5w)).

### Prerequisites
Everything hardware related or software that is needed for running a full data transmission setup is summarised in [our documentation](https://optoboard-system.docs.cern.ch/software/). The dependency installation is handled using the `pyproject.toml` file.

# How to configure/use an Optoboard
**The suggested way to use the `optoboard_felix` software is via its containerized version: follow the instructions in [https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-optoboard](https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-optoboard).** It is also possible to locally install the `optoboard_felix` software using the following instructions.
1. Install the package from the Package Registry:
```bash
pip install optoboard-felix --index-url https://gitlab.cern.ch/api/v4/projects/113584/packages/pypi/simple --extra-index-url https://gitlab.cern.ch/api/v4/groups/33370/-/packages/pypi/simple
``` 
2. Set up your FELIX environment, configure FELIX card and check link alignment
3. Run `felixcore` in a separate terminal or `felixStar`
4. After the installation of the optoboard_felix package, the `InitOpto` command is available (more on how to use it later). Notice that one has to specify the communication tool (`ic-over-netio` <-> `felixcore`, `lpgbt-com`, `itk-ic-over-netio-next` <-> `felixStar`) to be used for the IC communication when creating instances of the `Optoboard` class with `InitOpto`

## Configure through CLI

The InitOpto command is a shim for the InitOpto.py script.

```bash
InitOpto [-h]
         [-P/--optoListPath OPTOLISTPATH]
         [-p/--config_path CONFIG_PATH]
         -s/--optoboard_serial OPTOBOARD_SERIAL
         [-v/--vtrx_v {1.2,1.3}]
         -G/--flx_G {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23}
         --flx_cid FLX_CID , if not used, FLX_D must be given
         [-d/--flx_d FLX_D]
         [-T/--commToolName {"flpgbtconf","flpgbtconfAPI", "ic-over-netio","itk-ic-over-netio-next", "lpgbt-com"}]
         [--woflxcore {0,1}]
         [-c/--configure {0,1}]
         [-D/--debug {0,1}]
         [-t/--test_mode {0,1}]
         [--configInDB {0,1}]
         [--CONFIGDB_ADDRESS CONFIGDB_ADDRESS]
         [--runkey runkey_name]
         [-i]
```

For an explanation on the arguments, run `InitOpto -h` or consult the [API](https://optoboard-system.docs.cern.ch/software/optoboard_felix/InitOpto/). Notice that the `-i` argument is used to start an interactive iPython session. If the user wants to operate multiple optoboards in a single session, use the "optoListPath" to point to your personalized configuration file for all the optoboards (follow the standard defined in `src/optoboard_felix/configs/optoConfigList_example.json`).  

### Examples
Minimal example to configure a full Optoboard using default configuration files and using the Python session afterwards:

```python
InitOpto --optoboard_serial 4400072 --vtrx_v 1.3 --flx_G 0 --flx_cid 0 -i --configure 1 
>>> opto.lpgbt1.check_PUSM_status()
>>> opto.lpgbt1.swap_rx_polarity(0) # invert rx polarity for channel 0 (possible values: 0 -> 5)
>>> opto.lpgbt1.swap_tx_polarity(0) # invert tx polarity for channel 0 (possible values: 0 -> 7)
```

or by providing a config like `2400001_test_lpgbtv1_vtrxv1_2.json`:

```bash
InitOpto --config_path configs/2400001_test_lpgbtv1_vtrxv1_2.json --configure 1 -i
```

Notice that if one uses the `config_path` argument, then `optoboard_serial`, `vtrx_v`, `flx_G` and `flx_cid` **must be specified inside the configuration file** by adding a key-value pair of the type:

```python
    "Optoboard":
    {
        "serial": "2400001",
        "flx_G": 0,
        "flx_cid": 0,
        "vtrx_v": "1.2"
    },

```

Remove the `-i` flag if you don't want to start the Python interactive shell and just configure.

Consult our [optoboard_felix API](https://optoboard-system.docs.cern.ch/software/optoboard_felix/) in the Optoboard system docs for available methods and more information.

## Config files

The script has default configs according to the lpGBT and VTRx+ quad laser driver version, **please do not change them**. If the user wants to provide its own config please **duplicate the default ones according to your lpGBT and VTRx+ quad laser driver version and change values there**. Note that the format of the provided file has to follow the one of `2400001_test_lpgbtv1_vtrxv1_2` and `3200030_test_lpgbtv1_vtrxv1_3`, which differs from the default. Also note that different chip versions have different register maps.

The configuration of the fields of the registers of the ASICs of the same type on the Optoboard is stored in lists. An example is reported. The first value stored in the list refers to the ASIC denoted as 1 (example: `lpgbt1`), the 4th to the ASIC denoted as 4 (example: `lpgbt4`). These values can be adjusted independently.

```
"EPRX1CONTROL": {
    "EPRX10ENABLE": [1, 1, 1, 1],
    "EPRX11ENABLE": [0, 0, 0, 0],
    "EPRX12ENABLE": [0, 0, 0, 0],
    "EPRX13ENABLE": [0, 0, 0, 0],
    "EPRX1DATARATE": [3, 3, 3, 3],
    "EPRX1TRACKMODE": [2, 2, 2, 2]
},
```

## Config files from the ConfigDB
If the user is using the ConfigDB, they have to make sure the runkey is in the configDB storage. Then the user can call the runkey with InitOpto and the argument --runkey runkey_name


## Create multiple optoboard instances

Multiple optoboard instances can be initialized at once by providing a json following the model of `optoConfigList_example.json` to `InitOpto`:

```bash
InitOpto --optoListPath optoConfigList_example.json -i
```

Another option is shown in the minimal example below:

```python
from collections import OrderedDict

from driver.Optoboard import Optoboard, load_custom_config, load_default_config
from driver.CommWrapper import CommWrapper
from driver.components import components

optoboard_serial_1 = "3200030"
optoboard_serial_2 = "2400001"

vtrx_v = "1.3"    # connected VTRx+ quad laser driver version

# get Optoboard chip versions
components_opto_1 = OrderedDict(components[optoboard_serial_1])
components_opto_2 = OrderedDict(components[optoboard_serial_2])

my_config = "configs/my_cool_config.json"

# load configs
config_file_default = load_default_config(components_opto_1, vtrx_v)
config_file_custom = load_custom_config(my_config)

# create wrapper to FELIX
communication_wrapper_1 = CommWrapper(flx_G=0, flx_cid=0, lpgbt_master_addr=0x74, lpgbt_v=1)
communication_wrapper_2 = CommWrapper(flx_G=2, flx_cid=0, lpgbt_master_addr=0x70, lpgbt_v=1)    # another wrapper for different link and lpGBT address

# initialise Optoboard objects
opto1 = Optoboard(config_file_default, optoboard_serial_1, vtrx_v, components_opto_1, communication_wrapper_1)
opto2 = Optoboard(config_file_custom, optoboard_serial_2, vtrx_v, components_opto_2, communication_wrapper_2)

opto1.configure()    # configure all available chips on the Optoboard
opto2.configure()

# some examples of what one can do
opto1.lpgbt3.check_PUSM_status()    # check PUSM status of lpGBT3

opto1.lpgbt3.phase_training(2)    # train phases on EPRX20 channels of lpGBT3

opto.lpgbt1.bert_by_elink("EPRX20")    # BERT on lpGBT1 for elink 2

opto.lpgbt2.read("EPRX00CHNCNTR", "EPRX00INVERT")    # simple read to check polarity of elink 6 (lpGBT2 ch. 0)

opto.vtrx.read("GCR", None)    # check enabled fibre channels of VTRx+ (only v1.3)

opto1.bertScan("lpgbt3", "gbcr3", 0, 6, 11)    # perform an GBCR3 equalizer scan with BERT on lpGBT3
```

## Info for developers

The best approach to test changes in the code of the `optoboard-felix` package is to do so by exploiting the environment provided by the `itk-demo-optoboard` containers.

1. Start the itk-demo-optoboard microservice and the other needed microservices either using the [example](https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-optoboard/-/tree/main/example?ref_type=heads) or via one of the stack repositories, like [this one](https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/deployments/bern). 
2. Clone this repository in the folder mounted in the `/workspace` of the `optoboard-worker` container (usually the one containing the docker compose file used to start the `optoboard-worker`).
3. Attach a terminal to the `optoboard-worker` container: with `pip show optoboard-felix` one can check the current version installed in the container.
4. OLD: In order to test your changes to the `optoboard_felix` repository located in the volume mounted on the `optoboard-worker`, run the following from within the container: `cd /workspace/optoboard_felix && pip uninstall optoboard-felix && pip install .`. At this point it is possible to normally run the `InitOpto ...` command using the code with the local changes.
4. NEW: using uv. The new recommended procedure has different pip install lines  `cd /workspace/optoboard_felix && pip uninstall optoboard-felix && uv pip install .`

Notice that every time the containers are restarted, the local version of the package needs to be reinstalled.
