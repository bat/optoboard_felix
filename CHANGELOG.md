# Changelog

## Tag 1.0.51

- Implementation of the recommended way to configure the secondary lpgbts.

## Tag 1.0.50

- Add back missing registers to config files
- CLKGConfig0 register corrected 

## Tag 1.0.47

- Increase in Optoboard configuration speed (~2.5x)
- Reorganisation of soft error functions, incl. parallelisation of soft error scan function (but only for links on the same device)
- Addition of DC offset (bias) to eye monitoring function output, and change of output to dictionary specifying location of output files

## Tag 1.0.45

- ConfigDB runkey payloads have now to be in a specific format
- Runkey name can be added to Initopto to get it from the ConfigDB



## Tag 1.0.43

- Improved information for developers in the README 
- Major clean-up to work with configDB
- Fix to SoftErrorScan plotting

## Tag 1.0.42

- Changed name to check the EPRX phase
- Added bert_by_phase lpGBT function
- Fix to EPRX40INVERT and EPRX50INVERT in default configs
- Added progress to dependencies

## Tag 1.0.41

- Improved dump_opto_config, bertScan and softErrorScan

## Tag 1.0.40

- Fix to vtrx.configure() function: if only a set of lpGBTs is programmed, then only enable the corresponding fibres
- Added functions to interpret the lpGBT ADCs

## Tag 1.0.39

- Increased timeout for lpgbt-com

## Tag 1..38

- Correction in the components.json file

## Tag 1.0.37

- Renamed opto_doc function to opto_status

## Tag 1.0.36

- Slimmed the configuration files, keeping only the needed registers. Removed the vtrx part, as only the register that controls how many fibers are enabled is written, based on components.json
- Add function for lpGBT to reset the clock for the slave ASICs
- INTERFACE can be either an IP address or the name of the comtainer the IC communication tool needs to connect to, assuming the containers are on the same network 
