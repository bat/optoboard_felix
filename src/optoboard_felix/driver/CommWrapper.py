# pylint: disable=line-too-long, invalid-name, import-error
"""Communication wrapper module."""

import re
import subprocess
import sys
import time
import requests
import traceback

from optoboard_felix.driver.log import logger
from optoboard_felix.driver.CommConfig import *
try:
    from optoboard_felix.register_maps import lpgbt_register_map_v1
    from optoboard_felix.register_maps import lpgbt_register_map_v0
except (ImportError, ModuleNotFoundError):
    logger.critical("Problem with import of lpGBT register maps!")

try:
    from libic_comms import IChandler
except ImportError:
    logger.warning("IChandler library not found, check if installed with FELIX software!")

try:
    from libitk_ic_over_netio_next import ICNetioNextHandler
except ImportError:
    logger.warning("ICNetioNextHandler library not found, check if installed with FELIX software!")



try:
    from lpgbtcom import *
    class ICMaster(ICMaster) :
        def read_reg(self, addr, size = 1) :
            return self.read(addr, size)
        def write_reg(self, addr, value) :
            self.write(addr, bytes(value))
except ImportError:
    logger.warning("lpgbtcom library for lpgbt-com not found! ") 

class CommWrapper():
    """Class for handling the access to the FELIX communication wrapper.

    Communication happens either through the IChandler library (to be installed additionally, check README)
    or the FELIX low level tool flpgbtconf.
    """

    def __init__(self, flx_cid, flx_G, lpgbt_master_addr, lpgbt_v, commToolName="ic-over-netio", test_mode=False):
        """Initialize the CommWrapper object.

        Args:
            flx_cid (int): FELIX device to use (please refer to the FELIX user manual)
            flx_G (int): GBT link number (please refer to the FELIX user manual)
            lpgbt_master_addr (hex): I2C address of the I2C controller
            lpgbt_v (int): lpGBT version (0 or 1)
            commToolName (string): select the communication wrapper to be used
                Current possible values: "flpgbtconf", "ic-over-netio", "lpgbt-com"
            test_mode (bool, optional): set to True if hardware is not available and to test software.
                Returns sent register data for write_read and 4 for all read operations.

        Attributes:
            flx_cid (int): FELIX device to use (please refer to the FELIX user manual)
            flx_G (int): GBT link number (please refer to the FELIX user manual)
            lpgbt_master_addr (hex): I2C address of the I2C controller
            lpgbt_v (int): lpGBT version (0 or 1)
            commToolName (string): select the communication wrapper to be used
                Current possible values: "flpgbtconf", "ic-over-netio", "lpgbt-com"
            test_mode (bool, optional): set to True if hardware is not available and to test software.
                Returns sent register data for write_read and 4 for all read operations.
            lpgbt_reg_map (class): lpGBT register map imported
        """
        self.flx_cid = flx_cid
        self.flx_G = flx_G
        self.lpgbt_master_addr = lpgbt_master_addr
        self.lpgbt_v = lpgbt_v
        self.commToolName = commToolName
        self.test_mode = test_mode
        if self.lpgbt_v:
            self.lpgbt_reg_map = lpgbt_register_map_v1.LpgbtRegisterMapV1
        else:
            self.lpgbt_reg_map = lpgbt_register_map_v0.LpgbtRegisterMapV0

        if commToolName == "ic-over-netio":
            if "libic_comms" not in sys.modules:
                logger.error("IChandler library libic_comms not found, setting commToolName to \"flpgbtconf\"! To configure you now need to disable felixcore and use \"flpgbtconf\" or install the library!")
                self.commToolName = "flpgbtconf"
            else:
                logger.info(CommConfig["IChandler_config"])
                self.ICHandler = IChandler(CommConfig["IChandler_config"]["ip"], CommConfig["IChandler_config"]["tx_port"]+int(self.flx_cid), CommConfig["IChandler_config"]["rx_port"]+int(self.flx_cid), 
                    CommConfig["IChandler_config"]["tx_tag"] + 64*int(self.flx_G), CommConfig["IChandler_config"]["rx_tag"] + 64*int(self.flx_G), False, self.lpgbt_v, self.lpgbt_master_addr)
                logger.info("Initialized IChandler with IP: %s, TX port: %s, RX port: %s, TX tag: %s, RX tag: %s", CommConfig["IChandler_config"]["ip"], CommConfig["IChandler_config"]["tx_port"]+int(self.flx_cid), CommConfig["IChandler_config"]["rx_port"]+int(self.flx_cid),
                            CommConfig["IChandler_config"]["tx_tag"] + 64*int(self.flx_G), CommConfig["IChandler_config"]["rx_tag"] + 64*int(self.flx_G))
                try:
                    self.ICHandler.connect()
                except:
                    logger.warning("ICHandler.connect() failed! The method might be unavailable in the ICHandler version in use! Only consider this warning if communication with the Optoboard via Felixcore fails.")
                self.ICHandler.setMaxRetries(4)

        elif commToolName == "itk-ic-over-netio-next":
            if "libitk_ic_over_netio_next" not in sys.modules:
                logger.error("ICNetioNextHandler library libitk_ic_over_netio_next not found, setting commToolName to \"flpgbtconf\"! To configure you now need to disable felixcore and use \"flpgbtconf\" or install the library!")
                self.commToolName = "flpgbtconf"
            else:
                # the values for rx_fid (tx_fid) are printed for each IC channel when felix-tohost (-toflx) is started
                # Use the `felix-fid -v <FULL-FID>` command to investigate the meaning of each bit
                logger.info(CommConfig["lpgbt_to_felixStar_config"])
                self.rx_fid = 0x1000000000000000 | (CommConfig["lpgbt_to_felixStar_config"]["rx_tag"] << 16) +  (4*int(self.flx_G) << 20) + (int(self.flx_cid) << 36)
                self.tx_fid = 0x1000000000008000 | (CommConfig["lpgbt_to_felixStar_config"]["tx_tag"] << 16) +  (4*int(self.flx_G) << 20) + (int(self.flx_cid) << 36)                     
                self.bus_dir = CommConfig["lpgbt_to_felixStar_config"]["bus_dir"]   
                self.interface = CommConfig["lpgbt_to_felixStar_config"]["ip"]    
                self.ICHandler = ICNetioNextHandler(self.interface, self.bus_dir, self.tx_fid, self.rx_fid, False, 
                                                    self.lpgbt_v, self.lpgbt_master_addr)
                logger.info("TX: %s RX: %s", self.tx_fid, self.rx_fid)
                try:
                    self.ICHandler.connect()
                except:
                    logger.warning("ICHandler.connect() failed! Check if the method is available and if the ICHandler configuration is correct!")
                    
        elif commToolName == "lpgbt-com":
            if "lpgbtcom" not in sys.modules:
                logger.error("lpgbtcom library not found, setting commToolName to \"flpgbtconf\"! To configure you now need to disable felixclient and use \"flpgbtconf\" or install the library!")
                self.commToolName = "flpgbtconf" 
            else:
                # the values for rx_fid (tx_fid) are printed for each IC channel when felix-tohost (-toflx) is started
                # Use the `felix-fid -v <FULL-FID>` command to investigate the meaning of each bit
                logger.info(CommConfig["lpgbt_to_felixStar_config"])
                self.rx_fid = 0x1000000000000000 | (CommConfig["lpgbt_to_felixStar_config"]["rx_tag"] << 16) +  (4*int(self.flx_G) << 20) + (int(self.flx_cid) << 36)
                self.tx_fid = 0x1000000000008000 | (CommConfig["lpgbt_to_felixStar_config"]["tx_tag"] << 16) +  (4*int(self.flx_G) << 20) + (int(self.flx_cid) << 36)                
                self.bus_dir = CommConfig["lpgbt_to_felixStar_config"]["bus_dir"]   
                self.interface = CommConfig["lpgbt_to_felixStar_config"]["ip"]
                self.verbose_level = CommConfig["lpgbt_to_felixStar_config"]["verbose_level"]
                # arguments: interface/ip, bus directory, bug group, rx fid, tx fid, log level, verbose bus
                self.ctrl = FelixClientCtrl(self.interface, self.bus_dir, "FELIX",  self.rx_fid,  self.tx_fid, self.verbose_level, False)
                logger.info("TX: %s RX: %s", self.tx_fid, self.rx_fid)
                
                try:
                    self.ctrl.connect()
                    time.sleep(0.5)
                except:
                    logger.fatal("Check if the specified path to the folder \"/bus\" of the FELIX software in use is correct and if FELIX is running!")

                self.ic_lpgbt_com = ICMaster(self.ctrl)
                self.ic_lpgbt_com.lpgbt_ver = lpgbt_v
                self.ic_lpgbt_com.i2c_addr  = lpgbt_master_addr 
                self.ic_lpgbt_com.reply_timeout = 5000

        logger.info("CommWrapper object initialised!")

    def comm_wrapper(self, reg_name, reg_data, readback_bool=True):
        """Communicate with FELIX.

        Tries 3 communication attempts before raising an Exception.

        Args:
            reg_name (str): register name
            reg_data (int): register data to write
            readback_bool (bool): if False, don't try to read back the written value

        Returns:
            read_reg (str): read register according to FELIX flpgbtconf tool
            read (int): read-back
        """
        max_tries = 2

        if self.test_mode:
            if reg_data is None:
                reg_data = 4
            return reg_name, reg_data

        if self.commToolName == "flpgbtconf":

            flpgbtconf_tries = 0

            logger.debug("comm_wrapper - parsed - flx_G: %s, flx_cid: %s, lpgbt_v: %s, lpgbt_master_addr: %s, reg_data: %s", self.flx_G, self.flx_cid, self.lpgbt_v, self.lpgbt_master_addr, reg_data)

            process_list = ["flpgbtconf"]

            if self.lpgbt_v:
                process_list.append("-1")

            process_list.extend(["-G", str(self.flx_G), "-I", hex(self.lpgbt_master_addr), "--flx_cid", str(self.flx_cid)])

            process_list.append(reg_name)


            if reg_data is not None:
                process_list.append(hex(reg_data))

            logger.debug("comm_wrapper - process_list: %s", process_list)

            while flpgbtconf_tries<max_tries:
                output = subprocess.Popen(process_list, stdout=subprocess.PIPE)

                output_lines = output.stdout.readlines()

                logger.debug("comm_wrapper - readlines() output - output_lines: %s", output_lines)

                output_last_line = str(output_lines[-1])
                logger.debug("comm_wrapper - output_last_line: %s", output_last_line)

                if not readback_bool:
                    return "Write-only operation"

                if "Reply not received" in output_last_line:

                    flpgbtconf_tries += 1

                    logger.warning("Reply not received from FELIX! - tried %s time(s)", flpgbtconf_tries)

                    if flpgbtconf_tries>max_tries-1:
                        logger.error("comm_wrapper - no reply from FELIX. Aborting, check link, Optoboard serial, config file or FELIX settings!")
                        raise Exception("No FELIX connection.")
                else: break

            try:
                read_reg = re.search("\'(.+?)\:", output_last_line).group(1)
                read = int(re.search("\((.+?)\)", output_last_line).group(1))
            except:
                logger.error("Failed to parse the reply from flpgbtconf! Check if FelixCore is running!")
                raise Exception("No FELIX connection.")

            logger.debug("comm_wrapper - returning - read_reg: %s, read: %s", read_reg, read)

            flpgbtconf_tries = 0

            return read_reg, read

        elif self.commToolName == "flpgbtconfAPI":

            flpgbtconfAPI_tries = 0
            
            url = os.environ.get("FELIX_BACKEND_URL") if os.environ.get("FELIX_BACKEND_URL") is not None else "http://localhost:8001"

            if reg_data is None:
                endpoint = url + "/readoptoboard/"
            else:
                endpoint = url + "/writeoptoboard/"
            
            endpoint += "-G" + str(self.flx_G) + "--flx_cid" + str(self.flx_cid) + "-I" + hex(self.lpgbt_master_addr)
            
            if self.lpgbt_v:
                endpoint += "-1"
            
            endpoint += "/" + reg_name 

            if reg_data is not None:
                endpoint += "/" + str(reg_data)

            logger.debug("comm_wrapper - endpoint: %s", endpoint)

            while flpgbtconfAPI_tries<max_tries:

                output = requests.get(endpoint, headers={"content-type": "application/json"}).json()["result"].split("\n")
                logger.debug("comm_wrapper - readlines() output - output_lines: %s", output)

                output_last_line = output[-2]
                logger.debug("comm_wrapper - output_last_line: %s", output_last_line)

                if not readback_bool:
                    return "Write-only operation"

                if "Reply not received" in output_last_line:

                    flpgbtconfAPI_tries += 1

                    logger.warning("Reply not received from FELIX! - tried %s time(s)", flpgbtconfAPI_tries)

                    if flpgbtconfAPI_tries>max_tries-1:
                        logger.error("comm_wrapper - no reply from FELIX. Aborting, check link, Optoboard serial, config file or FELIX settings! Also check that the url of the FELIX Server is correct!")
                        raise Exception("No FELIX connection.")
                else: break

            try:
                read_reg = output_last_line.split(":")[0]
                read = int(output_last_line.split("(")[1].split(")")[0])
            except:
                logger.error("Failed to parse the reply from flpgbtconf! Check if FelixCore is running!")
                raise Exception("No FELIX connection.")

            logger.debug("comm_wrapper - returning - read_reg: %s, read: %s", read_reg, read)

            flpgbtconf_tries = 0

            return read_reg, read


        elif self.commToolName == "ic-over-netio" or self.commToolName == "itk-ic-over-netio-next":
            ICHandler_tries = 0

            while ICHandler_tries<max_tries:

                try:
                    if reg_data is not None:
                        self.ICHandler.sendRegs({reg_name:[int(reg_data)]})
                    if not readback_bool:
                        return "Write-only operation"

                    readAll = self.ICHandler.readRegs([reg_name])
                    read_reg = readAll[0][0]
                    read_back = readAll[0][1]

                except:
                    ICHandler_tries += 1
                    logger.warning("Reply not received from FELIX! - tried %s time(s)", ICHandler_tries)

                    if ICHandler_tries>max_tries-1:
                        e = str(traceback.format_exc())
                        if "M_range_check" in e:
                            logger.error("""comm_wrapper - no reply from FELIX. Aborting. Here is a list of things that could be wrong from most to less probable:
                                ->Use flx-info pod and flx-info link to know more about the connection to the optoboard
                                1. The felix group might be wrong, check the flx_g in the configuration file,
                                2. The fibers connection might be wrong,
                                3. The optoboard power might be off,
                                and less probable:
                                    4. The lpgbt address might be wrong or the lpgbt version number. Check in the components file,
                                    5. The IP address of felix might be wrong,
                                    6. The felix device might be wrong,
                                    7. The port used for communication with Felix might be wrong.""")
                        elif "could not connect" in e:
                            logger.error("""comm_wrapper - no reply from FELIX. Aborting. Here is a list of things that could be wrong:
                                ->Use flx-info pod and flx-info link to know more about the connection to the optoboard
                                1. The felix device might be wrong, check the flx_cid/flx_d in the configuration file,
                                2. The port used for communication with Felix might be wrong,
                                3. The fibers connection might be wrong,
                                4. The optoboard power might be off,
                                and less probable:
                                    5. The IP address of felix might be wrong,
                                    6. The felix group might be wrong,
                                    7. The lpgbt address might be wrong or the lpgbt version number. Check in the components file.""")
                        else:
                            logger.error(e)
                            logger.error("comm_wrapper - no reply from FELIX. Aborting, check link, Optoboard serial, config file or FELIX settings!")
                        raise Exception("No FELIX connection.")
                    try:
                        self.ICHandler.resetConnections()
                    except:
                        logger.warning("ICHandler.resetConnections() failed! The method might be unavailable in the ICHandler version in use!")

                else: break

            logger.debug("comm_wrapper - ICHandler - reg_data: %s, read_reg: %s, read_back: %s", reg_data, read_reg, read_back)

            return read_reg, read_back

        elif self.commToolName == "lpgbt-com":
            lpgbt_com_tries = 0

            while lpgbt_com_tries<max_tries:

                try:
                    if reg_data is not None:
                        self.ic_lpgbt_com.write_reg(int(self.lpgbt_reg_map().Reg[reg_name].value), [int(reg_data)])
                    if not readback_bool:
                        return "Write-only operation"

                    # read operation
                    read_back = self.ic_lpgbt_com.read_reg(self.lpgbt_reg_map().Reg[reg_name].value)
                    if read_back == b'':
                        raise Exception("Empty reply")

                except:
                    lpgbt_com_tries += 1
                    logger.warning("Reply not received from FELIX! - tried %s time(s)", lpgbt_com_tries)
                    if lpgbt_com_tries>max_tries-1:
                        logger.error("comm_wrapper - no reply from FELIX. Aborting, check link, Optoboard serial, config file or FELIX settings!")    
                        raise Exception("No FELIX connection.")
                else: break
            
            read_back = int.from_bytes(read_back, "little")
            logger.debug("comm_wrapper - lpgbt_com_tries - reg_data: %s, read_reg: %s, read_back: %s", reg_data, reg_name, read_back)

            return reg_name, read_back
            
        else:
            logger.fatal("The provided name for the communication library doesn't exist!!")

