# pylint: disable=line-too-long, invalid-name, import-error
"""GBCR module."""

from optoboard_felix.driver.log import logger
from optoboard_felix.driver.Hardware import Hardware, ordered_configuration

try:
    from optoboard_felix.register_maps import gbcr2_register_map, gbcr3_register_map
except (ImportError, ModuleNotFoundError):
    logger.critical("Error in importing GBCR register maps!")



class Gbcr(Hardware):
    """Class for controlling the GBCRs on an Optoboard.

    Attributes:
        device_type (str): "gbcr"
        master (bool): False because it can only be controlled via the lpGBT master
    """

    device_type = "gbcr"
    master = False

    def __init__(self, Communication_wrapper, device, address, master, I2C_master, config_file, gbcr_v):
        """Initialize of GBCR object.

        Args:
            Communication_wrapper (class): low level access to FELIX access
            device (str): gbcr1, gbcr2, gbcr3, gbcr4
            address (hex): I2C address of the GBCR
            master (bool): should always be False
            I2C_master (int): I2C controller to use (can be 0, 1, 2)
            config_file (dict): configuration values from JSON config
            gbcr_v (int): GBCR version

        Attributes:
            gbcr_v (int): GBCR version, either 2 or 3
            gbcr_reg_map (class): imported GBCR register map module
            config_file (dict): configuration values from JSON config

        Raises:
            ValueError: if gbcr_v is not 2 or 3
        """
        super().__init__(Communication_wrapper, device, address, master, I2C_master, config_file)

        self.gbcr_v = gbcr_v

        if self.gbcr_v == 2:
            self.gbcr_reg_map = gbcr2_register_map.GBCRRegisterMap
        elif self.gbcr_v ==3:
            self.gbcr_reg_map = gbcr3_register_map.GBCRRegisterMap
        else:
            raise ValueError("gbcr version is nor 2 neither 3")

        self.config_file["gbcr"] = ordered_configuration(self.config_file["gbcr"], self.gbcr_reg_map)
        self.config_file.pop("power_up_registers_master")
        self.config_file.pop("power_up_registers_slave")
        self.config_file.pop("lpgbt")
        logger.info("%s object initialised!", self.device)

    def configure(self, validate = True):
        """Configure all registers of the GBCR.
            If a secondary lpgbt was configured beforhand without using the opto.configure() method, the GBCRs might need to be reset (opto.lpgbt1.reset_all_GBCR())
            validate (bool, optional): set True if a read back and comparison should be performed
        """

        regs = []

        for reg in self.config_file["gbcr"]:
            regs.append(reg)

        logger.debug("gbcr.configure - regs: %s", regs)
        try:
            self.write_list(regs, read_back=validate)
            logger.info("GBCR%s configured!", self.device_nr)
        except Exception as e:
            logger.error("Error configuring GBCR%s: %s. \n The GBCRs might need to be reset (opto.lpgbt1.reset_all_GBCR())", self.device_nr, e)
    

    def set_retiming_mode(self, channel, phase = 5):
        """Set the retiming mode of a GBCR channel with phase.

        Args:
            channel (int): channel of GBCR
            phase (int, optional): phase to set, default 5
        Returns:
            None
        """
        logger.info("Enabling retiming mode for %s channel %s..", self.device, channel)

        if self.gbcr_v == 2:

            # setting RXEN=1, RXSETCM=1, RXENTERMINATION=1, DISTX=1
            self.write_read("LVDSRXTX", None, 113)
            self.write_read("PHASESHIFTER0", "DLLENABLE", 1)

            # turn off equalizer mode on the channel
            self.write_read("CH" + str(channel+1) + "UPLINK2", "CH"  + str(channel+1) + "DISDFF", 0)

            # setting default phase to 5
            if channel == 0:
                self.write_read("PHASESHIFTER5", "DLLCLOCKDELAYCH1", phase)
            elif channel in [1,2]:
                self.write_read("PHASESHIFTER4", "DLLCLOCKDELAYCH"+str(channel+1), phase)
            elif channel in [3,4]:
                self.write_read("PHASESHIFTER3", "DLLCLOCKDELAYCH"+str(channel+1), phase)
            elif channel == 5:
                self.write_read("PHASESHIFTER2", "DLLCLOCKDELAYCH"+str(channel+1), phase)
        
        if self.gbcr_v == 3:

            # enable retimed signal of the voter
            self.write_read("CH" + str(channel+1) + "UPLINK0", "CH" + str(channel+1) + "MUX", 15)
            # adjust the phase
            self.write_read("CH" + str(channel+1) + "UPLINK3", "CH" + str(channel+1) + "CLKDELAY", phase)

        logger.info("%s channel %s retiming mode enabled and set to phase %s!", self.device, channel, phase)

    def set_equalizer(self,channel_lpgbt,mf_eq=None,hf_eq=None):
        """Change the settings of the equalization on one of the channels of the gbcr and read it back.

        Args:
            channel_lpgbt (int): channel of lpGBT
            mf_eq (int, optional): middle frequencies of the gbcr equalization strength
            hf_eq (int, optional): high frequencies of the gbcr equalization strength
        """
        channel_gbcr=6-channel_lpgbt
        if self.gbcr_v == 2:
            if mf_eq == None:
                mid_freq = self.read("CH" + str(channel_gbcr) + "UPLINK1", "CH" + str(channel_gbcr) + "CTLEMFSR")
            else:
                mid_freq = self.write_read("CH" + str(channel_gbcr) + "UPLINK1", "CH" + str(channel_gbcr) + "CTLEMFSR", mf_eq) 
            if hf_eq == None:
                high_freq=self.read("CH" + str(channel_gbcr) + "UPLINK1", "CH" + str(channel_gbcr) + "CTLEHFSR")  
            else:
                high_freq = self.write_read("CH" + str(channel_gbcr) + "UPLINK1", "CH" + str(channel_gbcr) + "CTLEHFSR", hf_eq)
            logger.info("Current gbcr equalization strengh for version 2 on channel %s: mid_freq %s/15 and high_freq %s/15",channel_gbcr,mid_freq, high_freq)

        elif self.gbcr_v == 3:
            if mf_eq == None:
                mid_freq = self.read("CH" + str(channel_gbcr) + "UPLINK2", "CH" + str(channel_gbcr) + "CTLEMFSR")
            else:
                mid_freq = self.write_read("CH" + str(channel_gbcr) + "UPLINK2", "CH" + str(channel_gbcr) + "CTLEMFSR", mf_eq) 
            if hf_eq == None:
                high_freq1 = self.read("CH" + str(channel_gbcr) + "UPLINK1", "CH" + str(channel_gbcr) + "CTLEHF1SR")
                high_freq2 = self.read("CH" + str(channel_gbcr) + "UPLINK1", "CH" + str(channel_gbcr) + "CTLEHF2SR")
                high_freq3 = self.read("CH" + str(channel_gbcr) + "UPLINK2", "CH" + str(channel_gbcr) + "CTLEHF3SR")  
            else:
                high_freq1 = self.write_read("CH" + str(channel_gbcr) + "UPLINK1", "CH" + str(channel_gbcr) + "CTLEHF1SR", hf_eq)
                high_freq2 = self.write_read("CH" + str(channel_gbcr) + "UPLINK1", "CH" + str(channel_gbcr) + "CTLEHF2SR", hf_eq)
                high_freq3 = self.write_read("CH" + str(channel_gbcr) + "UPLINK2", "CH" + str(channel_gbcr) + "CTLEHF3SR", hf_eq)
            logger.info("Current gbcr equalization strengh for version 3 on channel %s: mid_freq %s/15, high_freq1 %s/15, high_freq2 %s/15, high_freq3 %s/15",channel_gbcr,mid_freq, high_freq1, high_freq2, high_freq3)
            