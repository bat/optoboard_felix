# pylint: disable=line-too-long, invalid-name, import-error
"""Hardware module."""

import inspect

from collections import OrderedDict

from optoboard_felix.driver.log import logger

def ordered_configuration(configuration_4_device, regMap):
    """Order configuration.

    ?

    Attributes:
        configuration_4_device (?): ?
        regMap (class): register map

    Returns:
        dictionary with keys and values
    """

    logger.debug("configuration_4_device: %s", configuration_4_device)

    def myOrder(e):
        return e["reg_address"]

    ordered_configuration = []
    for reg in configuration_4_device:
        dict4_ordering = {}
        dict4_ordering["reg_name"] = reg
        dict4_ordering["reg_address"] = getattr(regMap, str(reg)).address
        if type(configuration_4_device).__name__ == "dict":
            dict4_ordering["content"] = configuration_4_device[reg]
        ordered_configuration.append(dict4_ordering)

    ordered_configuration.sort(key=myOrder)

    if type(configuration_4_device).__name__ == "dict":
        return OrderedDict({x["reg_name"]:x["content"] for x in ordered_configuration})
    else:
        return [x["reg_name"] for x in ordered_configuration]



class Hardware():
    """Class for handling the write and read with the FELIX communication wrapper.

    Attributes:
        I2C_* (hex): I2C controller commands used for writing to slave devices on the Optoboard.
        FREQ (int): I2C controller bus speed
        SCLDRIVE (int): I2C controller SCLDriveMode (see also lpGBTv1 manual ch. 12.2.1)
    """

    I2C_WRITE_CR = 0x0
    I2C_WRITE_MSK = 0x1
    I2C_1BYTE_WRITE = 0x2
    I2C_1BYTE_READ = 0x3
    I2C_1BYTE_WRITE_EXT = 0x4
    I2C_1BYTE_READ_EXT = 0x5
    I2C_1BYTE_RMW_OR = 0x6
    I2C_1BYTE_RMW_XOR = 0x7
    I2C_W_MULTI_4BYTE0 = 0x8
    I2C_W_MULTI_4BYTE1 = 0x9
    I2C_W_MULTI_4BYTE2 = 0xA
    I2C_W_MULTI_4BYTE3 = 0xB
    I2C_WRITE_MULTI = 0xC
    I2C_READ_MULTI = 0xD
    I2C_WRITE_MULTI_EXT = 0xE
    I2C_READ_MULTI_EXT = 0xF
    FREQ = 2
    SCLDRIVE = 0

    def __init__(self, Communication_wrapper, device, address, master, I2C_master, config_file):
        """Initialise the Hardware object.

        Args:
            Communication_wrapper (class): low level access to FELIX access
            device (str): device to write/read
            address (hex): I2C address of device
            master (bool): True if lpGBT master, False if anything else
            I2C_master (int): I2C controller number to use
            config_file (dict): dictionary with list of registers to be configured and their values

        Attributes:
            comm_wrapper (class): low level access to FELIX access
            device (str): device to write/read
            address (hex): I2C address of device
            master (bool): True if lpGBT master, False if anything else
            I2C_master (int): I2C controller number to use
            config_file (dict): dictionary with list of registers to be configured and their values
            device_type (str): can be lpgbt or gbcr or vtrx
            device_nr (int): number of the device if lpGBT or GBCR, can be 1, 2, 3 or 4
        """
        self.comm_wrapper = Communication_wrapper.comm_wrapper
        self.device = device
        self.address = address
        self.master = master
        self.I2C_master = I2C_master
        self.config_file = config_file

        if device[:-1] in ["lpgbt", "gbcr"]:
            self.device_type = device[:-1]
            self.device_nr = int(device[-1:])

        else:
            self.device_type = device

    def validate_reg_data(self, reg, reg_field, reg_data):
        """Validate a provided reg_data according the allowed values from the register map class.

        No return but raises a ValueError if the value is not allowed.

        Args:
            reg (str): register name as in the register maps
            reg_field (str): register field name as in the register maps.
            reg_data (int):	register or register field data to be written

        Raises:
            ValueError: entered register data is not possible
        """
        logger.debug("validate_reg_data - parsed - reg: %s, reg_field: %s, reg_data: %s", reg, reg_field, reg_data)

        reg_field_class = getattr(getattr(eval("self." + self.device_type + "_reg_map"), reg), reg_field)
        length = getattr(reg_field_class, "length")

        if not reg_field_class.validate(reg_data):
            logger.error("Entered register value %s for %s_%s in config is not valid! Available integer values <= %s", reg_data, reg, reg_field, 2**length)
            raise ValueError("Wrong register data entered.")

    def construct_reg_data(self, reg):
        """Construct the full 8 bits reg_data according to the register field values in config_file and the offsets/lengths from the register map class.

        Args:
            reg (str): register name as in the register maps

        Returns:
            reg_data (int): register data constructed

        Raises:
            AttributeError: device does not have a corresponding register map
            AttributeError: register not found in the register map
            AttributeError: setting not found in register
            AttributeError: offset not defined for setting in register
        """
        reg_data = 0
        logger.debug("construct_reg_data() - parsed - reg: %s", reg)

        device_reg_map = getattr(self, f"{self.device_type}_reg_map", None)
        if not device_reg_map:
            logger.error("Device type %s does not have a corresponding register map.", self.device_type)
            raise AttributeError(f"Device does not have a corresponding register map.")

        reg_class = getattr(device_reg_map, reg, None)
        if not reg_class:
            logger.error("Register %s not found in the register map for device type %s", reg, self.device_type)
            raise AttributeError(f"Register not found in the register map.")

        for setting in dir(reg_class):
            if setting.startswith("__") or setting == "address":    # skipping internal attributes
                continue

            reg_field_class = getattr(reg_class, setting, None)
            if not reg_field_class:
                logger.error("Setting %s not found in register %s", setting, reg)
                raise AttributeError(f"Setting not found in register.")

            try:
                value = self.config_file[self.device_type][reg][setting][self.device_nr-1]
            except KeyError:    # fallback to the default value from the register setting
                if hasattr(reg_field_class, "default"):
                    value = reg_field_class.default
                else:
                    logger.debug("%s has no default value for setting %s, using value 0", reg, setting)
                    value = 0

            offset = getattr(reg_field_class, "offset", None)
            if offset is None:
                logger.error("Offset not defined for setting %s in register %s", setting, reg)
                raise AttributeError(f"Offset not defined for setting in register.")

            logger.debug("construct_reg_data() - found setting: %s, value: %s, offset: %s", setting, value, offset)

            if self.device_type == "vtrx":
                self.validate_reg_data(reg, setting, value)
                reg_data = reg_data | value << offset
            else:
                self.validate_reg_data(reg, setting, value)
                reg_data = reg_data | value << offset

        logger.debug("construct_reg_data - returning reg_data: d\'%s (h\'%s / b\'%s)", reg_data, hex(reg_data), bin(reg_data)[2:].zfill(8))

        return reg_data
    
    def sort_regs(self, reg_dict):
        """Splits and sorts a dictionary of registers including values according to their consecutive addresses.

        Enables the user to split an arbitrary amount of registers and their desired values if their addresses are not consecutive.
        The split and sorted dictionaries are to optimise the write process of configurations.

        Example input:
        {
         'reg_addr': [200, 208, 201, 212, 202, 216, 203],
         'vals': [77, 66, 55, 44, 33, 22, 11],
         'regs': ['EPRX0CONTROL', 'EPRX00CHNCNTR', 'EPRX1CONTROL', 'EPRX10CHNCNTR', 'EPRX2CONTROL', 'EPRX20CHNCNTR', 'EPRX3CONTROL']
        }
        which returns:
        [
         {
           'reg_addr': [200, 201, 202, 203],
           'vals': [77, 55, 33, 11],
           'regs': ['EPRX0CONTROL', 'EPRX1CONTROL', 'EPRX2CONTROL', 'EPRX3CONTROL']
         },
         {'reg_addr': [208], 'vals': [66], 'regs': ['EPRX00CHNCNTR']},
         {'reg_addr': [212], 'vals': [44], 'regs': ['EPRX10CHNCNTR']},
         {'reg_addr': [216], 'vals': [22], 'regs': ['EPRX20CHNCNTR']}
        ]
        
        Args:
            reg_dict (dictionary)

        Returns:
            sorted_dict (list): a list of dictionaries split b
        
        """

        sorted_pairs = sorted(zip(reg_dict["reg_addr"], reg_dict["vals"], reg_dict["regs"]))
        sorted_addr, vals, regs = map(list, zip(*sorted_pairs))
        sorted_dict = []
        current_dict = {
            "reg_addr": [],
            "vals": [],
            "regs": []
        }
        for i in range(len(sorted_addr)):
            if not current_dict["reg_addr"]:
                current_dict["reg_addr"].append(sorted_addr[i])
                current_dict["vals"].append(vals[i])
                current_dict["regs"].append(regs[i])
            else:
                diff = sorted_addr[i] - current_dict["reg_addr"][-1]
                if diff ==  1:
                    current_dict["reg_addr"].append(sorted_addr[i])
                    current_dict["vals"].append(vals[i])
                    current_dict["regs"].append(regs[i])
                elif diff > 1 and diff < 5 :
                    # Add the missing register in between for consecutive addresses
                    for diff_i in range(diff-1,0,-1):
                        current_dict["reg_addr"].append(sorted_addr[i] - diff_i)
                        inbetween_name = self.lpgbt_reg_map.Reg(sorted_addr[i]-diff_i).name
                        current_dict["regs"].append(inbetween_name)
                        value = self.default_value_register[inbetween_name] if inbetween_name in self.default_value_register.keys() else 0
                        current_dict["vals"].append(value)
                    #Also adding the current register
                    current_dict["reg_addr"].append(sorted_addr[i])
                    current_dict["vals"].append(vals[i])
                    current_dict["regs"].append(regs[i])
                else:
                    # If not consecutive, save the current group and start a new one
                    sorted_dict.append(current_dict)
                    current_dict = {
                        "reg_addr": [sorted_addr[i]],
                        "vals": [vals[i]],
                        "regs": [regs[i]]
                    }
        
        # Add the last group if it exists
        if current_dict:
            sorted_dict.append(current_dict)
        
        return sorted_dict

    def write_list(self, regs, reg_vals=None, read_back=True):
        """Write a list of registers.

        Args:
            regs (list): list of registers (strings) to write
            reg_vals (list, optional): register values, if none provided the values from the configuration file are taken
            read_back (bool, optional):

        Returns:
            readout (dict, optional): dictionary with register names and values

        Raises:
            TypeError: input regs is not of type list or not all elements of the list are strings
        """
        write_dict = {
            "reg_addr": [],
            "vals": [],
            "regs": regs
        }
        logger.debug("write_list() - args - regs: %s, reg_vals: %s, read_back: %s", regs, reg_vals, read_back)

        if not isinstance(regs, list) and all(isinstance(item, str) for item in regs):
            logger.error("Input is not a list or the content are not strings! regs: %s, type(regs): %s", regs, type(regs))
            raise TypeError("Input is not of type list or doesn't contain strings")

        for i, reg in enumerate(regs):
            logger.debug("write_list() - i: %s, reg: %s", i, reg)

            if "lpgbt" in self.device:
                write_dict["reg_addr"].append(int(self.lpgbt_reg_map().Reg[reg].value))
            elif "gbcr" in self.device:
                write_dict["reg_addr"].append(int(self.gbcr_reg_map().Reg[reg].value))
            else:
                write_dict["reg_addr"].append(int(self.vtrx_reg_map().Reg[reg].value))

            if not reg_vals == None:
                write_dict["vals"].append(reg_vals[i])
            else:
                write_dict["vals"].append(self.construct_reg_data(reg))


        dicts_to_write = self.sort_regs(write_dict)

        logger.debug("write - write_dict: %s", write_dict)
        logger.debug("write - dicts_to_write: %s", dicts_to_write)
        readout_list = []
        for sorted_dict in dicts_to_write:
            count_mismatch = 1
            write_read_check = False
            while count_mismatch < 5 and not write_read_check:
                if len(sorted_dict["reg_addr"]) == 1:
                    readout = self.write_read(sorted_dict["regs"][0], None, sorted_dict["vals"][0])
                    write_read_check = readout == sorted_dict["vals"][0] 
                    if not write_read_check and count_mismatch < 4 and read_back:  
                        logger.warning("For %s - expected: %s, received: %s",sorted_dict["regs"], sorted_dict["vals"], readout)
                    if write_read_check and count_mismatch > 1 and read_back:
                        logger.info("Writing register %s successful after %s tries!", sorted_dict["regs"][0], count_mismatch)
                    if read_back:
                        readout_list.append(readout)
                else:
                    readout = self.write_multi(sorted_dict["reg_addr"][0], sorted_dict["reg_addr"][-1], sorted_dict["vals"], read_back=read_back)
                    reg_val_dict = dict(zip(sorted_dict['regs'], sorted_dict['vals']))
                    write_read_check = readout == reg_val_dict
                    if not write_read_check and count_mismatch < 4 and read_back:  
                        logger.warning("For %s - expected: %s, received: %s",sorted_dict["regs"], sorted_dict["vals"], readout)
                    if write_read_check and count_mismatch > 1 and read_back:
                        logger.info("Writing register %s successful after %s tries!", sorted_dict["regs"], count_mismatch)
                    if read_back:
                        readout_list.append(readout)
                count_mismatch += 1
                if count_mismatch == 4:
                    logger.error("The read back: %s is not identical to the sent reg_data: %s, %s", readout, sorted_dict["regs"],sorted_dict["vals"])
                    # raise ValueError("The read back value is not identical to sent data! 4 attempts were tried!")
        if read_back:
            return readout_list
        

    def write_read(self, reg, reg_field, reg_data, skip_retry=False):
        """Write and read back a register of a device on the Optoboard.

        Args:
            reg (str): register name as in the register maps
            reg_field (str): register field name as in the register maps.
                If None is provided reg_data gets sent to the device as a full byte
                If a reg_field string is provided only the field gets updated.
            reg_data (int):	register or register field data to be written

        Returns:
            read_back (int): read back value of the register written

        Raises:
            ValueError: if 4 attempts of writing have been attempted and the read back is not identical to the sent reg_data
        """
        count_mismatch = 0
        while count_mismatch < 4:

            reg = reg.upper()
            if reg_field is not None: reg_field = reg_field.upper()

            if reg_field is not None:

                logger.debug("Writing register field %s", reg_field)
                self.validate_reg_data(reg, reg_field, reg_data)

                logger.disabled = True
                current_reg_data = self.read(reg, None)      # gets the currently stored register value
                logger.disabled = False

                reg_data_neg = 0
                reg_class = getattr(eval("self." + self.device_type + "_reg_map"), reg)

                for setting in inspect.getmembers(reg_class):
                    if inspect.isclass(setting[1]) and not "__" in setting[0]:
                        if not setting[0] == reg_field:
                            reg_data_neg = reg_data_neg | getattr(getattr(reg_class, setting[0]), "bit_mask")   # creates an integer from 1 byte with all 1s but the size and offset of reg_field

                new_reg_data = current_reg_data & reg_data_neg      # takes the current_reg_data and sets the bits from reg_field to 0

                logger.debug("write_read - current_reg_data: %s, reg_data_neg: %s, new_reg_data: %s", bin(current_reg_data)[2:].zfill(8), bin(reg_data_neg)[2:].zfill(8), bin(new_reg_data)[2:].zfill(8))

                new_reg_data = new_reg_data | reg_data << getattr(getattr(reg_class, reg_field), "offset")      # writes the reg_field with the provided reg_data

                logger.debug("write_read - changed reg_data from %s to %s", bin(current_reg_data)[2:].zfill(8), bin(new_reg_data)[2:].zfill(8))

                parsed_reg_data = reg_data
                reg_data = new_reg_data     # set reg_data to new 1 byte register data otherwise the reg_field data gets written to the full register

            ### write-read for lpGBT master over IC
            if self.master:

                read_reg, read_back = self.comm_wrapper(reg, reg_data)

                if reg_field is not None:

                    reg_field_class = getattr(getattr(eval("self." + self.device_type + "_reg_map"), reg), reg_field)
                    read_back = read_back >> getattr(reg_field_class, "offset") & (2**getattr(reg_field_class, "length")-1)
                    read_reg = reg + "_" + reg_field


                logger.debug("write_read - lpgbt master - returning - reg: %s, reg_field: %s, read_reg %s, read_back: %s", reg, reg_field, read_reg, read_back)


            ### write-read to slave device with lpgbt master I2C controller
            else:

                reg_addr = getattr(getattr(eval("self." + self.device_type + "_reg_map"), reg), "address")

                # configure I2C transaction
                if self.device_type == "lpgbt":        # differentiation because lpGBT slave reg address can be larger than 256, address gets split into Data0 and Data1
                    NBYTE = 3
                else:
                    NBYTE = 2

                logger.debug("write-read - I2C slave - NBYTE: %s", NBYTE)

                self.comm_wrapper("I2CM" + str(self.I2C_master) + "DATA0", (self.SCLDRIVE << 7) | (NBYTE << 2) | self.FREQ)
                self.comm_wrapper("I2CM" + str(self.I2C_master) + "CMD", self.I2C_WRITE_CR)

                # Send register address and register value
                if self.device_type == "lpgbt":
                    self.comm_wrapper("I2CM" + str(self.I2C_master) + "DATA0", divmod(reg_addr,0x100)[1])    # Lower half of register address
                    self.comm_wrapper("I2CM" + str(self.I2C_master) + "DATA1", divmod(reg_addr,0x100)[0])    # Upper half of register address
                    self.comm_wrapper("I2CM" + str(self.I2C_master) + "DATA2", reg_data)        # Register data to send
                else:
                    self.comm_wrapper("I2CM" + str(self.I2C_master) + "DATA0", reg_addr)
                    self.comm_wrapper("I2CM" + str(self.I2C_master) + "DATA1", reg_data)

                self.comm_wrapper("I2CM" + str(self.I2C_master) + "CMD", self.I2C_W_MULTI_4BYTE0)
                self.comm_wrapper("I2CM" + str(self.I2C_master) + "ADDRESS", self.address)
                self.comm_wrapper("I2CM" + str(self.I2C_master) + "CMD", self.I2C_WRITE_MULTI)    # Initiate send of register address and register value

                # reading status register for read of register
                status_value = self.comm_wrapper("I2CM" + str(self.I2C_master) + "STATUS", None)
                self.status_info(status_value)     # Read status register

                ## Read for I2C device
                # logger.disabled = True
                read_back = self.read(reg, reg_field)
                # logger.disabled = False

            if reg_field is not None:
                read_reg = reg + "_" + reg_field
                reg_data = parsed_reg_data
            else:
                read_reg = reg      # would otherwise return "I2CM" + str(I2C_master) + "READ15"

            if not read_back == reg_data:
                if skip_retry: break
                logger.warning("Read back value is not identical to sent data! Register: %s, sent: %s, read back: %s. Trying again...", read_reg, reg_data, read_back)
                count_mismatch += 1

            else:
                if count_mismatch > 0:
                    logger.info("Read back value and sent value are identical after %s tries!", count_mismatch+1)
                break

            logger.debug("Written to %s - %s: %s (%s, %s)", self.device, read_reg, read_back, hex(read_back), bin(read_back))

        if count_mismatch == 4:
            logger.error("The read back: %s is not identical to the sent reg_data: %s", read_back, reg_data)
            raise ValueError("The read back value is not identical to sent data! 4 attempts were tried!")

        return read_back

    def write_multi(self, first_reg, last_reg, reg_val, read_back=True):
        """Write multiple successive registers of a device.

        Primarly thought for I2C slave devices, but can handle writes to lpGBT master through IC.
        
        Args:
            first_reg (int): address of the first register
            last_reg (int): address of the last register
            reg_val (list): values to write
            read_back (bool, optional): returns dict of read_multi()
        
        Raises:
            ValueError: write to lpGBT1 (master) is not done through I2C
            ValueError: first register higher as last register
            ValueError: register values out of bounds
        """
        if "lpgbt" in self.device:
            max_reg = 493
        elif "vtrx" in self.device:
            if self.vtrx_v == "1.2":
                max_reg = 31
            else:
                max_reg = 29        # VTRx+ QLD v1.3 and v1.4 have only 29 registers specified in the manual
        else:
            max_reg = 31        # GBCRs

        if first_reg > last_reg:
            logger.error("Register address start needs to be lower than the end! first_reg: %s, last_reg: %s", first_reg, last_reg)
            raise ValueError("Check register address bounds")

        if last_reg > max_reg or first_reg < 0:
            logger.error("Register address range out of bounds! first_reg: %s (%s, %s), last_reg: %s (%s, %s)", first_reg, hex(first_reg), bin(first_reg), last_reg, hex(last_reg), bin(last_reg))
            raise ValueError("Check register address bounds")

        n_regs = last_reg - first_reg + 1
        logger.debug("write_multi() - first_reg: %s, last_reg: %s, n_regs: %s", first_reg, last_reg, n_regs)

        reg_index = 0
        reg_addr = first_reg

        while (reg_addr <= last_reg):

            if self.master:
                self.comm_wrapper(self.lpgbt_reg_map.Reg(reg_addr).name, reg_val[reg_index], readback_bool=False)
                reg_index += 1
                reg_addr += 1

            else:

                if "lpgbt" in self.device:      # only for lpGBT slaves with register addresses greater than 8 bits
                    double_reg_address = True
                    
                    if reg_addr + 14 > last_reg:
                        NBYTE = last_reg - reg_addr + 3    # adding 3 for two bytes register address of lpGBT slave
                    else:
                        NBYTE = 16

                else:       # GBCRs and VTRx+ have single byte address
                    double_reg_address = False

                    if reg_addr + 15 > last_reg:
                        NBYTE = last_reg - reg_addr + 2
                    else:
                        NBYTE = 16

                logger.debug("write_multi() - reg_addr: %s, NBYTE: %s", reg_addr, NBYTE)

                self.comm_wrapper("I2CM" + str(self.I2C_master) + "DATA0", (self.SCLDRIVE << 7) | (NBYTE << 2) | self.FREQ)
                self.comm_wrapper("I2CM" + str(self.I2C_master) + "CMD", self.I2C_WRITE_CR)

                if "lpgbt" in self.device:    # here the split of the register address > 255 is done (only for lpGBTs)
                        reg_addr0 = reg_addr & 0xFF
                        reg_addr1 = (reg_addr >> 8) & 0xFF
                        self.comm_wrapper("I2CM" + str(self.I2C_master) + "DATA0", reg_addr0)
                        self.comm_wrapper("I2CM" + str(self.I2C_master) + "DATA1", reg_addr1)
                else:
                    self.comm_wrapper("I2CM" + str(self.I2C_master) + "DATA0", reg_addr)

                for byte in range(0, 4):
                    for data in range(0,4):

                        if byte == 0:
                            if data == 0:
                                continue    # skip first DATA0 due to register address
                            if double_reg_address and data == 1:
                                continue    # skip DATA1 register due to two byte register address of lpGBT slave

                        logger.debug("write_multi() - reg_addr: %s, BYTE%s, DATA%s, reg_index: %s, reg_val[%s]: %s", reg_addr, byte, data, reg_index, reg_index, reg_val[reg_index])
                        self.comm_wrapper("I2CM" + str(self.I2C_master) + "DATA"+str(data), reg_val[reg_index])
                    
                        reg_index += 1
                        reg_addr += 1

                        if reg_addr > last_reg:
                            break

                    self.comm_wrapper("I2CM" + str(self.I2C_master) + "CMD", self.I2C_W_MULTI_4BYTE0+byte)
                    if reg_addr > last_reg:
                        break

                self.comm_wrapper("I2CM" + str(self.I2C_master) + "ADDRESS", self.address)
                self.comm_wrapper("I2CM" + str(self.I2C_master) + "CMD", self.I2C_WRITE_MULTI)

                while True:
                    if self.status_info(self.comm_wrapper("I2CM" + str(self.I2C_master) + "STATUS", None)):
                        break
        if read_back:
            readout = self.read_multi(first_reg, last_reg)
            return readout

    def read(self, reg, reg_field):
        """Read a register of a device on the Optoboard.

        Args:
            reg (str): register name as in the register maps
            reg_field (str): register field name as in the register maps.
                If None is provided reg_data gets sent to the device as a full byte,
                If a reg_field string is provided only the field gets updated.

        Returns:
            read (int): read value of the register
        """
        reg = reg.upper()
        if reg_field is not None: reg_field = reg_field.upper()
        logger.debug("read - parsed - reg: %s, reg_field: %s", reg, reg_field)

        ### Read for lpGBT master over IC
        if self.master:

            try:
                getattr(getattr(eval("self." + self.device_type + "_reg_map"), reg), "address")
            except:
                raise NameError("The provided register name is not in the register map!")

            read_reg, read = self.comm_wrapper(reg, None)

        ### Read with lpGBT master I2C controller on Optoboard
        else:
            try:
                reg_addr = getattr(getattr(eval("self." + self.device_type + "_reg_map"), reg), "address")
            except:
                raise NameError("The provided register name is not in the register map!")

            if self.device_type == "lpgbt":
                NBYTE = 2
            else:
                NBYTE = 1

            self.comm_wrapper("I2CM" + str(self.I2C_master) + "DATA0", (self.SCLDRIVE << 7) | (NBYTE << 2) | self.FREQ)
            self.comm_wrapper("I2CM" + str(self.I2C_master) + "CMD", self.I2C_WRITE_CR)

            # Send register address
            if self.device_type == "lpgbt":
                self.comm_wrapper("I2CM" + str(self.I2C_master) + "DATA0", divmod(reg_addr,0x100)[1])    # Lower half of register address
                self.comm_wrapper("I2CM" + str(self.I2C_master) + "DATA1", divmod(reg_addr,0x100)[0])    # Upper half of register address

            else:
                self.comm_wrapper("I2CM" + str(self.I2C_master) + "DATA0", reg_addr)

            self.comm_wrapper("I2CM" + str(self.I2C_master) + "CMD", self.I2C_W_MULTI_4BYTE0)
            self.comm_wrapper("I2CM" + str(self.I2C_master) + "ADDRESS", self.address)
            self.comm_wrapper("I2CM" + str(self.I2C_master) + "CMD", self.I2C_WRITE_MULTI)    # Initiate send of register address and register value

            # read answer from I2C device
            NBYTE = 1
            self.comm_wrapper("I2CM" + str(self.I2C_master) + "DATA0", (self.SCLDRIVE << 7) | (NBYTE << 2) | self.FREQ)
            self.comm_wrapper("I2CM" + str(self.I2C_master) + "CMD", self.I2C_WRITE_CR)

            self.comm_wrapper("I2CM" + str(self.I2C_master) + "ADDRESS", self.address)
            self.comm_wrapper("I2CM" + str(self.I2C_master) + "CMD", self.I2C_READ_MULTI)

            # if not Optoboard.test_mode:
            #     # Reading status register for read of register
            status_value = self.comm_wrapper("I2CM" + str(self.I2C_master) + "STATUS", None)
            self.status_info(status_value)     # Read status register

            # Read slave answer from LpGBT master register
            read_reg, read = self.comm_wrapper("I2CM" + str(self.I2C_master) + "READ15", None)

        if reg_field is not None:

            reg_field_class = getattr(getattr(eval("self." + self.device_type + "_reg_map"), reg), reg_field)
            read = read >> getattr(reg_field_class, "offset") & (2**getattr(reg_field_class, "length")-1)

            logger.debug("read - reg_field is not None - offset: %s, length: %s", getattr(reg_field_class, "offset"), getattr(reg_field_class, "length"))

            read_reg = reg + "_" + reg_field
        else:
            read_reg = reg      # would otherwise return "I2CM" + str(I2C_master) + "READ15"

        logger.debug("Read from %s - %s: %s (%s, %s)", self.device, read_reg, read, hex(read), bin(read))

        return read

    def read_multi(self, first_reg=0, last_reg=None):
        """Read multiple successive registers of a device.
        
        If no arguments are given a full read-out of all registers is performed.

        Args:
            first_reg (int, optional): first register adress
            last_reg (int, optional): last register address

        Returns:
            readout (dict): dictionary with register name as key and read values as value

        Raises:
            ValueError: start register higher as last register
            ValueError: register values out of bounds
        """
        readout = {}
        if "lpgbt" in self.device:
            max_reg = 493
        elif "vtrx" in self.device:
            if self.vtrx_v == "1.2":
                max_reg = 31
            else:
                max_reg = 29        # VTRx+ QLD v1.3 and v1.4 have only 29 registers specified in the manual
        else:
            max_reg = 31        # GBCRs

        if last_reg == None:
            last_reg = max_reg

        if first_reg > last_reg:
            logger.error("Register address start needs to be lower than the end! first_reg: %s, last_reg: %s", first_reg, last_reg)
            raise ValueError("Check register address bounds")

        if last_reg > max_reg or first_reg < 0:
            logger.error("Register address range out of bounds! first_reg: %s (%s, %s), last_reg: %s (%s, %s)", first_reg, hex(first_reg), bin(first_reg), last_reg, hex(last_reg), bin(last_reg))
            raise ValueError("Check register address bounds")

        if self.master:
            
            for reg_addr in range(first_reg, last_reg+1):
                reg = self.lpgbt_reg_map.Reg(reg_addr).name
                read_reg, read = self.comm_wrapper(reg, None)

                readout[reg] = read

            return readout
        
        i2c_read = []

        for reg_addr in range(first_reg, last_reg+1, 16):

            if "lpgbt" in self.device:    # need two registers for I2C controller to send the address of the register to be read
                NBYTE = 2
            else:
                NBYTE = 1
            self.comm_wrapper("I2CM" + str(self.I2C_master) + "DATA0", (self.SCLDRIVE << 7) | (NBYTE << 2) | self.FREQ)
            self.comm_wrapper("I2CM" + str(self.I2C_master) + "CMD", self.I2C_WRITE_CR)

            if "lpgbt" in self.device:    # here the split of the register address > 255 is done (only for lpGBTs)
                reg_addr0 = reg_addr & 0xFF
                reg_addr1 = (reg_addr >> 8) & 0xFF
                self.comm_wrapper("I2CM" + str(self.I2C_master) + "DATA0", reg_addr0)
                self.comm_wrapper("I2CM" + str(self.I2C_master) + "DATA1", reg_addr1)
            else:
                self.comm_wrapper("I2CM" + str(self.I2C_master) + "DATA0", reg_addr)

            self.comm_wrapper("I2CM" + str(self.I2C_master) + "CMD", self.I2C_W_MULTI_4BYTE0)
            self.comm_wrapper("I2CM" + str(self.I2C_master) + "ADDRESS", self.address)
            self.comm_wrapper("I2CM" + str(self.I2C_master) + "CMD", self.I2C_WRITE_MULTI)    # Initiate send of register address

            # read registers with MULTI_BYTE_READ
            if reg_addr > (last_reg - 16):
                NBYTE = last_reg - reg_addr + 1
            else:
                NBYTE = 16
            logger.debug("read_all_multi - NBYTE: %s", NBYTE)
            logger.debug("Reading registers %s to %s", reg_addr, reg_addr+NBYTE-1)
            
            self.comm_wrapper("I2CM" + str(self.I2C_master) + "DATA0", (self.SCLDRIVE << 7) | (NBYTE << 2) | self.FREQ)
            self.comm_wrapper("I2CM" + str(self.I2C_master) + "CMD", self.I2C_WRITE_CR)

            self.comm_wrapper("I2CM" + str(self.I2C_master) + "ADDRESS", self.address)
            self.comm_wrapper("I2CM" + str(self.I2C_master) + "CMD", self.I2C_READ_MULTI)

            while True:
                if self.status_info(self.comm_wrapper("I2CM" + str(self.I2C_master) + "STATUS", None)):
                    break

            # read answers from lpGBT master registers
            for r in range(15, 15-NBYTE, -1):
                i2c_read.append(self.comm_wrapper("I2CM" + str(self.I2C_master) + "READ"+str(r), None)[1])

        r = 0
        for addr in range(first_reg, last_reg+1):
            
            if "lpgbt" in self.device:
                readout[self.lpgbt_reg_map.Reg(addr).name] = i2c_read[r]
            elif "gbcr" in self.device:
                readout[self.gbcr_reg_map.Reg(addr).name] = i2c_read[r]
            elif "vtrx" in self.device:
                readout[self.vtrx_reg_map.Reg(addr).name] = i2c_read[r]
            else:
                logger.error("Something went wrong with the device")
            r += 1

        logger.debug("read_multi() - readout: %s", readout)

        return readout

    def status_info(self, status):
        """Check whether I2C transaction was successful.

        Depends on I2CMXStatus register of lpGBT master.

        Args:
            status (int): value of the I2CMxSTATUS register

        Returns:
            passed (bool): True if transmission status register is saying success

        Raises:
            Exception: I2C controller clock disabled
            Exception: the last transaction was not acknowledged by the I2C slave
            Exception: invalid command sent to I2C channel
            Exception: SDA line is pulled low '0' before initiating a transaction
            Exception: I2C transmission error, transaction not successful
        """
        passed = False
        status = bin(status[1])[2:].zfill(8)    # Most significant bit now is bit 7, least significant bit now is bit 0
        logger.debug("status_info - status: %s", status)

        if int(status[0]):
            logger.error("40 MHz clock of the I2C master channel is disabled.")
            raise Exception("I2C transmission error")

        elif int(status[1]):
            logger.error("Last transaction not acknowledged by the I2C slave.")
            raise Exception("I2C transmission error")

        elif int(status[2]):
            logger.error("Invalid command sent to I2C channel.")
            raise Exception("I2C transmission error")

        elif int(status[4]):
            logger.error("SDA line is pulled low '0' before initiating a transaction.")
            raise Exception("I2C transmission error")

        elif int(status[5]):
            passed = True

        else:
            logger.error("I2C transmission error, transaction not successful! status: %s", status)
            raise Exception("I2C transmission error")

        return passed
