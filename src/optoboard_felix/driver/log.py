# pylint: disable=line-too-long, invalid-name, import-error
"""Logger module.

    Creates a filehandler for dumping the logger messages into a file.
"""
import logging
import logging.handlers
import os

class LoggerFormat(logging.Formatter):
    """Class for formatting logger with colors.

    Attributes:
        colors (str): color code for formatting
        format (str): format for the logging message
        FORMATS (dict): setting the logger level to colors selected
    """
    white = "\x1b[37;20m"
    blue = "\x1b[34;20m"
    yellow = "\x1b[33;20m"
    green = "\x1b[32;20m"
    red = "\x1b[31;20m"
    bold_red = "\x1b[31;1m"
    reset = "\x1b[0m"
    format = "%(asctime)s - %(levelname)s - %(filename)s - %(message)s"

    FORMATS = {
        logging.DEBUG: blue + format + reset,
        logging.INFO: format,
        logging.WARNING: yellow + format + reset,
        logging.ERROR: red + format + reset,
        logging.CRITICAL: bold_red + format + reset
    }

    def format(self, record):
        """Set format for message."""
        log_fmt = self.FORMATS.get(record.levelno)
        formatter = logging.Formatter(log_fmt)
        return formatter.format(record)

workspace = os.environ.get("WORKSPACE")
if workspace is not None:
    file_name = str(workspace) + '/optoboard.log'
else:
    file_name = 'optoboard.log'

fh = logging.handlers.RotatingFileHandler(file_name, mode='a', maxBytes=10000000, backupCount=5, encoding='utf-8', delay=True)
fh.setLevel(logging.DEBUG)
fh.setFormatter(LoggerFormat())

ch = logging.StreamHandler()
ch.setFormatter(LoggerFormat())
ch.setLevel(logging.INFO)

logger = logging.getLogger(__file__)    # create logger
logger.setLevel(logging.DEBUG)
logger.addHandler(ch)
logger.addHandler(fh)
