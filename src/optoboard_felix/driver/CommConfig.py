# pylint: disable=line-too-long, invalid-name, import-error

"""Dictionary with configuration of the communication tools. """

import os
import socket
from optoboard_felix.driver.log import logger

def get_ip_address(host):
    try:
        ip_address = socket.gethostbyname(host)
        return ip_address
    except socket.gaierror as e:
        logger.error(f"Error: {e}")
        return None

TX_PORT = int(os.environ["TX_PORT"]) if "TX_PORT" in os.environ else 12340
RX_PORT = int(os.environ["RX_PORT"]) if "RX_PORT" in os.environ else 12350
TX_TAG = int(os.environ["TX_TAG"]) if "TX_TAG" in os.environ else 17
RX_TAG = int(os.environ["RX_TAG"]) if "RX_TAG" in os.environ else 29
INTERFACE = str(os.environ["INTERFACE"]) if "INTERFACE" in os.environ else "127.0.0.1"
FELIX_BUS = str(os.environ["FELIX_BUS"]) if "FELIX_BUS" in os.environ else "/bus"
VERBOSE_LEVEL = int(os.environ["VERBOSE_LEVEL"]) if "VERBOSE_LEVEL" in os.environ else 3

CommConfig = { "IChandler_config": {"ip": get_ip_address(INTERFACE), "tx_port": TX_PORT, "rx_port": RX_PORT, "tx_tag": TX_TAG, "rx_tag": RX_TAG}, 
              "lpgbt_to_felixStar_config": {"ip": get_ip_address(INTERFACE), "bus_dir": FELIX_BUS, "tx_tag": TX_TAG, "rx_tag": RX_TAG, "verbose_level": VERBOSE_LEVEL}, }