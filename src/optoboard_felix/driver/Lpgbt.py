# pylint: disable=line-too-long, invalid-name, import-error
"""Lpgbt module."""

import time
from time import sleep
import requests
import os
import json
#from alive_progress import alive_bar
from optoboard_felix.driver.Hardware import Hardware, ordered_configuration
from optoboard_felix.driver.log import logger, LoggerFormat

try:
    from optoboard_felix.register_maps import lpgbt_register_map_v0, lpgbt_register_map_v1
except (ImportError, ModuleNotFoundError):
    logger.critical("Problem with import of lpGBT register maps!")

try:
    import matplotlib.pyplot as plt
except (ImportError, ModuleNotFoundError):
    logger.critical("Problem with the import of matplotlib! Some function might not work properly!")
try:
    import math
except (ImportError, ModuleNotFoundError):
    logger.critical("Problem with the import of math! Some function might not work properly!")

try:
    import numpy as np
except (ImportError, ModuleNotFoundError):
    logger.critical("Problem with the import of numpy! Some function might not work properly!")
try:
    from collections import Counter
except (ImportError, ModuleNotFoundError):
    logger.critical("Problem with the import of Counter! Some function might not work properly!")
try:
    import csv
except (ImportError, ModuleNotFoundError):
    logger.critical("Problem with the import of csv! Some function might not work properly!")


class Lpgbt(Hardware):
    """Class for controlling the lpGBTs on an Optoboard.

    Attributes:
        device_type (str): "lpgbt"
    """

    device_type = "lpgbt"

    def __init__(self, Communication_wrapper, device, address, master, I2C_master,
                config_file, optoboard_serial, optoboard_v, lpgbt_v, efused):
        """Initialize of lpGBT object.

        Args:
            Communication_wrapper (class): low level access to FELIX access
            device (str): lpgbt1, lpgbt2, lpgbt3, lpgbt4
            address (hex): I2C address of the lpGBT
            master (bool): True if lpGBT master
            I2C_master (int): I2C controller to use (can be 0, 1, 2)
            config_file (dict): configuration values from JSON config
            optoboard_serial (str): provided Optoboard serial number
            optoboard_v (float): Optoboard serial
            lpgbt_v (int): lpGBT version
            efused (int): 1 if Optoboard is efused

        Attributes:
            lpgbt_v (int): lpGBT version
            optoboard_serial (str): provided Optoboard serial number
            optoboard_v (float): Optoboard serial
            efused (int): 1 if Optoboard is efused
            state (str): PUSM status of lpGBT, None if lpGBT not configured
            ready (bool): True if PUSM status is ready, Flase if anything else
            lpgbt_reg_map (class): lpGBT register map imported
            master (bool): True if lpGBT master
            config_file (dict): configuration values from JSON config
        """
        super().__init__(Communication_wrapper, device, address, master, I2C_master, config_file)

        self.lpgbt_v = lpgbt_v
        self.optoboard_serial = optoboard_serial
        self.optoboard_v = optoboard_v
        self.lpgbt_calibration = {}
        self.efused = efused
        self.state = None
        self.ready = False
        self.last_PTAT_measurement = [None,None]
        self.default_value_register = None
        
        if self.lpgbt_v:
            self.lpgbt_reg_map = lpgbt_register_map_v1.LpgbtRegisterMapV1
            try:
                path_default_value = "lpgbt_v1_default.json"
                path_default_value = os.path.join(os.path.dirname(os.path.abspath(__file__)), path_default_value)
                with open(path_default_value) as f:
                    self.default_value_register = json.load(f)
                self.default_value_register = self.default_value_register["non_zero_default_register"]
                if master:
                    self.default_value_register = self.default_value_register["master"]
                else:
                    self.default_value_register = self.default_value_register["slave"]
            except FileNotFoundError:
                logger.error("Default values for lpgbt v1 not found!")
        else:
            self.lpgbt_reg_map = lpgbt_register_map_v0.LpgbtRegisterMapV0

        if self.master:

            self.config_file["power_up_registers_master"] = ordered_configuration(self.config_file["power_up_registers_master"], self.lpgbt_reg_map)
            self.config_file.pop("power_up_registers_slave")
            self.config_file.pop("gbcr")

        else:

            self.config_file["power_up_registers_slave"] = ordered_configuration(self.config_file["power_up_registers_slave"], self.lpgbt_reg_map)
            self.config_file.pop("power_up_registers_master")
            self.config_file.pop("gbcr")

        logger.info("%s object initialised!", self.device)


    def configure(self):
        """Configure lpGBT according to power_up_registers_* list in config JSON.
            If this function is used outside opto.configure(), the secondary lpgbt might need that the master lpgbt enables their clock via those type of registers: EPCLK2CHNCNTRH, EPCLK2CHNCNTRL
        Has some conditions on individual Optoboards as there might have been problems during efusing

        Returns:
            ready (bool): True if lpGBT ready
        """
        logger.info("Configuring lpGBT%s", self.device_nr)
        
        if ((self.optoboard_serial=="08000000") or ("524d" in self.optoboard_serial) or (self.optoboard_v==1)) and self.master:        # condition because wrongly efused bits, this will force the lpgbt back into configuration mode
            logger.info("Force PUSM state to PAUSE_FOR_DLL_CONFIG due to efusing errors..")
            self.write_read("POWERUP4", None, 0xa3)
            self.write_read("POWERUP3", None, 0x8d)      # PUSMState set to 13 (PAUSE_FOR_DLL_CONFIG), see lpgbt manual ch. 8.1

        if not self.efused:
            if self.master:
                for reg in self.config_file["power_up_registers_master"]:
                    self.write_read(reg, None, self.construct_reg_data(reg))

                logger.info("Enable eclock output for GBCR%s", self.device_nr)
                self.write_list(["EPCLK5CHNCNTRH", "EPCLK5CHNCNTRL"])
            else:
                logger.info("Resetting lpGBT%s..", self.device_nr)
                self.write_read("POWERUP4", None, 0xa3)
                self.write_read("POWERUP3", None, 0x80, skip_retry=False)
                sleep(0.001)

                status = self.read("PUSMSTATUS", "PUSMSTATE")
                time_zero = time.time()
                while status < 10:
                    status = self.read("PUSMSTATUS", "PUSMSTATE")
                    logger.info("Waiting for lpGBT%s PLL lock, current status: %s", self.device_nr,status)
                    if time.time()-time_zero > 5:
                        logger.error("lpGBT%s PLL lock not reached in time, exiting..", self.device_nr)
                        raise Exception("lpGBT PLL lock not reached in time!")
                
                logger.debug("configure() - lpGBT%s has status: %s", self.device_nr, status)

                self.write_list(self.config_file["power_up_registers_slave"])


                logger.info("Enable eclock output for GBCR%s", self.device_nr)
                self.write_list(["EPCLK5CHNCNTRH", "EPCLK5CHNCNTRL"])

                logger.info("Releasing forced PUSM state..")
                self.write_read("POWERUP4", None, 0)
                self.write_read("POWERUP3", None, 0)
                

        if "524d" in self.optoboard_serial and self.master:      # set e-port receiver trackmode to setting stored in the config files (Optoboards with serial 524dxxxx are wrongly efused to fixed phase)
            logger.info("Changing wrongly efused trackmode bit to the config values..")
            for group in range(0,6):
                self.write_read("EPRX"+str(group)+"CONTROL", "EPRX"+str(group)+"TRACKMODE", self.config_file["lpgbt"]["EPRX"+str(group)+"CONTROL"]["EPRX"+str(group)+"TRACKMODE"][0])

        if self.master:
            self.configure_I2C_controller()

        if (self.optoboard_serial=="08000000") or ("524d" in self.optoboard_serial) or (self.optoboard_v==1) and self.master:
            logger.info("Releasing forced PUSM state..")
            self.write_read("POWERUP4", None, 0)
            self.write_read("POWERUP3", None, 0)

        logger.info("lpGBT%s configured!", self.device_nr)

        check = self.check_PUSM_status()

        self.state = check["PUSM_state"]
        self.ready = check["PUSM_ready"]

        return self.ready

    def reset_all_GBCR(self):
        """
        Send a pulse to the GBCR reset signal
        """
        logger.info("Sending a pulse to the GBCR reset signal")
        self.write_read("PIOPULLENAH", None, 0b11111111 )
        self.write_read("PIOPULLENAL", None, 0b11111111)

        self.write_read("PIODIRH", None, 0b11111111 )
        self.write_read("PIODIRL", None, 0b11111111)

        self.write_read("PIOUPDOWNH", None, 0b11111111)
        self.write_read("PIOUPDOWNL", None, 0b11111111)
        time.sleep(0.001)

        self.write_read("PIOOUTH",None, 0)
        self.write_read("PIOOUTL",None, 0)
        time.sleep(0.001)
        self.write_read("PIOOUTH", None,0b11111111)
        self.write_read("PIOOUTL", None, 0b11111111)
        logger.info("Done")

    def reset_VTRX(self):
        """
        Send a pulse to the VTRX reset signal via the RTS2 register"""
        logger.info("Sending a pulse to the VTRX reset signal")
        v = self.read("RST2", None)
        self.write_read("RST2", None, v | 0b01000000)
        time.sleep(0.001)
        self.write_read("RST2", None, v)

    
    def configure_I2C_controller(self):
        """Set the I2C controller settings of the lpGBT master and generate a reset pulse on it.

        Returns:
            configured (bool): True if lpGBT slave I2C controller configured
        """
        configured = False
        if not self.master:
            logger.warning("No need to configure I2C controller on lpgbt slave! Exiting..")
            return configured

        logger.info("Configuring I2C controller settings on I2C master %s and reset..", self.I2C_master)

        # configuring I2C settings on I2C master
        self.write_read("I2CM" + str(self.I2C_master) + "CONFIG", None, self.construct_reg_data("I2CM" + str(self.I2C_master) +"CONFIG"))
        configured = True
        # resetting I2C master
        logger.info("Reset the I2C master: generating a pulse 0->1->0 on bit RSTI2CM%s", self.I2C_master)

        self.write_read("RST0", "RSTI2CM" + str(self.I2C_master), 0)
        self.write_read("RST0", "RSTI2CM" + str(self.I2C_master), 1)
        self.write_read("RST0", "RSTI2CM" + str(self.I2C_master), 0)

        logger.info("I2C controller %s settings done", self.I2C_master)

        return configured


    def bert(self, eprx_group, bertmeastime, bertsource_c=6):
        """Perform a Bit Error Rate Test (BERT).

        Please consult the lpGBT manual ch. 14 or https://optoboard-system.docs.cern.ch/hardware/optoboard/lpgbt/#bit-error-rate-tests-bert for more information on the arguments and procedure.
        For low limits the BERT_error_count can be devided by 3 acc. to the lPGBT manual. The limit is calculated with upper limits from Statistics by R. J. Barlow (page 133).

        Args:
            eprx_group (int): EPRX group number
                if EPRX link => coarse source bertsource_g = eprx_group + 1,
                if EPTX link => bertsource_g = eprx_group + 9
            bertsource_c (int): fine source for BERT
                if EPRX link => bertsource_c should be set to 6 (always channel 0) for 1.28 Gb/s,
                if EPTX link => bertsource_c should be 4 (channel 0) or 5 (channel 2)
            bertmeastime (int): controls the duration of the BERT, from 0 to 15 possible
                defaults to 11, which relates to approx. 3 seconds

        Returns:

            dict with the following keys and values:

            total_time (float): approx. time for BERT in seconds
            total_bits (int): amount of bits measured
            BERT_error_count (int): amount of errors detected
            BER_limit (float): limit calculated,
                if BERT_error_count < 11 => limit = poisson_upper_limits[BERT_error_count/3]/total_bits,
                if BERT_error_count > 10 => BERT_error_count + 1.96*(BERT_error_count)**(1/2) according to https://en.wikipedia.org/wiki/97.5th_percentile_point (Gaussian approximation)

        Raises:
            ValueError: BERT checker disabled
            ValueError: only channel 6 is used for 1.28 Gb/s uplinks
            NameError: downlink BERT on lpGBT slaves, downlinks only on lpGBT master
            ValueError: up-/downlink EC and downlink serializer not supported
            Exception: according to the lpGBT status check no data is arriving to the selected elink
        """
        poisson_upper_limits = [3.0, 4.74, 6.3, 7.75, 9.15, 10.51, 11.84, 13.15, 14.43, 15.71, 16.96]  # from Statistics by Barlow, p. 133
        bertsource_g = eprx_group + 1
        logger.info("Starting BERT on %s..", self.device)
       
        logger.debug("BERT parameters - device: %s, bertsource_g (EPRX+1): %s, bertsource_c: %s, bertmeastime: %s", self.device, bertsource_g, bertsource_c, bertmeastime)

        if bertsource_g == 0:
            logger.error("EPRX: %d, BERT checker is disabled", eprx_group)
            raise ValueError("BERT checker is disabled!")

        if bertsource_g < 6 and not bertsource_c==6:
            logger.error("bertsource_c: %d, channel 1-3 are not available for 1.28 Gb/s elinks.", bertsource_c)
            raise ValueError("Wrong bertsource_c, channel 1-3 are not connected for 1.28 Gb/s elinks.")

        BERT_error_count = -1

        T_clock = 25*10**(-9)           # 40 MHz clock period [s]

        # bits/clock cycle for uplink data rates
        if  bertsource_g <= 6:
            if bertsource_c==6:
                bits_per_clock_cycle = 32
            elif (bertsource_c in [4, 5]):
                bits_per_clock_cycle = 16
            else:
                bits_per_clock_cycle = 8

        # bits/clock cycle for downlink data rates
        elif (9 <= bertsource_g <= 12):
            if self.device != "lpgbt1":
                raise NameError("Can not do BERT for downlink on lpGBT slaves, downlinks only come from lpGBT1 (master)!")
            elif bertsource_c==6:
                bits_per_clock_cycle = 8
            elif (bertsource_c in [4, 5]):
                bits_per_clock_cycle = 4
            else:
                bits_per_clock_cycle = 2

        else:
            logger.error("bertsource_g: %d, Uplink/downlink data group EC and downlink deserializer frame not supported", bertsource_g)
            raise ValueError("Up/downlink EC or downlink deserializer frame not supported!")

        total_bits = 2**(5+2*bertmeastime)*bits_per_clock_cycle

        runtime_expected = 2**(5+2*bertmeastime)*T_clock        # amount of clock cycles * period of clock [s]

        logger.info("Expected runtime: %.2f s", runtime_expected)

        # start measurement
        t_start = time.time()   # start timer

        self.write_read("BERTSOURCE", None, (bertsource_g << 4) | bertsource_c)
        self.write_read("BERTCONFIG", "bertmeastime", bertmeastime)
        self.write_read("BERTCONFIG", "BERTSTART", 1)

        while True:
            status = bin(self.read("BERTStatus", None))[2:].zfill(8)

            logger.debug("while loop - status: %s", status)

            if int(status[7]):
                total_time = time.time()-t_start

                logger.info("BERT finished, saving results..")

                BERT_error_count = (self.read("BERTRESULT4", None) << 32) | (self.read("BERTRESULT3", None) << 24) | (self.read("BERTRESULT2", None) << 16) | (self.read("BERTRESULT1", None) << 8) | (self.read("BERTRESULT0", None))

                logger.info("Stopping measurement..")
                self.write_read("BERTCONFIG", None, 0)

                break

            elif int(status[5]):

                logger.error("BERT error flag (there was no data on the input group %s channel %s). Stopping measurement..", bertsource_g, bertsource_c)
                self.write_read("BERTCONFIG", None, 0)

                raise Exception("No data arrived at the pattern checker.")

        if BERT_error_count<11:
            mu = poisson_upper_limits[BERT_error_count]

        else:
            mu = BERT_error_count + 1.96*(BERT_error_count)**(1/2)	# we believe we can approximate it with this function.

        BER_limit = mu/total_bits

        if self.lpgbt_v:
            if BER_limit < 0.001:

                BERT_error_count = int(BERT_error_count/3)		# see docstring on why divided by 3

                if BERT_error_count<11:
                    mu = poisson_upper_limits[BERT_error_count]
                else:
                    mu = BERT_error_count + 1.96*(BERT_error_count)**(1/2)

                BER_limit = mu/total_bits

        logger.info(f"Total time: {total_time} s, total bits: {total_bits}, error bits: {BERT_error_count}, BER limit (95%% confidence interval) {BER_limit}")
        logger.info("BERT done!")

        return {"EPRX": eprx_group, "total_time": total_time, "total_bits": total_bits, "BERT_error_count": BERT_error_count, "BER_limit": BER_limit}

    def bert_internal(self, eprx_group, bertmeastime, bertsource_c=6):
        """Perform an internal BERT by generating the PRBS7 with the lpGBT pattern generator.

        Args:
            eprx_group (int): EPRX group number,
                if EPRX link => coarse source bertsource_g = eprx_group + 1,
                if EPTX link => bertsource_g = eprx_group + 9
            bertsource_c (int): fine source for BERT,
                if EPRX link => bertsource_c should be set to 6 (always channel 0) for 1.28 Gb/s,
                if EPTX link => bertsource_c should be 4 (channel 0) or 5 (channel 2)
            bertmeastime (int): controls the duration of the BERT, from 0 to 15 possible.
                defaults to 11, which relates to approx. 3 seconds

        Returns:

            dict with the following keys and values:

            total_time (float): approx. time for BERT in seconds
            total_bits (int): amount of bits measured
            BERT_error_count (int): amount of errors detected
            BER_limit (float): limit calculated,
                if BERT_error_count < 11 => limit = poisson_upper_limits[BERT_error_count/3]/total_bits,
                if BERT_error_count > 10 => BERT_error_count + 1.96*(BERT_error_count)**(1/2) according to https://en.wikipedia.org/wiki/97.5th_percentile_point (Gaussian approximation)

        Raises:
            ValueError: selected elink parameters outside possible scope
        """
        bertsource_g=eprx_group+1
        if bertsource_g<7 and not bertsource_g==0:
            logger.info("Performing an internal BERT on EPRX%s%s..", eprx_group, bertsource_c)
            self.set_UL_elinks(eprx_group, "PRBS7")
            results = self.bert(eprx_group+1, bertsource_c, bertmeastime)
            self.set_UL_elinks(eprx_group, "reset")

        elif bertsource_g>8 and bertsource_g<13:
            if bertsource_c==4: channel = 0
            else: channel = 2
            logger.info("Performing an internal BERT on EPTX%s%s..", bertsource_g-9, channel)
            self.set_DL_elinks(bertsource_g-9, "PRBS7")
            results = self.bert(bertsource_g, bertsource_c, bertmeastime)
            self.set_DL_elinks(bertsource_g-9, "reset")

        else:
            logger.error("Coarse source %s for BERT outside possible limits (EPRX BERT: 1-6, EPTX BERT: 9-12)", bertsource_g)
            raise ValueError("Coarse source for BERT outside possible limits!")

        return results
    
    def bert_by_phase(self, eprx_group, bertmeastime, bertsource_c=6):
        """Perform a BERT while iterating through phase settings (0 to 15), assuming fixed phase mode.
        
        Args:
            eprx_group (int): EPRX group number
                if EPRX link => coarse source bertsource_g = eprx_group + 1,
                if EPTX link => bertsource_g = eprx_group + 9
            bertsource_c (int): fine source for BERT
                if EPRX link => bertsource_c should be set to 6 (always channel 0) for 1.28 Gb/s,
                if EPTX link => bertsource_c should be 4 (channel 0) or 5 (channel 2)
            bertmeastime (int): controls the duration of the BERT, from 0 to 15 possible
                defaults to 11, which relates to approx. 3 seconds

        Returns:
            (nested) dict with the following keys and values:

            phase (int): The lpGBT phase set for each BERT (keys of top level dict)
            total_time (float): approx. time for BERT in seconds
            total_bits (int): amount of bits measured
            BERT_error_count (int): amount of errors detected
            BER_limit (float): limit calculated,
                if BERT_error_count < 11 => limit = poisson_upper_limits[BERT_error_count/3]/total_bits,
                if BERT_error_count > 10 => BERT_error_count + 1.96*(BERT_error_count)**(1/2) according to https://en.wikipedia.org/wiki/97.5th_percentile_point (Gaussian approximation)
        """
        group = str(eprx_group)
        original_mode = self.read("EPRX"+group+"CONTROL", "EPRX"+group+"TRACKMODE")
        
        if original_mode != 0:
            self.write_read("EPRX"+group+"CONTROL", "EPRX"+group+"TRACKMODE",0)
        
        phase_dict = {phase: None for phase in range(0,16)}
        for phase in range(0,16):
            logger.info("Switching to phase: " + str(phase))
            self.write_read("EPRX"+group+"0ChnCntr", "EPRX"+group+"0PhaseSelect", phase)
            result = self.bert(eprx_group, bertmeastime, bertsource_c)
            phase_dict[phase] = result
            
        if original_mode != 0:
            self.write_read("EPRX"+group+"CONTROL", "EPRX"+group+"TRACKMODE",original_mode)
        else:
            logger.warning("The reset to the initial phase is not performed!")
        logger.info(phase_dict)
        return phase_dict
            
        
    def bert_by_elink(self, elink, bertmeastime=11):
        """Execute a BERT with short notation.

        Args:
            elink (str): short notation, provide EPTXGC or EPRXGC where G group and C channel
            bertmeastime (int, optional): controls the duration of the BERT, from 0 to 15 possible.

        Returns:

            dict with the following keys and values:

            total_time (float): approx. time for BERT in seconds
            total_bits (int): amount of bits measured
            BERT_error_count (int): amount of errors detected
            BER_limit (float): limit calculated,
                if BERT_error_count < 11 => limit = poisson_upper_limits[BERT_error_count/3]/total_bits,
                if BERT_error_count > 10 => BERT_error_count + 1.96*(BERT_error_count)**(1/2) according to https://en.wikipedia.org/wiki/97.5th_percentile_point (Gaussian approximation)

        Raises:
            ValueError: elink not possible
        """
        poss_elinks = ["EPRX00", "EPRX10", "EPRX20", "EPRX30", "EPRX40", "EPRX50",
                    "EPTX00", "EPTX02", "EPTX10", "EPTX12", "EPTX20", "EPTX22", "EPTX30", "EPTX32"]
        if not elink in poss_elinks:
            logger.error("Wrong elink string provided: %s, must be %s!", elink, poss_elinks)
            raise ValueError("Wrong elink string provided!")

        group = int(elink[4])
        channel = int(elink[5])
        rx = True
        if elink[2] == "T":
            rx = False

        if rx:
            bertsource_g = group + 1
            bertsource_c = 6

        else:
            bertsource_g = group + 9

            if channel == 0: bertsource_c = 4
            else: bertsource_c = 5

        return self.bert(bertsource_g, bertsource_c, bertmeastime)

    def get_lpgbt_info(self):
        """Get lpGBT CHIPID and USERID.

        The IDs can be unreliable due to efuse problems (see ch. 3.7 lpGBTv1 manual)

        Returns:

            dict with the following keys and values:

            CHIPID (hex): given by the lpGBT team
            USERID (hex): serial of Optoboard if efused by Bern
        """
        logger.info("Getting %s information..", self.device)

        CHIPID0 = self.read("CHIPID0", None)
        CHIPID1 = self.read("CHIPID1", None)
        CHIPID2 = self.read("CHIPID2", None)
        CHIPID3 = self.read("CHIPID3", None)

        CHIPID = CHIPID0 << 24 | CHIPID1 << 16 | CHIPID2 << 8 | CHIPID3

        logger.info("%s CHIPID: %s (%s)", self.device, CHIPID, hex(CHIPID))

        USERID0 = self.read("USERID0", None)
        USERID1 = self.read("USERID1", None)
        USERID2 = self.read("USERID2", None)
        USERID3 = self.read("USERID3", None)

        USERID = USERID0 << 24 | USERID1 << 16 | USERID2 << 8 | USERID3

        logger.info("%s USERID: %s (%s)", self.device, USERID, hex(USERID))

        return {"CHIPID" : hex(CHIPID), "USERID" : hex(USERID)}


    def get_PLL_DLL_timeout(self):
        """read the number of PLL /DLL Timeout Action Occurrences

        Returns:

            dict with the following keys and values:

            PLLTIMEOUT (hex): number of PLL Timeout Action Occurrences
            DLLTIMEOUT (hex): number of DLL Timeout Action Occurrences
        """
        logger.info("Getting %s information..", self.device)

        PLLTIMEOUT = self.read("PUSMPLLTIMEOUT", None)
    
        logger.info("%s PLLTIMEOUT: %s (%s)", self.device, PLLTIMEOUT, hex(PLLTIMEOUT))

        DLLTIMEOUT = self.read("PUSMDLLTIMEOUT", None)
    
        logger.info("%s DLLTIMEOUT: %s (%s)", self.device, DLLTIMEOUT, hex(DLLTIMEOUT))

        return {"PLLTIMEOUT " : hex(PLLTIMEOUT ), "DLLTIMEOUT" : hex(DLLTIMEOUT)}

    def check_PUSM_status(self):
        """Check lpGBTs power up state machine (PUSM) status.

        Returns:
            dict with the following keys and values:

            PUSM_state (int): state of the PUSM
            PUSM_ready (bool): True if lpGBT ready
        """
        state = self.read("PUSMSTATUS", "PUSMSTATE")
        ready = False

        PUSM2STR_V0 = ["ARESET", "RESET", "WAIT_VDD_STABLE", "WAIT_VDD_HIGHER_THAN_0V90",
            "FUSE_SAMPLING", "UPDATE_FROM_FUSES", "PAUSE_FOR_PLL_CONFIG", "WAIT_POWER_GOOD",
            "RESETOUT", "I2C_TRANS", "RESET_PLL", "WAIT_PLL_LOCK", "INIT_SCRAM",
            "PAUSE_FOR_DLL_CONFIG", "RESET_DLLS", "WAIT_DLL_LOCK",
            "RESET_LOGIC_USING_DLL", "WAIT_CHNS_LOCKED", "READY"]

        PUSM2STR_V1 = ["ARESET", "RESET1", "WAIT_VDD_STABLE", "WAIT_VDD_HIGHER_THAN_0V90",
            "COPY_FUSES", "CALCULATE_CHECKSUM", "COPY_ROM", "PAUSE_FOR_PLL_CONFIG_DONE", "WAIT_POWER_GOOD",
            "RESET_PLL", "WAIT_PLL_LOCK", "INIT_SCRAM", "RESETOUT", "I2C_TRANS",
            "PAUSE_FOR_DLL_CONFIG_DONE", "RESET_DLLS", "WAIT_DLL_LOCK",
            "RESET_LOGIC_USING_DLL", "WAIT_CHNS_LOCKED", "READY"]

        if self.lpgbt_v:
            if state==19:
                logger.info("%s status is %s%s%s", self.device, LoggerFormat.green, PUSM2STR_V1[state], LoggerFormat.reset)
                ready = True
            else:
                logger.info("%s status is %s%s%s", self.device, LoggerFormat.red, PUSM2STR_V1[state], LoggerFormat.reset)

        else:
            if state==18:
                logger.info("%s status is %s%s%s", self.device, LoggerFormat.green, PUSM2STR_V0[state], LoggerFormat.reset)
                ready = True
            else:
                logger.info("%s status is %s%s%s", self.device, LoggerFormat.red, PUSM2STR_V0[state], LoggerFormat.reset)

        self.state = state

        return {"PUSM_state": state, "PUSM_ready" : ready}

    def check_lpgbt_config_pins(self):
        """Check lpGBTs hardware configuration.

        Checks for the transceiver mode and lock mode of lpGBT. For the Optoboard:
        - lpGBT master should be in transceiver 10.24 Gbps FEC12 mode and lock mode 1 (recover clock from data stream)
        - lpGBT slaves should be in transmitter 10.24 Gbps FEC12 mode and lock mode 0 (recover from clock)
        - BootConfig should be 0 (load from efuses) or 1 (load from lpGBT ROM)

        Note: these pins/modes can only be changed by changing resistors!

        Returns:

            dict with the following keys and values:

            BootConfig_ready (bool): True if 0 or 1
            Mode_ready (bool): True if correct mode
            LockMode_ready (bool): True if correct LockMode
        """
        logger.info("Checking %s hardware configuration..", self.device)

        MODE2STR = ["OFF", "TX 5.12Gbps FEC5", "RX 5.12Gbps FEC5", "TRX 5.12Gbps FEC5",
                "OFF", "TX 5.12Gbps FEC12", "RX 5.12Gbps FEC12", "TRX 5.12Gbps FEC12",
                "OFF", "TX 10.24Gbps FEC5", "RX 10.24Gbps FEC5", "TRX 10.24Gbps FEC5",
                "OFF", "TX 10.24Gbps FEC12", "RX 10.24Gbps FEC5", "TRX 10.24Gbps FEC12"]

        LPGBTMode = self.read("CONFIGPINS", "LPGBTMODE")
        LockMode = self.read("CONFIGPINS", "LOCKMODE")

        if self.lpgbt_v:
            BootConfig = self.read("CONFIGPINS", "BOOTCONFIG")
            if BootConfig<2:
                logger.info("%s BOOTCNF is %s%s%s", self.device, LoggerFormat.green, BootConfig, LoggerFormat.reset)
                BootConfig_ready = True
            else:
                logger.info("%s BOOTCNF is %s%s%s", self.device, LoggerFormat.red, BootConfig, LoggerFormat.reset)
                BootConfig_ready = False

        else:
            ConfigSelect = self.read("CONFIGPINS", "CONFIGSELECT")    # state of SC_I2C pin
            logger.info("%s SC_I2C is %s%s%s", self.device, LoggerFormat.red, ConfigSelect, LoggerFormat.reset)


        # lpGBT master MODE should be transceiver 10.24Gbps FEC12
        if self.master and LPGBTMode==15:
            logger.info("%s mode is %s%s%s", self.device, LoggerFormat.green, MODE2STR[LPGBTMode], LoggerFormat.reset)
            Mode_ready = True
        # lpGBT slave MODE should be transmitter 10.24Gbps FEC12, LOCKMODE 0 (external clock)
        elif not self.master and LPGBTMode==13:
            logger.info("%s mode is %s%s%s", self.device, LoggerFormat.green, MODE2STR[LPGBTMode], LoggerFormat.reset)
            Mode_ready = True
        else:
            logger.info("%s mode is %s%s%s", self.device, LoggerFormat.red, MODE2STR[LPGBTMode], LoggerFormat.reset)
            Mode_ready = False


        # lpGBT master LockMode should be 1 (reference-less, recover from data stream)
        if self.master and LockMode==1:
            logger.info("%s LockMode is %s%s%s", self.device, LoggerFormat.green, LockMode, LoggerFormat.reset)
            LockMode_ready = True
        # lpGBT slave LockMode should be 0 (external clock)
        elif not self.master and LockMode==0:
            logger.info("%s lock mode is %s%s%s", self.device, LoggerFormat.green, LockMode, LoggerFormat.reset)
            LockMode_ready = True
        else:
            logger.info("%s lock mode is %s%s%s", self.device, LoggerFormat.red, LockMode, LoggerFormat.reset)
            LockMode_ready = False

        return {"BootConfig_ready" : BootConfig_ready, "Mode_ready" : Mode_ready, "LockMode_ready" : LockMode_ready}

    def check_EPRX_locking(self):
        """Check lpGBTs elink uplink phase locking for all channels.

        Returns:

            Nested dictionaries with keys:
            `EPRX_group0`, `EPRX_group1`, `EPRX_group2`, `EPRX_group3`, `EPRX_group4`, `EPRX_group5`.
            Each has the following keys and values (example `EPRX_group0`):

            locking (bool): True if locked
            state (int): state of initialization state machine for EPRX00
            phase (int): current phase of lpGBT EPRX00
        """
        logger.info("Checking %s EPRX phase locking..", self.device)
        dict_EPRX_locking = {}

        STATE2STR = ["Reset", "Force down", "Confirm early state", "Free running state"]

        for G in range(0,6):

            G_str = str(G)
            dict_EPRX_locking["EPRX_group" + G_str] = {}

            eprxlocked = self.read("EPRX"+G_str+"LOCKED", None)

            phase_lock = eprxlocked >> 4	# get bits 7:4
            state = eprxlocked & 3		# get bits 1:0
            group, lock_bool, phase = self.check_EPRX_phase(G)

            if int(bin(phase_lock)[-1]):
                dict_EPRX_locking["EPRX_group" + G_str]["locking"] = True
                dict_EPRX_locking["EPRX_group" + G_str]["state"] = state
                dict_EPRX_locking["EPRX_group" + G_str]["phase"] = phase
                logger.info("%s e-link %s (EPRX group %s channel 0) is %slocked%s with phase %s. State: %s", self.device, G_str, G_str, LoggerFormat.green, LoggerFormat.reset, phase, STATE2STR[state])
            else:
                dict_EPRX_locking["EPRX_group" + G_str]["locking"] = False
                dict_EPRX_locking["EPRX_group" + G_str]["state"] = state
                dict_EPRX_locking["EPRX_group" + G_str]["phase"] = phase
                logger.info("%s e-link %s (EPRX group %s channel 0) is %snot locked%s with phase %s. State: %s", self.device, G_str, G_str, LoggerFormat.red, LoggerFormat.reset, phase, STATE2STR[state])

        return dict_EPRX_locking

    
    def check_EPTX_status(self):
        """Check lpGBTs elink downlink status for all channels.
        
        Specifically: outputs frequency and polarity of the downlinks.
        
        Downlink numbering convention is as follows:
        dl 0 == (e-group,channel) 00
        1 == 02
        2 == 10
        3 == 12
        4 == 20
        5 == 22
        6 == 30
        7 == 32

        Returns:
            Nested dictionary with the status of the 8 downlinks with keys:
            ["downlink"+str(dl)]["polarity"] and ["downlink"+str(dl)]["data_rate"]

            polarity (string): 0=NON INVERTED, 1=INVERTED
            data_rate (int): possible values in [0,3], 2 corresponds to 160Mb/s
        """
        
        output_driver = ["01_00","03_02","11_10","13_12","21_20","23_22","31_30","33_32"]
        group = [0,0,1,1,2,2,3,3]
        channel = [0,2,0,2,0,2,0,2]
        downlink_bitrates = ["0 Mb/s", "80 Mb/s", "160 Mb/s", "320 Mb/s"]
        
        dict_downlink_status = {}

        for dl in range(0,8):
            dict_downlink_status["EPTX"+str(group[dl])+str(channel[dl])] = {}
            polarity = self.read("EPTX"+str(output_driver[dl])+"CHNCNTR","EPTX"+str(group[dl])+str(channel[dl])+"INVERT")
            if polarity:
                polarity_str = "INVERTED (1)"
            else:
                polarity_str = "NOT INVERTED (0)"
            
            dict_downlink_status["EPTX"+str(group[dl])+str(channel[dl])]["polarity"] = polarity_str 
                
            logger.info("Status of EPTX%s%s:", str(group[dl]), str(channel[dl]))
            logger.info("The polarity is %s%s%s", LoggerFormat.yellow, polarity_str, LoggerFormat.reset)
            
            data_rate = self.read("EPTXDATARATE","EPTX"+str(group[dl])+"DATARATE")
            
            dict_downlink_status["EPTX"+str(group[dl])+str(channel[dl])]["data_rate"] = data_rate 
            
            if data_rate == 2:
                logger.info("The data rate is %s%s%s", LoggerFormat.green, downlink_bitrates[data_rate], LoggerFormat.reset)
            else:
                logger.info("The data rate is %s%s%s", LoggerFormat.red, downlink_bitrates[data_rate], LoggerFormat.reset)
                    
        return dict_downlink_status
     
    def check_EPRX_DLL_locking(self):
        """Check lpGBTs elink uplink DLL locking for all channels.

        Returns:

            Nested dictionaries with keys:
            `EPRX_group0`, `EPRX_group1`, `EPRX_group2`, `EPRX_group3`, `EPRX_group4`, `EPRX_group5`.
            Each has the following keys and values (example `EPRX_group0`):

                DLL_lock (bool): True if locked
                DLL_state (int): state of lock filter state machine for EPRX00
                DLL_loss_count (int): loss of Lock counter value
        """
        logger.info("Checking %s EPRX DLL status..", self.device)
        dict_EPRX_DLL_locking = {}

        for G in range(0,6):

            G_str = str(G)
            dict_EPRX_DLL_locking["EPRX_group" + G_str] = {}

            eprxdllstatus = self.read("EPRX"+G_str+"DLLSTATUS", None)

            dll_lock = eprxdllstatus >> 7		# get bit 7
            state = (eprxdllstatus & 96) >> 5 		# get bits 6:5
            loss_counter = eprxdllstatus & 31		# get bits 4:0

            if dll_lock==1:
                dict_EPRX_DLL_locking["EPRX_group" + G_str]["DLL_lock"] = True
                dict_EPRX_DLL_locking["EPRX_group" + G_str]["DLL_state"] = state
                dict_EPRX_DLL_locking["EPRX_group" + G_str]["DLL_loss_count"] = loss_counter
                logger.info("%s e-link %s (EPRX group %s channel 0) DLL is %slocked%s", self.device, G_str, G_str, LoggerFormat.green, LoggerFormat.reset)
            else:
                dict_EPRX_DLL_locking["EPRX_group" + G_str]["DLL_lock"] = False
                dict_EPRX_DLL_locking["EPRX_group" + G_str]["DLL_state"] = state
                dict_EPRX_DLL_locking["EPRX_group" + G_str]["DLL_loss_count"] = loss_counter
                logger.info("%s e-link %s (EPRX group %s channel 0) DLL is %snot locked%s", self.device, G_str, G_str, LoggerFormat.red, LoggerFormat.reset)

        return dict_EPRX_DLL_locking

    def check_EPRX_status(self):
        """Check lpGBTs elink uplink status for all channels.

        Returns:

            Nested dictionaries with keys:
            `EPRX_group0`, `EPRX_group1`, `EPRX_group2`, `EPRX_group3`, `EPRX_group4`, `EPRX_group5`.
            Each has the following keys and values (example `EPRX_group0`):

            enable (bool): True if enabled
            data_rate (str): either 1.28 if 1.28 Gb/s or not 1.28
            polarity (str): inverted or not inverted
            selected_phase (int): selected phase for EPRX channel, this value can be different from the actual phase if channel is not in fixed phase mode!
        """
        TRACKMODE2STR = ["fixed phase", "initial training", "continuous phase tracking", "continuous phase tracking with initial training"]

        logger.info("Checking %s EPRX status..", self.device)
        dict_EPRX_status = {}

        for G in range(0,6):

            G_str = str(G)

            dict_EPRX_status["EPRX_group_" + G_str] = {}

            eprxcontrol = self.read("EPRX"+G_str+"CONTROL", None)
            EPRX_en = bool((eprxcontrol & 16) >>4)
            EPRX_data_rate = (eprxcontrol & 12) >> 2
            EPRX_track_mode = eprxcontrol & 3

            eprxchncntr = self.read("EPRX"+G_str+"0CHNCNTR", None)
            EPRX_pol = (eprxchncntr & 8) >> 3
            EPRX_phase = eprxchncntr >> 4

            dict_EPRX_status["EPRX_group_" + G_str]["enable"] = EPRX_en

            logger.info(EPRX_data_rate)
            if EPRX_en and not EPRX_data_rate==0:

                if EPRX_data_rate==3:
                    dict_EPRX_status["EPRX_group_" + G_str]["data_rate"] = "1.28"
                    logger.info("%s e-link %s (EPRX group %s channel 0) data rate is %s1.28 Gbps%s", self.device, G_str, G_str, LoggerFormat.green, LoggerFormat.reset)
                else:
                    dict_EPRX_status["EPRX_group_" + G_str]["data_rate"] = "not 1.28"
                    logger.warning("%s e-link %s (EPRX group %s channel 0) data rate is %snot 1.28 Gbps%s", self.device, G_str, G_str, LoggerFormat.red, LoggerFormat.reset)

                dict_EPRX_status["EPRX_group_" + G_str]["track_mode"] = TRACKMODE2STR[EPRX_track_mode]
                logger.info("%s e-link %s (EPRX group %s channel 0) track mode is %s%s%s", self.device, G_str, G_str, LoggerFormat.yellow, TRACKMODE2STR[EPRX_track_mode], LoggerFormat.reset)

                if EPRX_pol:
                    dict_EPRX_status["EPRX_group_" + G_str]["polarity"] = "inverted"
                    logger.info("%s e-link %s (EPRX group %s channel 0) is %sinverted%s", self.device, G_str, G_str, LoggerFormat.yellow, LoggerFormat.reset)
                else:
                    dict_EPRX_status["EPRX_group_" + G_str]["polarity"] = "not inverted"
                    logger.info("%s e-link %s (EPRX group %s channel 0) is %snot inverted%s", self.device, G_str, G_str, LoggerFormat.yellow, LoggerFormat.reset)

                dict_EPRX_status["EPRX_group_" + G_str]["selected_phase"] = EPRX_phase

            else:
                logger.info("%s e-link %s (EPRX group %s channel 0) is %snot enabled%s", self.device, G_str, G_str, LoggerFormat.red, LoggerFormat.reset)

        return dict_EPRX_status

    def bypass_ul(self):
        """Set the lpGBTs 10.24 Gb/s uplink to output a 40 MHz clock and bypass the interleaver, scrambler and FEC encoder.

        Changes the data link from the lpGBT and through the fibres to a 40 MHz clock. Useful for debugging as the clock signal can be ssen on an oscilloscope with an optical to electrical converter.

        Warning: breaks the communication if one sets it on the lpGBT master - only a power cycle will restore communication!
        """
        clock_40MHz = 8

        if self.master:
            logger.warning("You are changing the interleaved, scrambled, forward encoded 10.24 Gbps uplink of the lpgbt master to a raw 40 MHz clock pattern! This breaks communication via IC and you need to power-cycle the Optoboard to re-establish!")

        logger.info("Configuring UL data path bypassing..")

        self.write_read("DATAPATH", None, 7)

        logger.warning("%s 10.24 Gbps uplink bypassing interleaver, scrambler and FEC encoder", self.device)

        logger.warning("Changing UL serializer pattern to 40 MHz clock!")
        self.write_read("ULDATASOURCE0", "ULSERTESTPATTERN", clock_40MHz)

    def set_UL_elinks(self, group, data_source):
        """Set data source of EPRX group.

        The output of any EPRX group can be set to a known pattern i.e. PRBS7. The pattern can be analysed on the FELIX side if one extracts the elink data on FELIX.

        Note: for the CONST_PATTERN option, registers DPDataPattern0, DPDataPattern1, DPDataPattern2, DPDataPattern3 need to be set with the wanted pattern.

        Args:
            group (int): EPRX group (elink number)
            data_source (str): data source of EPRX group, possibilities:
                "EPORTRX_DATA", "PRBS7", "BIN_CNTR_UP", "BIN_CNTR_DOWN", "CONST_PATTERN", "CONST_PATTERN_INV", "DLDATA_LOOPBACK"

        Returns:
            read (int): data source index read back

        Raises:
            ValueError: pattern to select not possible
            ValueError: elink group outside scope
        """
        patterns = ["EPORTRX_DATA", "PRBS7", "BIN_CNTR_UP", "BIN_CNTR_DOWN", "CONST_PATTERN", "CONST_PATTERN_INV", "DLDATA_LOOPBACK"]

        if data_source == "reset":
            data_source = "EPORTRX_DATA"

        if data_source not in patterns:
            logger.error("Can not set %s for uplinks, not in %s", data_source, patterns)
            raise ValueError("Can not set uplink generator to chosen pattern for uplinks!")

        data_source_index = patterns.index(data_source)

        logger.info("Configure %s EPRX%s0 uplink data path to %s..", self.device, group, data_source)
        read = 0

        ulgdatasource = "ULG"+str(group)+"DATASOURCE"
        if group>= 0 and group<2:
            read = self.write_read("ULDATASOURCE1", ulgdatasource, data_source_index)
        elif group>1 and group<4:
            read = self.write_read("ULDATASOURCE2", ulgdatasource, data_source_index)
        elif group>3 and group<6:
            read = self.write_read("ULDATASOURCE3", ulgdatasource, data_source_index)
        else:
            logger.error("Group %s not allowed, 0 to 5 possible", group)
            raise ValueError("Wrong group input for setting UL elinks generator.")

        if data_source != "EPORTRX_DATA":
            logger.warning("Data path disabled, sending %s test pattern on EPRX%s0!", data_source, group)
        else:
            logger.info("%s EPRX%s0 elinks set to %sdata path%s!", self.device, group, LoggerFormat.green, LoggerFormat.reset)

        return read

    def set_DL_elinks(self, group, data_source):
        """Set EPTX data source.

        The output of any EPTX group can be set to a known pattern i.e. PRBS7. The pattern can be analysed on the module side.

        Note: for the CONST_PATTERN option, registers DPDataPattern0, DPDataPattern1, DPDataPattern2, DPDataPattern3 need to be set with the wanted pattern.

        Args:
            group (int): EPTX group (elink number), possible: [0, 1, 2, 3]
            data_source (str): data source of EPTX group, possibilities:
                "LINK_DATA", "PRBS7", "BIN_CNTR_UP, "CONST_PATTERN"

        Returns:
            read (int): data source index read back

        Raises:
            ValueError: pattern to select not possible
            ValueError: elink group outside scope
        """
        patterns = ["LINK_DATA", "PRBS7", "BIN_CNTR_UP", "CONST_PATTERN"]

        if data_source == "reset":
            data_source = "LINK_DATA"

        if data_source not in patterns:
            logger.error("Can not set %s for downlinks, not in %s", data_source, patterns)
            raise ValueError("Can not set downlink generator to chosen pattern for downlinks!")

        if group > 3:
            logger.error("Group %s not allowed, 0 to 5 possible", group)
            raise ValueError("Wrong group input for setting UL elinks generator.")

        data_source_index = patterns.index(data_source)

        logger.info("Configure %s EPTX%s0 and EPTX%s2 downlink data path to %s..", self.device, group, group, data_source)

        read = self.write_read("ULDATASOURCE5", "DLG"+str(group)+"DATASOURCE", data_source_index)

        if data_source != "LINK_DATA":
            logger.warning("Data path disabled, sending %s test pattern on EPTX%s0 and EPTX%s2 downlink elinks!", data_source, group, group)
        else:
            logger.info("%s EPTX%s0 and EPTX%s2 elinks set to %sdata path%s!", self.device, group, group, LoggerFormat.green, LoggerFormat.reset)

        return read

    def read_adc(self, adcchannel, VREFTune=56,number_of_reading=1,verbose = True):
        """Read the ADC channel of the lpGBT.

        Time-out set to 5 s.

        Args:
            adcchannel (int): ADC channel to measure, from 0 to 15
            VREFTune (int, optional): Tuning value to select the optimal voltage reference generator
            verbose (boolean): if True, print all the steps (ADC value)
        Returns:
            adcval (int): ADC value of the channel

        Raises:
            ValueError: wrong ADC channel input
            TimeoutError: ADC conversion takes to long
        """
        assert number_of_reading>0, "The number of measurements should be positive"
        if adcchannel > 15 or adcchannel < 0:
            logger.error("ADC channel %s not available, 0 to 15 possible.", adcchannel)
            raise ValueError("ADC channel not available!")

        # Configure input multiplexers to measure ADC in single ended modePins
        ADCInPSelect = adcchannel
        ADCInNSelect = 15			# always VREF/2 or 15 because only single ended and internal mode on Opto V1
        self.write_read("ADCSELECT", None, (ADCInPSelect << 4) | ADCInNSelect) # ADCSelect

        # enable ADC core and set gain of the differential amplifier
        ADCEnable = 1
        ADCGainSelect = 0			# 0: x2, 1: x8, 2: x16, 3: x32
        self.write_read("ADCCONFIG", None, (ADCEnable << 2) | ADCGainSelect) # Enable ADC and select gain (ADCConfig)
        
        VREFEnable = 1
        #VREFTune = 56	# from fig 18.3 of lpGBTv0 manual

        # Enable internal voltage reference
        #self.write_read("VREFCNTR", None, (VREFEnable << 7) | VREFTune) # Enable internal voltage reference (VREFCNTR), VREFEnable (1), VREFTune (Tuning 0x38)
        self.write_read("VREFCNTR", "VREFEnable", VREFEnable)
        self.write_read("VREFTUNE", None, VREFTune)

        # Wait until voltage reference is stable
        sleep(0.01)

        # Start ADC conversion, enable ADC core and set gain of the differential amplifier
        ADCConvert = 1
        self.write_read("ADCCONFIG", None, (ADCConvert << 7) | (ADCEnable << 2) | ADCGainSelect) # Start conversion (ADCConfig)

        timeout_time = time.time() + 5
        adc_val = []
        for i in range(number_of_reading):
            while True:
                adc_done = self.read("ADCSTATUSH", "ADCDone")
                if adc_done:
                    adc_val_H = self.read("ADCSTATUSH", None) & 3		# take only bits 1:0 from the register ADCStatusH (ADCValue[9:8])
                    adc_val_L = self.read("ADCSTATUSL", None)		# ADCValue[7:0]
                    adc_val.append( (adc_val_H << 8) | adc_val_L)		# calculate ADCValue[9:0]
                    if verbose:
                        logger.info("ADC ch. %s value: %s", adcchannel, adc_val)
                    break
                else:
                    logger.info("ADCBusy - ADC core is performing conversion")

                    if time.time() > timeout_time:
                        logger.error("Timeout while waiting for reading ADC channel, waited for 5 seconds")
                        raise TimeoutError("Timeout while waiting for reading ADC channel")
                sleep(0.1)
        return sum(adc_val)/len(adc_val)

    def phase_training(self, group):
        """Switches the mode of the EPRX group to Initial Training and does a phase training.

        See ch. 7.6.2 lpGBTv1 manual for more information.

        Args:
            group (int): Uplink EPRX data group to train phase, allowed [0, 1, 2, 3, 4, 5]

        Returns:
            locked (bool): True if channel locked after training
            phase (int): phase of channel after training
        """
        G_str = str(group)
        locked = False

        self.write_read("EPRX"+G_str+"CONTROL", "EPRX"+G_str+"TRACKMODE", 1)

        if group in [0,1]:
            self.write_read("EPRXTRAIN10", "EPRX"+G_str+"TRAIN", 1)
            self.write_read("EPRXTRAIN10", "EPRX"+G_str+"TRAIN", 0)
        elif group in [2,3]:
            self.write_read("EPRXTRAIN32", "EPRX"+G_str+"TRAIN", 1)
            self.write_read("EPRXTRAIN32", "EPRX"+G_str+"TRAIN", 0)
        elif group in [4,5]:
            self.write_read("EPRXTRAIN54", "EPRX"+G_str+"TRAIN", 1)
            self.write_read("EPRXTRAIN54", "EPRX"+G_str+"TRAIN", 0)
        else:
            logger.error("%s EPRX group %s can not be trained because it does not exist", self.device, G_str)
            return

        phase_lock = self.read("EPRX"+G_str+"LOCKED", "EPRX"+G_str+"CHNLOCKED")
        phase = self.read("EPRX"+G_str+"CURRENTPHASE10", "EPRX"+G_str+"CURRENTPHASE0")

        if int(bin(phase_lock)[-1]):
            logger.info("%s e-link %s (EPRX group %s channel 0) is %slocked%s with phase %s", self.device, G_str, G_str, LoggerFormat.green, LoggerFormat.reset, phase)
            locked = True
        else:
            logger.info("%s e-link %s (EPRX group %s channel 0) is %snot locked%s, set phase: %s", self.device, G_str, G_str, LoggerFormat.red, LoggerFormat.reset, phase)

        return locked, phase

    def fuses_read_bank(self, address, timeout=0.01):
        """Read a single bank of eFuses.

        One bank is 32 bits and contains 4 registers.

        Args:
            address (int): first register address to read (multiple of 4)
            timeout (float): timeout for read completion in seconds (default 0.01)

        Returns:
            value (int): value of the fuse bank

        Raises:
            ValueError: fuse bank address wrong
            TimeoutError: reading efuses takes too long
        """
        if address%4 != 0 or address > self.lpgbt_reg_map.I2CM0CONFIG.address:
            logger.error("Incorrect address for burn bank! (address = 0x%s)", address)
            raise ValueError("Incorrect address for burn bank!")

        _, _, address_high, address_low = self.u32_to_bytes(address)
        self.write_read("FUSEBLOWADDH", None, address_high)
        self.write_read("FUSEBLOWADDL", None, address_low)
        self.write_read("FUSECONTROL", "FUSEREAD", 1)

        timeout_time = time.time() + timeout

        while True:
            status = self.read("FUSESTATUS", "FUSEDATAVALID")

            if status:
                fuse0 = self.read("FUSEVALUESA", None)
                fuse1 = self.read("FUSEVALUESB", None)
                fuse2 = self.read("FUSEVALUESC", None)
                fuse3 = self.read("FUSEVALUESD", None)

                value = fuse0 | fuse1 << 8 | fuse2 << 16 | fuse3 << 24
                self.write_read("FUSECONTROL", None, 0)
                break

            if time.time() > timeout_time:
                self.write_read("FUSECONTROL", None,  0)
                logger.error("Timeout while waiting for reading fuses")
                raise TimeoutError("Timeout while waiting for reading fuses")

        return value

    def read_fuses(self):
        """Read all eFuse banks.

        Goes from 0x000 to 0x0ff (register CRC3)

        Returns:
            fuse_values (dict): dictionary with bank address as key and tuple as values.
        """
        if not self.ready:
            logger.warning("lpGBT not ready, reading efuses should be done if the chip is in ready mode! Reading efuses anyway..")

        fuse_values = {}

        for adr in range(0, self.lpgbt_reg_map.I2CM0CONFIG.address, 4):
            fuse_values[adr] = self.u32_to_bytes(self.fuses_read_bank(adr))

        return fuse_values

    def u32_to_bytes(self, val):
        """Convert 32 bits to 4 bytes.

        Args:
            val (int): value to convert to bytes

        Returns:
            byte3, byte2, byte1, byte0 (tuple): bytes of value
        """
        byte3 = (val >> 24) & 0xFF
        byte2 = (val >> 16) & 0xFF
        byte1 = (val >> 8) & 0xFF
        byte0 = (val >> 0) & 0xFF

        return (byte3, byte2, byte1, byte0)

    def eye_opening_monitor(self, meastime=10, filename="Eye_Diagram_image"):
        """Plots the eye diagram measurement of the incoming 2.56 Gb/s high speed data

        Args:
            meastime (int): measurement time, valid values between 1 and 10
            filename (str): name of the plot
        
        Returns:
            XY_values (list): list containing the normalized results of the measurement
        """

        if self.master is False:
            raise Exception("eyediagram_monitor is only available on lpGBT1")
        
        XY_values = []
        if (meastime < 1) or (meastime > 10):
            logger.warning("Invalid value, resetting to 10") 
            meastime = 10
                
        self.write_read("EOMCONFIGH", None, 1 | meastime << 4)
        
        XY_sum = []
        logger.warning("If ic.master starts returning: resending 2/3 ... , please restart FELIX container.")
    #with alive_bar(1984) as bar:     
        logger.info("Creating eye diagram...")        
        for j in range(0,31):
            xy_values = []
            self.write_read("EOMVOFSEL", None, j)
            for i in range(0, 64):
                #bar()
                self.write_read("EOMCONFIGL", None, i)
                time.sleep(0.005)
                
                self.write_read("EOMCONFIGH", "EOMSTART", 1)
                # self.write_read("EOMCONFIGH", None, 2 | (meastime << 4 | 1) ) 

                while True:
                    status = self.read("EOMSTATUS", None)
                    if not (status & 2) and (status & 1):
                        break
                    else:
                        pass
                
                counterValue = self.read("EOMCOUTERVALUEL", None) | self.read("EOMCOUTERVALUEH", None) << 8
                counter40M =  self.read("EOMCOUNTER40ML", None) | self.read("EOMCOUNTER40MH", None) << 8

                xy_values.append(counterValue/counter40M)

                self.write_read("EOMCONFIGH", None, 1 | meastime << 4)      
                # self.write_read("EOMCONFIGH", "EOMSTART", 0)
                
            XY_values.append(xy_values)
            total = np.sum(xy_values)
            XY_sum.append(total)
            
        XY_sum_max = np.max(XY_sum)
        XY_sum_max_pos = np.argmax(XY_sum)
        DC_Offset = 15 - XY_sum_max_pos
        
        plt.imshow(XY_values, origin='lower', cmap="inferno")
        plt.xlabel("Phase")
        plt.ylabel("Voltage")
        plt.colorbar(shrink=0.6)
        time_string = time.strftime("%m%d%Y_%H%M%S", time.localtime())

        workspace = os.environ.get("WORKSPACE")
        if workspace is not None:
            file_name = str(workspace) + "/" + filename
        else:
            file_name = filename

        plt.savefig(file_name)
        np.savetxt(file_name + ".txt",XY_values)
        plt.close()
        return {"DC_Offset": float(DC_Offset), "Eye_diagram_values": file_name + ".txt", "Eye_diagram": file_name + ".png"}
    
    def single_event_upset_counter(self):
        """Monitor the number of single event upsets

        Returns:
            seuCounter (int): total number of SEUs detected during monitoring        
        """
        
        # Enable SEU Counter
        self.write_read("PROCESSANDSEUMONITOR", "SEUENABLE", 1)

        try:
            while True:
                seuCounter = self.read("SEUCOUNTL", None) | (self.read("SEUCOUNTH", None) << 8)
                logger.info(f"SEU Monitor Counter : {seuCounter}")
                time.sleep(5)
        except:
            # Disable SEU Counter
            self.write_read("PROCESSANDSEUMONITOR", "SEUENABLE", 0)
        
        return seuCounter

    def rx_polarity_swap(self,group):
        """Swapping the polarity of RX (uplink) for all channels.
        This always concerns channel 0 of each e-group. 
        If inverted, change it to not inverted, and visa versa.
        
        Args:
            group (int) can be from 0 to 5
        """
        
        group_str = str(group)
        logger.info("Swapping uplink polarity of e-group %s", group_str)
        polarity = self.read("EPRX"+group_str+"0CHNCNTR","EPRX"+group_str+"0INVERT")
        
        if polarity:
            self.write_read("EPRX"+group_str+"0CHNCNTR","EPRX"+group_str+"0INVERT",0)
            logger.info("Polarity swapped from INVERTED to NOT INVERTED")
            
        else:
            self.write_read("EPRX"+group_str+"0CHNCNTR","EPRX"+group_str+"0INVERT",1)
            logger.info("Polarity swapped from NOT INVERTED to INVERTED") 
             

    def tx_polarity_swap(self,dl):
        """Swapping the polarity of TX (downlink) for all channels. 
        If inverted, change it to not inverted, and visa versa.        
        The polarity corresponds to a specific combination of e-group and channel
        number. The convention is as follows:
        dl == (e-group,channel) 
        0 == 00
        1 == 02
        2 == 10
        3 == 12
        4 == 20
        5 == 22
        6 == 30
        7 == 32

        Args:
            dl (int) can be from 0 to 7
        """
        
        output_driver = ["01_00","03_02","11_10","13_12","21_20","23_22","31_30","33_32"]
        channels = ["00","02","10","12","20","22","30","32"]
        
        logger.info("Swapping polarity of downlink %s",str(channels[dl]))
        polarity = self.read("EPTX"+str(output_driver[dl])+"CHNCNTR","EPTX"+str(channels[dl])+"INVERT")
        
        if polarity:
            self.write_read("EPTX"+str(output_driver[dl])+"CHNCNTR","EPTX"+str(channels[dl])+"INVERT",0)
            logger.info("Polarity swapped from INVERTED to NOT INVERTED")
            
        else:
            self.write_read("EPTX"+str(output_driver[dl])+"CHNCNTR","EPTX"+str(channels[dl])+"INVERT",1)
            logger.info("Polarity swapped from NOT INVERTED to INVERTED")

    def reset_slave_clock(self, slave_name):
        """Reset the clock of the slave ASICs.

        Args:
            slave_name (str): name of the slave ASIC to reset, can be any of ["lpgbt2", "lpgbt3", "lpgbt4", "gbcr"] for lpGBT1 or ["gbcr"] for slave lpGBTs
        """
        if slave_name == "lpgbt2" and self.master:
            logger.info("Resetting clock for lpGBT2...")
            self.write_read("EPCLK2CHNCNTRH", None, 0)
            self.write_read("EPCLK2CHNCNTRL", None, 0)
            self.write_read("EPCLK2CHNCNTRH", None, self.construct_reg_data("EPCLK2CHNCNTRH"))
            self.write_read("EPCLK2CHNCNTRL", None, self.construct_reg_data("EPCLK2CHNCNTRL"))
            logger.info("Reset done")
        elif slave_name == "lpgbt3" and self.master:
            logger.info("Resetting clock for lpGBT3...")
            self.write_read("EPCLK26CHNCNTRH", None, 0)
            self.write_read("EPCLK26CHNCNTRL", None, 0)
            self.write_read("EPCLK26CHNCNTRH", None, self.construct_reg_data("EPCLK26CHNCNTRH"))
            self.write_read("EPCLK26CHNCNTRL", None, self.construct_reg_data("EPCLK26CHNCNTRL"))
            logger.info("Reset done")
        elif slave_name == "lpgbt4" and self.master:
            logger.info("Resetting clock for lpGBT4...")
            self.write_read("EPCLK19CHNCNTRH", None, 0)
            self.write_read("EPCLK19CHNCNTRL", None, 0)
            self.write_read("EPCLK19CHNCNTRH", None, self.construct_reg_data("EPCLK19CHNCNTRH"))
            self.write_read("EPCLK19CHNCNTRL", None, self.construct_reg_data("EPCLK19CHNCNTRL"))
            logger.info("Reset done")
        elif "gbcr" in slave_name:
            logger.info("Resetting clock for GBCR...")
            self.write_read("EPCLK5CHNCNTRH", None, 0)
            self.write_read("EPCLK5CHNCNTRL", None, 0)
            self.write_read("EPCLK5CHNCNTRH", None, self.construct_reg_data("EPCLK5CHNCNTRH"))
            self.write_read("EPCLK5CHNCNTRL", None, self.construct_reg_data("EPCLK5CHNCNTRL"))
            logger.info("Reset done")
        else:
            logger.error("Slave name not recognized, choose from ['lpgbt2', 'lpgbt3', 'lpgbt4', 'gbcr'] for lpGBT1 or ['gbcr'] for slave lpGBTs")


    def tx_enable_disable(self,dl,boolean_value):
        """Enable or disable one channel of TX (downlink).
        The convention is as follows:
        dl == (e-group,channel) 
        0 == 00
        1 == 02
        2 == 10
        3 == 12
        4 == 20
        5 == 22
        6 == 30
        7 == 32

        Args:
            dl (int) can be from 0 to 7
            boolean_value: True for enable and False for disable
        """
        groups = ["10","10","10","10","32","32","32","32"]
        group_channel = ["00","02","10","12","20","22","30","32"]
        register = "EPTX" + str(groups[dl]) + "ENABLE_EPTX" + str(group_channel[dl]) + "ENABLE"

        # Enable one of the downlinks
        if boolean_value == True:
            self.write_read(register.split("_")[0] , register.split("_")[1] , 1)
            logger.info("Enabling downlink " + str(group_channel[dl]))

        # Disable one of the downlinks
        if boolean_value == False:
            self.write_read(register.split("_")[0] , register.split("_")[1] , 0)       
            logger.info("Disabling downlink " + str(group_channel[dl]))


    def rx_enable_disable(self,group,boolean_value):
        """Enable or diasable one channel of RX (uplink).  
        Args:
            group (int) can be from 0 to 5
            boolean_value: True for enable and False for disable
        """
        group_str = str(group)
        register = "EPRX" + group_str + "CONTROL_EPRX" + group_str + "0ENABLE"

        # Enable one of the uplinks
        if boolean_value == True:
            self.write_read(register.split("_")[0] , register.split("_")[1] , 1)
            logger.info("Enabling uplink " + group_str + "0")

        # Disable one of the uplinks
        if bool == False:
            self.write_read(register.split("_")[0] , register.split("_")[1] , 0)    
            logger.info("Disabling uplink " + group_str + "0")

        


    def tx_rx_scan(self, optical_channel, url=os.environ.get("FELIX_BACKEND_URL") if os.environ.get("FELIX_BACKEND_URL") is not None else "http://localhost:8001"):
        """Iteratively turn off the lpGBT downlink to identify to which of them each FE is connected        
        Args:
            optical_channel (str): felix channel to be scanned
            url (str): felix backend url
        
        Returns:
            link_list (list): list containing the connected downlink and uplink numbers
        """
        if not self.master:
            raise Exception("The only downlinks in use are those of lpGBT1!")
        
        aligned_link = "DECODING_LINK_ALIGNED_" + str(optical_channel)
        link_list=[]
        registers = ["EPTX10ENABLE_EPTX00ENABLE", "EPTX10ENABLE_EPTX02ENABLE" , "EPTX10ENABLE_EPTX10ENABLE" , "EPTX10ENABLE_EPTX12ENABLE", "EPTX32ENABLE_EPTX20ENABLE" , "EPTX32ENABLE_EPTX22ENABLE", "EPTX32ENABLE_EPTX30ENABLE" , "EPTX32ENABLE_EPTX32ENABLE"]

        aligned_initial = requests.get(url + "/flxconfigget/" + aligned_link, headers={"content-type": "application/json"}).json()["result"].strip().split("=")[1]
        
        if "1" not in aligned_initial:
            logger.warning("No decoding alignment has been found!")
            return link_list

        for downlink_index, register in enumerate(registers):
            # disable i-th downlink
            self.write_read(register.split("_")[0] , register.split("_")[1] , 0)
            # Check new alignment
            aligned = requests.get(url + "/flxconfigget/" + aligned_link, headers={"content-type": "application/json"}).json()["result"].strip().split("=")[1]

            if aligned != aligned_initial:
                for uplink_index in range(6):
                    if aligned_initial[-uplink_index-1] != aligned[-uplink_index-1]:
                            logger.info("Optical link " + optical_channel + " => Downlink " + str(downlink_index) + " <-> Uplink " + str(uplink_index))
                            link_list.append({"downlink": downlink_index, "uplink": uplink_index})                  

            # enable i-th downlink
            self.write_read(register.split("_")[0] , register.split("_")[1] , 1)
            aligned = requests.get(url + "/flxconfigget/" + aligned_link, headers={"content-type": "application/json"}).json()["result"].strip().split("=")[1]
            if aligned_initial != aligned:
                raise Exception("Use this function before configuring the FE!")
        
        return link_list

    def set_phase_mode(self,phase_mode, group , phase = 0):
        """
        Function to set the phase mode of the e-group. Initial training will find the phase automatically and set it until changed. 
        Fixed phase will set the phase to the specified value.
        Continuous tracking will track the phase continuously and change it continuously.
        Args:
            phase_mode (str): can be "fixed_phase", "continuous_tracking" or "initial_training".
            group (int): can be from 0 to 5.
            phase (int): can be from 0 to 15, must be given only for fixed_phase mode.
        Returns:
            phase_lock (boolean): True if the phase is locked, False if not.
            phase (int): the phase of the e-group.
        """
        assert phase_mode in ["fixed_phase", "continuous_tracking", "initial_training"]
        assert group >=0 and group < 6, "The group is not in the correct range (0-5)"
        G_str = str(group)
        if phase_mode == "initial_training":
            phase_lock,phase = self.phase_training(group = group)
        else:
            if phase_mode == "fixed_phase":
                assert phase >=0 and phase < 16, "The phase is not in the correct range (0-15)"
                self.write_read("EPRX"+G_str+"CONTROL", "EPRX"+G_str+"TRACKMODE", 0)
                self.write_read("EPRX"+G_str+"0ChnCntr", "EPRX"+G_str+"0PhaseSelect", phase)
            if phase_mode == "continuous_tracking":
                self.write_read("EPRX"+G_str+"CONTROL", "EPRX"+G_str+"TRACKMODE", 2)
            phase_lock = self.read("EPRX"+G_str+"LOCKED", "EPRX"+G_str+"CHNLOCKED")
            phase = self.read("EPRX"+G_str+"CURRENTPHASE10", "EPRX"+G_str+"CURRENTPHASE0")
            if int(bin(phase_lock)[-1]):
                logger.info("%s e-link %s (EPRX group %s channel 0) is %slocked%s with phase %s", self.device, G_str, G_str, LoggerFormat.green, LoggerFormat.reset, phase)
            else:
                logger.info("%s e-link %s (EPRX group %s channel 0) is %snot locked%s, set phase: %s", self.device, G_str, G_str, LoggerFormat.red, LoggerFormat.reset, phase)
            return phase_lock, phase

    def check_EPRX_phase(self, group):
        """
        Function to read the phase of the specified e-group for the uplink.
        Could be used to monitor the phase when the continous phase tracking mode is used.
        Args:
            group (int): can be from 0 to 5.
        Returns:
            locked (boolean): True if the phase is locked, False if not.
            phase (int): the phase of the e-group.
        """
        assert group >=0 and group < 6, "The group is not in the correct range (0-5)"
        G_str = str(group)
        phase_lock = self.read("EPRX"+G_str+"LOCKED", "EPRX"+G_str+"CHNLOCKED")
        phase = self.read("EPRX"+G_str+"CURRENTPHASE10", "EPRX"+G_str+"CURRENTPHASE0")
        locked = False
        if int(bin(phase_lock)[-1]):
            logger.info("%s e-link %s (EPRX group %s channel 0) is %slocked%s with phase %s", self.device, G_str, G_str, LoggerFormat.green, LoggerFormat.reset, phase)
            locked = True
        else:
            logger.info("%s e-link %s (EPRX group %s channel 0) is %snot locked%s, set phase: %s", self.device, G_str, G_str, LoggerFormat.red, LoggerFormat.reset, phase)
        return group, locked, phase


    def read_id(self,verbose=True):
        """Read the Id of lpgbt and check if they are the same
        Args:
            verbose (boolean): if True, print all the steps (ADC value, Id)
        Returns:
            The Id or False if there is a mistake        
        """    
        id1 = self.fuses_read_bank(0)
        id2 = self.fuses_read_bank(8)
        id2=(id2>>6)|(id2 << (32 - 6)) & 0xFFFFFFFF
        id3 = self.fuses_read_bank(12)
        id3=(id3>>12)|(id3 << (32 - 12)) & 0xFFFFFFFF
        id4 = self.fuses_read_bank(16)
        id4=(id4>>18)|(id4 << (32 - 18)) & 0xFFFFFFFF
        id5 = self.fuses_read_bank(20)
        id5=(id5>>24)|(id5 << (32 - 24)) & 0xFFFFFFFF
        ids = [id1,id2,id3,id4,id5]
        if sum(ids[1:]) == 0:
            if verbose:
                logger.info("no redundancy was detected, return only the first register")
                logger.info("The id is %s " ,str(hex(ids[0]))) 
            return ids[0]

        most_probable_id = self.majority_vote(ids)["result_voted"]
        if verbose:
            logger.info("The most probable id is %s " ,str(hex(most_probable_id)) )
        return most_probable_id


    def majority_vote(self, ids):
        """Performs bit-wise majority voting for list of ids.
        Args:
            ids (list): list of ids to be voted on
        Returns:
            Dictionary with keys: result_voted, mismatches"""

        result_voted = 0
        mismatches = {}
        for bit in range(32):
            bits = []
            for value in ids:
                bit_value = (value >> bit) & 0x1
                bits.append(bit_value)
            bits_counter = Counter(bits)
            bit_voted, _ = bits_counter.most_common(1)[0]
            if len(bits_counter.most_common()) != 1:
                mismatches[bit] = {"voted": bit_voted, "bits": bits}
            result_voted |= bit_voted << bit
        return {"result_voted": result_voted, "mismatches": mismatches}


    def read_lpgbt_calibration_file(self, calibration_file_path = "/workspace/calibration/own_lpgbt_calibration.csv",verbose=True):
        '''Read the calibration values for the lpgbt. More specifically, find the right row in the table corresponding to the id
        Args:
            variable_name (array of strings) is the parameters one would like to access for the specific lpgbt id.
            verbose (boolean): if True, print all the steps (ADC value, temperature,...)
        Returns: 
            The values of the parameters given, corresponding to the id of the lpgbt.
        ''' 
        id = self.read_id(verbose=verbose)
        id_hex = format(id, '08x').upper()
        column_name = {}
        names = []
        try:
            f = open(calibration_file_path)
        except:
            logger.warning("The csv file "+ calibration_file_path + " couldn't be read.")
            return None
        try:
            reader = csv.reader(f)
            for row in reader:
                if row[0] == "CHIPID":
                    for idx, name in enumerate(row):
                        column_name[name] = idx
                        if name != "CHIPID":
                            names.append(name)
                if row[0] == id_hex:
                    for name in names:
                        self.lpgbt_calibration[name] = float(row[column_name[name]])
                    return None
            
            f.close()
            logger.warning(f"""The ID:{id_hex} couldn't be find in the csv calibration file. Please, copy from the file located at
                            https://lpgbt.web.cern.ch/lpgbt/calibration/lpgbt_calibration_latest.zip
                           the corresponding line from the chipID of this lpgbt. And copy it in the file calibration/own_lpgbt_calibration.csv"""
                           )
        except:
            f.close()
            logger.warning("""Failed to read the csv file."""
                           )
            return None
    
    def read_estimate_onchip_temperature(self, reset_temperature_sensor = True,verbose=False):
        '''Read the estimated temperature of the lpgbt.
        More specifically, read the adc voltage and use the calibration equation to translate it to temperature.
        Taken from the lpgbt manual "we recommend assuming a value 20 K above the expected ambient temperature and a large standard error of 20K for the following procedures."
        Args:
            reset_temperature_sensor (boolean) should be true if one would like to reset the temperature sensor before reading the adc value.
            verbose (boolean): if True, print all the steps (ADC value, temperature,...)
        Returns: 
            The estimate temperature in degree Celsius. (usually 20-30C below the accurate temperature).
        ''' 
        if reset_temperature_sensor:
            self.read(reg="ADCMon",reg_field="TEMPSensReset")
            self.write_read(reg="ADCMon",reg_field="TEMPSensReset",reg_data=1)
            self.write_read(reg="ADCMon",reg_field="TEMPSensReset",reg_data=0)
        adc_value = self.read_adc(14,verbose=verbose)
        if len(self.lpgbt_calibration) == 0:
            self.read_lpgbt_calibration_file(verbose=verbose)
        temperature_offset = self.lpgbt_calibration['TEMPERATURE_UNCALVREF_OFFSET']
        temperature_slope = self.lpgbt_calibration['TEMPERATURE_UNCALVREF_SLOPE']
        temperature = (adc_value*temperature_slope)+temperature_offset 
        if verbose:
            logger.info("Estimation of temperature is %.1f C  (unaccurate, usually 20-30C below)" %temperature)  
        return temperature
    
    def calibrate_adc_to_voltage(self,adc_value,T=None,verbose=True):
        """
        A calibration is applied to get the voltage value corresponding to the adc value read.
        Args:
            adc_value (float) is the value read from the self.read_adc() function.
            T (float, optional): the default setting gives the automatically estimated temperature.
            verbose (boolean): if True, print all the steps (ADC value, temperature,...)
        Returns:
            the adc value in Volt.
        """
        if T==None:
            T=self.read_onchip_temperature(verbose=verbose)
        
        if len(self.lpgbt_calibration) == 0:
                self.read_lpgbt_calibration_file(verbose=verbose)
        adc_x2_slope = self.lpgbt_calibration['ADC_X2_SLOPE']
        adc_x2_slope_temp = self.lpgbt_calibration['ADC_X2_SLOPE_TEMP']
        adc_x2_offset = self.lpgbt_calibration['ADC_X2_OFFSET']
        adc_x2_offset_temp = self.lpgbt_calibration['ADC_X2_OFFSET_TEMP']
        adc_v = adc_value  * (adc_x2_slope + T * adc_x2_slope_temp) + adc_x2_offset + T * adc_x2_offset_temp
        return adc_v
    
    def tune_vref(self, enable=True, T=None,verbose=True):
        '''Gives the value to tune the reference voltage to 1.0V.
        Args:
            enable (boolean): enables the VREF generator
            T (float, optional): the default setting gives the automatically estimated temperature.
            verbose (boolean): if True, print all the steps (ADC value, temperature,...)
        Returns:
            The optimal reference voltage generator tune code as a integer
        '''
        if T==None:
            T=self.read_estimate_onchip_temperature(verbose=verbose)
        if enable:
            self.write_read("VREFCNTR", "VREFEnable", 1)
        if len(self.lpgbt_calibration) == 0:
            self.read_lpgbt_calibration_file(verbose=verbose)
        cal_vref_slope = self.lpgbt_calibration['VREF_SLOPE']
        cal_vref_offset = self.lpgbt_calibration['VREF_OFFSET']
        VREFTune = round(T * cal_vref_slope + cal_vref_offset)
        return VREFTune
    

    def read_onchip_temperature(self, reset_temperature_sensor = True,verbose=True):
        '''Read the  temperature of the lpgbt.
        More specifically, calculate the temperature by using the estimate temperature and another calibration equation.
        Taken from the lpgbt manual "we recommend assuming a value 20 K above the expected ambient temperature and a large standard error of 20K for the following procedures."
        Args:
            reset_temperature_sensor (boolean) is used to reset the temperature sensor before reading it
            verbose (boolean): if True, print all the steps (ADC value, temperature,...)
        Returns: 
            The more precise temperature in degree Celsius, is not completely reliable.
        ''' 
        if reset_temperature_sensor:
            self.write_read(reg="ADCMon",reg_field="TEMPSensReset",reg_data=1)
            self.write_read(reg="ADCMon",reg_field="TEMPSensReset",reg_data=0)
        
        T = self.read_estimate_onchip_temperature(reset_temperature_sensor=True,verbose=verbose)-20
        VREFTune = self.tune_vref(T=T,verbose=verbose)
        adc_value = self.read_adc(14,VREFTune,verbose=verbose)
        v_adc = self.calibrate_adc_to_voltage(adc_value,T=T,verbose=verbose)
        if len(self.lpgbt_calibration) == 0:
            self.read_lpgbt_calibration_file(verbose=verbose)
        temperature_offset = self.lpgbt_calibration['TEMPERATURE_OFFSET']
        temperature_slope = self.lpgbt_calibration['TEMPERATURE_SLOPE']
        temp_c = (v_adc*temperature_slope)+temperature_offset
        if verbose:
            logger.info("Accurate temperature is %.1f °C (+-10°C). For more precise measurement of T on Optoboard, prefer read_TH2() (VTRX T°)"%temp_c) 
        return temp_c 
    


    def read_supply_voltage(self, monitor_channel,T=None, disable_monitor=True, number_of_measurements=3,verbose=False):
        '''Read the supply voltage of the lpgbt.
        More specifically, read the adc voltage and use the calibration equation to translate it to supply voltage. 
        Delta V is 10mV.
        Args:
            monitor_channel (float): The channel from which one wants to read out the input voltage (0 = VDDTXmonEna, 1 = VDDRXmonEna, 2 = VDDmonEna, 3 = VDDANmonEna).
            T (float, optional): The default setting gives the automatically estimated temperature.
            disable_monitor (boolean): Disables the monitor after using it.
            verbose (boolean): if True, print all the steps (ADC value, temperature,...)
        Returns: 
            Dictionary with lpGBT hexidecimal ID and input voltage in volt.
        ''' 
        voltage_dict = {}
        id = self.read_id(verbose=verbose)
        id_hex = format(id, '08x').upper()
        voltage_dict["lpGBT_ID"] = id_hex
    
        if monitor_channel == 0:
            self.write_read(reg="ADCMon",reg_field="VDDTXmonEna",reg_data=1)
        elif monitor_channel == 1:
            self.write_read(reg="ADCMon",reg_field="VDDRXmonEna",reg_data=1)
        elif monitor_channel == 2:    
            self.write_read(reg="ADCMon",reg_field="VDDmonEna",reg_data=1)
        elif monitor_channel == 3:
            self.write_read(reg="ADCMon",reg_field="VDDANmonEna",reg_data=1)
        else:
            logger.warning("The monitor channel is not correct")
            return 0
        
        if T==None:
            T=self.read_onchip_temperature(verbose=verbose)
        VREFTune = self.tune_vref(T=T,verbose=verbose)
        
        adc_value = self.read_adc(int(10+monitor_channel),VREFTune,number_of_reading=number_of_measurements,verbose=verbose)
        v_adc = self.calibrate_adc_to_voltage(adc_value,T=T,verbose=verbose)
        
        if len(self.lpgbt_calibration) == 0:
            self.read_lpgbt_calibration_file(verbose=verbose)
        cal_vddmon_slope = self.lpgbt_calibration['VDDMON_SLOPE']
        cal_vddmon_slope_temp = self.lpgbt_calibration['VDDMON_SLOPE_TEMP']
        voltage= v_adc * (cal_vddmon_slope + T * cal_vddmon_slope_temp)
        logger.info("The supply voltage is %.3f V (+-0.01V)" %voltage) 

        if disable_monitor:
            if monitor_channel == 0:
                self.write_read(reg="ADCMon",reg_field="VDDTXmonEna",reg_data=0)
            elif monitor_channel == 1:
                self.write_read(reg="ADCMon",reg_field="VDDRXmonEna",reg_data=0)
            elif monitor_channel == 2:    
                self.write_read(reg="ADCMon",reg_field="VDDmonEna",reg_data=0)
            elif monitor_channel == 3:
                self.write_read(reg="ADCMon",reg_field="VDDANmonEna",reg_data=0)
        
        voltage_dict["Voltage"] = voltage
        return voltage_dict

    def read_PTAT(self,T=None, number_of_measurements=3, verbose = False):
        '''
        Read the PTAT voltage of the lpgbt (bpol2v5 temperature). 4mV/C is the typical value for the PTAT slope. Offset is undefined. 
        Return only the difference from previous measurement
        Args:
            T (float, optional): The default setting gives the automatically estimated temperature.
            verbose (boolean): if True, print all the steps (ADC value, temperature,...)
            number_of_measurements (int): The number of measurements to be taken and averaged.
        Returns: 
            The difference in temperature in degree Celsius from previous measurement. 
        '''
        if not self.master:
            raise Exception("PTAT is only available on slave lpgbt chips. Use lpgbt1.")
        if T==None:
            T=self.read_onchip_temperature(verbose=verbose)
        VREFTune = self.tune_vref(T=T,verbose=verbose)
        adc_value = self.read_adc(3,VREFTune,number_of_reading=number_of_measurements,verbose=verbose)
        v_adc = self.calibrate_adc_to_voltage(adc_value,T=T,verbose=verbose)
        T_PTAT = v_adc/0.004 
        if self.last_PTAT_measurement[0] == None:
            self.last_PTAT_measurement = [T_PTAT,time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())]
            logger.info("Undefined for the first measurement, only the difference from the previous measurement is given. Try again later")
            return 0
        diff = T_PTAT - self.last_PTAT_measurement[0]
        last_time = self.last_PTAT_measurement[1]
        formatted_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
        self.last_PTAT_measurement = [T_PTAT,formatted_time]
        logger.info("The PTAT temperature since the last PTAT measurement ("+last_time+") changed by %.1f °C (+-2°C)" %(diff))
        return diff
    
    def read_RSSI(self,T=None,number_of_measurements=3,verbose=False):
        '''
        Read the RSSI current output of the VTRX TIA. Received Signal Strength Indicator. Useful for diagnosis. Usually around 400 uA.
        Args:
            T (float, optional): The default setting gives the automatically estimated temperature.
            number_of_measurements (int): The number of measurements to be taken and averaged.
            verbose (boolean): if True, print all the steps (ADC value, temperature,...)
        Returns: 
            The RSSI in V
        '''
        if not self.master:
            raise Exception("RSSI is only available on slave lpgbt chips. Use lpgbt1.")
        if T==None:
            T=self.read_onchip_temperature(verbose=verbose)
        VREFTune = self.tune_vref(T=T,verbose=verbose)
        adc_value = self.read_adc(0,VREFTune,number_of_reading=number_of_measurements,verbose=verbose)
        v_adc = self.calibrate_adc_to_voltage(adc_value,T=T,verbose=verbose)
        RSSI_v = v_adc*1470/470
        RSSI = (2.5 - RSSI_v)/4700
        logger.info("The voltage monitor of RSSI after resistance conversion is %.0f uA" %(RSSI*1000000))
        return RSSI
    
    def read_TH2(self,T=None,number_of_measurements=3,verbose=False):
        '''
        Read the temperature from the VTRX. +-2°C of accuracy.
        Args:
            T (float, optional): The default setting gives the automatically estimated temperature.
            number_of_measurements (int): The number of measurements to be taken and averaged.
            verbose (boolean): if True, print all the steps (ADC value, temperature,...)
        returns:
            The temperature of the VTRx in degree Celsius.
        '''        
        if not self.master:
            raise Exception("TH2 is only available on slave lpgbt chips. Use lpgbt1 or use read_onchip_temperature() instead.")

        if T==None:
            T=self.read_onchip_temperature(verbose=verbose)
        VREFTune = self.tune_vref(T=T,verbose=verbose)
        adc_value = self.read_adc(1,VREFTune,number_of_reading=number_of_measurements,verbose=verbose)
        adc_value = self.calibrate_adc_to_voltage(adc_value,T=T,verbose=verbose)
        B = 3560 #K 
        T0 = 25 + 273.15 #K
        R0 = 1000 #Ohm
        R_pullup = 1500 #ohm
        V_pullup = 1.2 #V
        R = R_pullup/((V_pullup/adc_value) -1) #Voltage divider law
        T = 1/(1/T0 + ((np.log(R) - np.log(R0)) /B )) -273.15
        logger.info("The temperature of TH2 after conversion is %.1f °C (+-2°C)" %(T))
        return T

    def ring_oscill_freq(self, reps = 1, plot_bool = False, filename = 'Ring_oscill_freq.png'):
        '''
        Function to read lpGBT ring oscillators frequency 
        Args:
            reps (int): number of measurements for each oscillator.
            plot_bool (bool): set to True to plot results.
            filename (str): name of the output plot.
        '''

        if plot_bool:
            plt.figure(figsize=(10,8))
        
        logger.info("Only measuring oscillators 2 and 3, the results on 0 and 1 are unreliable")

        xp = np.array([2, 3])

        for _ in range(reps):

            measurements = []
  
            for x in xp:
                
                logger.info(f"measuring oscillator {x}")
                self.write_read("PROCESSANDSEUMONITOR", None, x << 1 | 1)

                status = self.read("PROCESSMONITORSTATUS", "PMDONE")

                while (not status):

                    status = self.read("PROCESSMONITORSTATUS", "PMDONE")

                result = self.read("PMFREQA", None) << 16 | self.read("PMFREQB", None) << 8 | self.read("PMFREQC", None)
                
                measurements.append(result)
                logger.info(result)

                self.write_read("PROCESSANDSEUMONITOR", None, 0)


            yp = np.array(measurements)
            
            if plot_bool: 

                plt.plot(xp, yp, marker = 'H', mec = 'k')
                plt.grid(color = 'blue', linestyle = '--', linewidth = 0.5)
                plt.title("Oscillators frequency")
                plt.xlabel("Channel")
                plt.ylabel("Frequency (Hz)")
    

                plt.xticks(xp)
                
        
        if plot_bool: 
   
            workspace = os.environ.get("WORKSPACE")
            if workspace is not None:
                file_name = str(workspace) + "/" + filename
            else:
                file_name = filename

            plt.savefig(file_name)
            plt.close()
