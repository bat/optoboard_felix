# pylint: disable=line-too-long, invalid-name, import-error
"""Optoboard module."""

import json
import inspect
from collections import defaultdict
from collections import OrderedDict
#from alive_progress import alive_bar
import logging
import logging.handlers
from datetime import datetime
from datetime import date
import os

from optoboard_felix.driver.log import logger, ch

try:
    import matplotlib.pyplot as plt
    import matplotlib.colors as colors
    import matplotlib.cm as cm
except (ImportError, ModuleNotFoundError):
    logger.critical("Problem with the import of matplotlib! Some function might not work properly!")
try:
    import numpy as np
except (ImportError, ModuleNotFoundError):
    logger.critical("Problem with the import of numpy! Some function might not work properly!")

import time
import os
import subprocess
import requests

from optoboard_felix.driver.Lpgbt import Lpgbt
from optoboard_felix.driver.Gbcr import Gbcr
from optoboard_felix.driver.Vtrx import Vtrx

from optoboard_felix.register_maps.lpgbt_register_map_v1 import LpgbtRegisterMapV1
from optoboard_felix.register_maps.vtrx_register_map_v1_3 import VTRxplusRegisterMapV1_3
from optoboard_felix.register_maps.gbcr2_register_map import GBCRRegisterMap as GBCRRegisterMap_V2
from optoboard_felix.register_maps.gbcr3_register_map import GBCRRegisterMap as GBCRRegisterMap_V3


now = datetime.now()
date_time = now.strftime("%Y-%m-%d_%H-%M")

def load_custom_config(path):
    """Load config from file.

    Args:
        path (str): path to config

    Returns:
        config_file (dict): contents of file

    Raises:
        FileNotFoundError: config file does not exist
    """
    try:
        logger.info('Loading custom config from %s', path)
        with open(path) as f:
            config_file = json.load(f)
    except:
        raise FileNotFoundError("Unavailable configuration file!")

    return config_file

def load_default_config(components, vtrx_v):
    """Load default config from file.

    Args:
        components (dict): contains Optoboard chip versions
        vtrx_v (str): connected VTRx+ version 

    Returns:
        config_file (dict): contents of file

    Raises:
        FileNotFoundError: config file does not exist
    """
    CorePath = "./configs/optoboard" + str(components["optoboard_v"]) + "_lpgbtv" + str(components["lpgbt_v"]) + "_gbcr" + str(components["gbcr_v"]) + "_vtrxv" + vtrx_v.replace(".", "_") + "_default.json"
    path = os.path.join(os.path.dirname(os.path.abspath(__file__)), CorePath)

    ## This is a minor line to add tdaq compability (Check files if they are backed up in TDAQ)
    if not os.path.exists(path):
        ITK_PATH = os.environ.get("ITK_INST_PATH","")
        path = ITK_PATH+"/share/data/optoboard_felix/"+CorePath


    try:
        logger.info('Loading default config from %s', path)
        with open(path) as f:
            config_file = json.load(f)
    except:
        raise FileNotFoundError("Unavailable configuration file!")

    return config_file


class Optoboard:
    """Class for handling the chips on an Optoboard."""

    def __init__(self, config_file, optoboard_serial, vtrx_v, components_opto, Communication_wrapper, debug=False):
        """Initialise the Optoboard object.

        Initialisation sets the debug level of the logger, not fully optimized yet.
        Args:
            config_file (dict): dictionary with list of registers to be configured and their values
            optoboard_serial (str): serial number of the optoboard
            vtrx_v (str): VTRx+ quad laser driver version
            components_opto (dict): ordered dictionary of the components.py dictionary
            Communication_wrapper (class): low level access to FELIX access
            debug (bool, optional): True for debug mode on, Flase per default

        Attributes:
            config_file (dict): dictionary with list of registers to be configured and their values
            optoboard_serial (str): serial number of the optoboard
            debug (bool, optional): True for debug mode on, Flase per default
            component (dict): ordered dictionary of the components.py dictionary
            lpgbt_v (int): lpGBT version
            gbcr_v (int): GBCR version
            lpgbt_master_address (hex): I2C address of the lpGBT
            I2C_master (int): I2C controller to use (can be 0, 1, 2)
            efused (int): 1 if Optoboard is efused
            activeLpgbt (int): mounted lpGBTs on Optoboard
            activeGbcr (int): mounted GBCRs on Optoboard
            devices (dict): ordered dictionary of active devices
            lpgbt* (class): lpGBT* object where * can be 1, 2, 3, 4 according to devices mounted
            gbcr* (class): GBCR* object where * can be 1, 2, 3, 4 according to devices mounted
            vtrx (class): VTRx+ object
        """
        self.config_file = config_file
        self.optoboard_serial = optoboard_serial

        self.debug = debug
        if debug:
            logger.setLevel(logging.DEBUG)
            ch.setLevel(logging.DEBUG)
            logger.info("Set logger level to DEBUG")
        else:
            logger.setLevel(logging.INFO)
            ch.setLevel(logging.INFO)     
            logger.info("Logger level at INFO")

        self.component = OrderedDict(components_opto)
        self.lpgbt_v = self.component["lpgbt_v"]
        self.gbcr_v = self.component["gbcr_v"]
        self.optoboard_v = self.component["optoboard_v"]
        self.lpgbt_master_addr = self.component["lpgbt_master_addr"]
        self.I2C_master = self.component["I2C_master"]
        self.efused = self.component["efused"]

        self.activeLpgbt = (self.component["lpgbt1"] << 3) | (self.component["lpgbt2"] << 2) | (self.component["lpgbt3"] << 1) | (self.component["lpgbt4"])
        self.activeGbcr = (self.component["gbcr1"] << 3) | (self.component["gbcr2"] << 2) | (self.component["gbcr3"] << 1) | (self.component["gbcr4"])

        self.devices = OrderedDict()

        logger.info("component: %s", self.component)

        for device, mounted in self.component.items():
            if mounted:
                if device=="lpgbt1":
                    self.lpgbt1 = Lpgbt(Communication_wrapper, "lpgbt1", self.lpgbt_master_addr, True, self.I2C_master, config_file.copy(), self.optoboard_serial, self.optoboard_v, self.lpgbt_v, self.efused)
                    self.devices["lpgbt1"] = self.lpgbt1
                elif device=="lpgbt2":
                    self.lpgbt2 = Lpgbt(Communication_wrapper, "lpgbt2", 0x75, False, self.I2C_master, config_file.copy(), self.optoboard_serial, self.optoboard_v, self.lpgbt_v, self.efused)
                    self.devices["lpgbt2"] = self.lpgbt2
                elif device=="lpgbt3":
                    self.lpgbt3 = Lpgbt(Communication_wrapper, "lpgbt3", 0x76, False, self.I2C_master, config_file.copy(), self.optoboard_serial, self.optoboard_v, self.lpgbt_v, self.efused)
                    self.devices["lpgbt3"] = self.lpgbt3
                elif device=="lpgbt4":
                    self.lpgbt4 = Lpgbt(Communication_wrapper, "lpgbt4", 0x77, False, self.I2C_master, config_file.copy(), self.optoboard_serial, self.optoboard_v, self.lpgbt_v, self.efused)
                    self.devices["lpgbt4"] = self.lpgbt4

                elif device=="gbcr1":
                    self.gbcr1 = Gbcr(Communication_wrapper, "gbcr1", 0x20, False, self.I2C_master, config_file.copy(), self.gbcr_v)
                    self.devices["gbcr1"] = self.gbcr1
                elif device=="gbcr2":
                    self.gbcr2 = Gbcr(Communication_wrapper, "gbcr2", 0x21, False, self.I2C_master, config_file.copy(), self.gbcr_v)
                    self.devices["gbcr2"] = self.gbcr2
                elif device=="gbcr3":
                    self.gbcr3 = Gbcr(Communication_wrapper, "gbcr3", 0x22, False, self.I2C_master, config_file.copy(), self.gbcr_v)
                    self.devices["gbcr3"] = self.gbcr3
                elif device=="gbcr4":
                    self.gbcr4 = Gbcr(Communication_wrapper, "gbcr4", 0x23, False, self.I2C_master, config_file.copy(), self.gbcr_v)
                    self.devices["gbcr4"] = self.gbcr4

        self.vtrx = Vtrx(Communication_wrapper, "vtrx", 0x50, False, self.I2C_master, config_file.copy(), vtrx_v, int('{:04b}'.format(self.activeLpgbt)[::-1],2))
        self.devices["vtrx"] = self.vtrx

        logger.info("Optoboard object initialised!")
        logger.debug("devices initialised: %s", self.devices)


    def configure(self, customactivelpGBT=None, customactiveGBCR=None):
        """Configure the Optoboard according to the configuration provided in the initialization phase.
        Args:
            customactivelpGBT (str): string of 4 characters denoting which lpGBTs are to be configured
            customactiveGBCR (str):	string of 4 characters denoting which GBCRs are to be configured
        """

        configure_devices = list(self.devices.items())[0:1] + list(self.devices.items())[:0:-1]
        logger.debug("configure() - configure_devices: %s", configure_devices)
        
        for device, obj in configure_devices:
            if "lpgbt" in device:
                if (customactivelpGBT is None) or (customactivelpGBT[int(device[-1])-1] == "1"):
                    logger.info("Configuring %s..", device)
                    obj.configure()

                    if device == "lpgbt1":
                        if self.component["lpgbt2"]:
                            logger.info("Enable eclock output for lpGBT2..")
                            self.devices['lpgbt1'].write_read("EPCLK2CHNCNTRH", None, self.devices['lpgbt1'].construct_reg_data("EPCLK2CHNCNTRH"))
                            clock_reg_low = self.devices['lpgbt1'].construct_reg_data("EPCLK2CHNCNTRL")
                            if clock_reg_low != 0:
                                self.devices['lpgbt1'].write_read("EPCLK2CHNCNTRL", None, clock_reg_low)

                        if self.component["lpgbt3"]:
                            logger.info("Enable eclock output for lpGBT3..")
                            self.devices['lpgbt1'].write_read("EPCLK26CHNCNTRH", None, self.devices['lpgbt1'].construct_reg_data("EPCLK26CHNCNTRH"))
                            clock_reg_low = self.devices['lpgbt1'].construct_reg_data("EPCLK2CHNCNTRL")
                            if clock_reg_low != 0:
                                self.devices['lpgbt1'].write_read("EPCLK26CHNCNTRL", None, clock_reg_low)

                        if self.component["lpgbt4"]:
                            logger.info("Enable eclock output for lpGBT4..")
                            self.devices['lpgbt1'].write_read("EPCLK19CHNCNTRH", None, self.devices['lpgbt1'].construct_reg_data("EPCLK19CHNCNTRH"))
                            clock_reg_low = self.devices['lpgbt1'].construct_reg_data("EPCLK2CHNCNTRL")
                            if clock_reg_low != 0:
                                self.devices['lpgbt1'].write_read("EPCLK19CHNCNTRL", None, clock_reg_low)


        
        self.devices['lpgbt1'].reset_all_GBCR()
        self.devices['lpgbt1'].reset_VTRX()

        for device, obj in configure_devices:
            if "gbcr" in device:
                if (customactiveGBCR is None) or (customactiveGBCR[int(device[-1])-1] == "1"):
                    obj.configure()
            elif "vtrx" in device:
                obj.configure(customactivelpGBT)
        
        logger.info("Optoboard configured!")


    def opto_status(self):
        """Check the status of the different devices on the Optoboard.

        Checks lpGBT info, PUSM status, lpGBT configuration pins, EPRX and EPTX settings, and locking status.
        Returns:
            opto_doc_dic (dict): status dictionary with devices as keys
        """
        opto_doc_dic = {}
        for device, obj in self.devices.items():
            
            logger.info("checking %s..", device)

            if device=="lpgbt1":
                eprx_phase_dict = {}
                opto_doc_dic[device] = {
                    **obj.get_lpgbt_info(),
                    **obj.check_PUSM_status(),
                    **obj.check_lpgbt_config_pins(),
                    **obj.check_EPRX_status(),
                    **obj.check_EPRX_locking(),
                    }


            elif device in ["lpgbt2", "lpgbt3", "lpgbt4"]:
                opto_doc_dic[device] = {
                    **obj.get_lpgbt_info(),
                    **obj.check_PUSM_status(),
                    **obj.check_lpgbt_config_pins(),
                    **obj.check_EPRX_status(),
                    **obj.check_EPRX_locking(),
                    }

            else:
                logger.debug("other devices to come..")

        logger.info("Optoboard checked!")

        return opto_doc_dic

    def reset_lpgbts(self):
        """Resets the master lpGBT. If the master lpGBT is not configurable after reset, power-cycle the Optoboard.

        See ch. 15.1.4 lpGBTv1 manual for more details 
        """
        logger.info("Checking if lpGBT master is READY")
        if self.lpgbt1.check_PUSM_status()["PUSM_ready"]:

            logger.warning("Resetting the lpGBT master")
            logger.warning("Writing 1 in ChipConfig_highSpeedDataInInvert")
            self.lpgbt1.comm_wrapper("CHIPCONFIG",64, False)

        else: 
            logger.info("lpGBT master is not in READY state: resetting is not possible!")


    def bertScan(self, lpgbt_name, gbcr_name, eprx_group, bertmeastime, filename="bertScan_result", plot=False, HFmin=0, HFmax=15, MFmin=0, MFmax=15, jump=1, bertsource_c=6):
        """Perform a 2D scan of the results of BERT as a function of the parameters of the equalizer of the GBCR.

        Args:
            lpgbt_name (str): lpGBT to initialize/scan, options: lpgbt1, lpgbt2, lpgbt3, lpgbt4
            gbcr_name (str): GBCR to initialize/scan, options: gbcr1, gbcr2, gbcr3, gbcr4
            eprx_group (int): EPRX group number
                if EPRX link => coarse source bertsource_g = eprx_group + 1,
                if EPTX link => bertsource_g = eprx_group + 9
            bertmeastime (int): controls the duration of the BERT, from 0 to 15 possible.
            filename (str, optional): name of the file where the results are saved (default "bertScan_result"). Set to None for no result file.
            plot (bool, optional): create plot of scanned equalizer registers (default False)
            HFmin (int, optional): CTLE high frequency lower bound (default 0)
            HFmax (int, optional): CTLE high frequency upper bound (default 16)
            MFmin (int, optional): CTLE mid frequency lower bound (default 0)
            MFmax (int, optional): CTLE mid frequency upper bound (default 16)
            jump (int, optional): level of detail of the scan of the GBCR parameters (default 1)
            bertsource_c (int): fine source for BERT if EPRX link => bertsource_c should be set to 6 (always channel 0) for 1.28 Gb/s [DEFAULT], if EPTX link => bertsource_c should be 4 (channel 0) or 5 (channel 2)
            
        
        Raises:
            NameError: Initialisation failes
            ValueError: lpGBT and GBCR number not identical
            ValueError: GBCR version not supported
        """

        try:
            lpgbt_obj = getattr(self, lpgbt_name)
            gbcr_obj = getattr(self, gbcr_name)
        except:
            logger.error("Initialisation of the lpGBT or GBCR object failed!")
            raise NameError("The initialisation of the chips for the BERT scan failed.")

        if lpgbt_name[-1] != gbcr_name[-1]:
            logger.error("lpGBT# should be the same as GBCR#, input: %s %s", lpgbt_name, gbcr_name)
            raise ValueError("lpGBT and GBCR number need to be identical")

        gbcrChannel = str(6 - eprx_group)
        data = []

        if gbcr_obj.gbcr_v == 2 or gbcr_obj.gbcr_v == 3 :

            if gbcr_obj.gbcr_v == 2:
                hf_prev = gbcr_obj.read("CH" + gbcrChannel + "UPLINK1", "CH" + gbcrChannel + "CTLEHFSR")
                mf_prev = gbcr_obj.read("CH" + gbcrChannel + "UPLINK1", "CH" + gbcrChannel + "CTLEMFSR")

            if gbcr_obj.gbcr_v == 3 :
                hf_prev = gbcr_obj.read("CH" + gbcrChannel + "UPLINK1", "CH" + gbcrChannel + "CTLEHF1SR")
                mf_prev = gbcr_obj.read("CH" + gbcrChannel + "UPLINK2", "CH" + gbcrChannel + "CTLEMFSR")


            for hfSetting in range(HFmin,HFmax+1,jump):
                for mfSetting in range(MFmin,MFmax+1,jump):
                    gbcr_obj.set_equalizer(eprx_group,mfSetting,hfSetting)
                    
                    output = lpgbt_obj.bert(eprx_group, bertmeastime,bertsource_c)

                    if filename is not None:
                        with open(filename + ".txt", "a") as f:
                            f.write(json.dumps({"CTLEHFSR": hfSetting, "CTLEMFSR": mfSetting, "results": output}))
                            f.write("\n")
                    data.append(json.dumps({"CTLEHFSR": hfSetting, "CTLEMFSR": mfSetting, "results": output}))
            
            gbcr_obj.set_equalizer(eprx_group,mf_prev,hf_prev)


        else:
            raise ValueError("Wrong GBCR version!")

        if plot:
            logger.info("Plotting the results of the BERT scan")
            self.plotHeatMap(filename, data)

    @staticmethod
    def upperLimit(BERT_error_count):
        """Calculating upper limit of BERT error count

        Args:
            BERT_error_count (int): number of BERT errors
        """
        poisson_upper_limits = [3.0, 4.74, 6.3, 7.75, 9.15, 10.51, 11.84, 13.15, 14.43, 15.71, 16.96]
        if BERT_error_count<0:
            return BERT_error_count
        if BERT_error_count<11:
            mu = poisson_upper_limits[int(BERT_error_count)]
        else:
            mu = BERT_error_count + 1.96*(BERT_error_count)**(1/2)
        return mu
    
    def softErrorCounter(self, D_OL_EG, meastime, url="http://felix:8000"):
        """The number of soft errors is monitored by checking the associated felix register, and outputted for the specified egroups - these can be monitored in parallel.
        
        Args:
            D_OL_EG (list of strings): example ["0_00_1"], where 0 is the device number, 00 is for felix channel 0 and 1 is egroup 1
            meastime (int): measuring time in seconds
            API (bool): use endpoint call to monitor the soft error
            url (str): url of the server with FELIX backend if the SR is not successful
            
        Raises:
            Exception: Unable to write felix registers
            Exception: Readout of FELIX register failed
        Returns:
            An imbedded dictionary with meastime as one key, and D_OL_EG as the remaining ones
        """
        
        try:
            url_felix = requests.get(os.environ.get("SR_URL")+"/get/?key=demi/lhep/itk-demo-optoboard/felix/url", headers={"content-type": "application/json"}).json()["demi/lhep/itk-demo-optoboard/felix/url"]
        except:
            url_felix = url
            logger.info("Using provided URL for FELIX backend") 
        full_dictionary = {}
        full_dictionary['Meastime'] = meastime
        alignment_status = []
        

        for pair in D_OL_EG:
            device = pair.split("_")[0]
            oplink = pair.split("_")[1]
            egroup = pair.split("_")[2]
            aligned_link = "DECODING_LINK_ALIGNED_" + oplink
            logger.info("Starting soft error counter for egroup " + egroup + " of optical link " + oplink)
            full_dictionary[pair] = {}
            
            aligned = requests.get(url_felix + "/flxconfigget/"+str(device)+"/" + aligned_link, headers={"content-type": "application/json"}).json()["result"].strip().split("=")[1][-(int(egroup)+1)]
            
            alignment_status.append(int(aligned))
            
            if (aligned!="1"):
                logger.error("Optical link " + oplink + " egroup " + egroup + ": Lost decoding alignment!")

            # select the egroup
            set_egroup = requests.get(url_felix + "/flxconfigset/"+str(device)+"/LINK_" + oplink + "_ERRORS_EGROUP_SELECT/" + egroup+"?enforce=true", headers={"content-type": "application/json"}).json()["result"]
            reset_counter = requests.get(url_felix + "/flxconfigset/"+str(device)+"/LINK_" + oplink + "_ERRORS_CLEAR_COUNTERS/1?enforce=true", headers={"content-type": "application/json"}).json()["result"]
            reset_counter = requests.get(url_felix + "/flxconfigset/"+str(device)+"/LINK_" + oplink + "_ERRORS_CLEAR_COUNTERS/0?enforce=true", headers={"content-type": "application/json"}).json()["result"]

            if (set_egroup != "") or (reset_counter != ""):
                logger.error("Unable to write felix registers! Check if felixcore is running, the validity of the provided arguments and the version of the felix fw/sw in use!")
                raise Exception("Unable to write felix registers!")   
        
        time.sleep(meastime)
        
        for i, entry in enumerate(D_OL_EG):
            device = entry.split("_")[0]
            oplink = entry.split("_")[1]
            egroup = entry.split("_")[2]

            
            aligned = requests.get(url_felix + "/flxconfigget/" +str(device)+"/"+ aligned_link, headers={"content-type": "application/json"}).json()["result"].strip().split("=")[1][-(int(egroup)+1)]

            try:
                if alignment_status[i] != 1: 
                    full_dictionary[entry]['SE counter'] = -1 
                    full_dictionary[entry]['BER limit'] = -1
                else:    
                    set_egroup = requests.get(url_felix + "/flxconfigset/"+str(device)+"/LINK_" + oplink + "_ERRORS_EGROUP_SELECT/" + egroup+"?enforce=true", headers={"content-type": "application/json"}).json()["result"]
                    output = int(requests.get(url_felix + "/flxconfigget/"+str(device)+"/LINK_" + oplink + "_ERRORS_COUNT", headers={"content-type": "application/json"}).json()["result"].strip().split("=")[1], 16)
                    averageErrors = Optoboard.upperLimit(output)
                    BER_limit = averageErrors/(1.28*10**9*62.12/100*meastime)
                    logger.info("Soft errors on optical link " + oplink + " egroup " + egroup + ": " + str(output))
                    full_dictionary[entry]['SE counter'] = output
                    full_dictionary[entry]['BER limit'] = BER_limit 
            except:
                logger.error("Readout of FELIX register failed!") 
                raise Exception("Readout of FELIX register failed!")             

            time.sleep(2)            
        
        return full_dictionary

    '''def softErrorCounter(self, D_OL_EG, meastime, API=True, url="http://felix:8000"):
        """The number of soft errors is monitored by checking the associated felix register.

        Args:
            D_OL_EG (list of strings): example ["0_00_1"], where 0 is the device number, 00 is for felix channel 0 and 1 is egroup 1
            meastime (int): measuring time in seconds
            API (bool): use endpoint call to monitor the soft error
            url (str): url of the server with FELIX backend, url of the server with FELIX backend if the SR is not successful
        Raises: 
            Exception: Unable to write felix registers
            Exception: Readout of FELIX register failed
        Returns:
            Dictionary with meastime, soft error counter and BER limit
        """
        try:
            url_felix = requests.get(os.environ.get("SR_URL")+"/get/?key=demi/lhep/itk-demo-optoboard/felix/url", headers={"content-type": "application/json"}).json()["demi/lhep/itk-demo-optoboard/felix/url"]
        except:
            url_felix = url
            logger.info("Using provided URL for FELIX backend") 

        
        logger.info("Starting soft error counter...")
        aligned_link = "DECODING_LINK_ALIGNED_" + optical_link
        felix_link = "LINK_" + optical_link + "_ERRORS_EGROUP" + str(egroup)
        counter = []

        if API:
            aligned = requests.get(url_felix + "/flxconfigget/"+str(device)+"/" + aligned_link, headers={"content-type": "application/json"}).json()["result"].strip().split("=")[1][-(egroup+1)]
            # select the egroup
            set_egroup = requests.get(url_felix + "/flxconfigset/"+str(device)+"/LINK_" + optical_link + "_ERRORS_EGROUP_SELECT/" + str(egroup)+"?enforce=true", headers={"content-type": "application/json"}).json()["result"]
            # reset the counter
            reset_counter = requests.get(url_felix + "/flxconfigset/"+str(device)+"/LINK_" + optical_link + "_ERRORS_CLEAR_COUNTERS/1?enforce=true", headers={"content-type": "application/json"}).json()["result"]
            reset_counter = requests.get(url_felix + "/flxconfigset/"+str(device)+"/LINK_" + optical_link + "_ERRORS_CLEAR_COUNTERS/0?enforce=true", headers={"content-type": "application/json"}).json()["result"]
            
            if (set_egroup != "") or (reset_counter != ""):
                logger.error("Unable to write felix registers! Check if felixcore is running, the validity of the provided arguments and the version of the felix fw/sw in use!")
                raise Exception("Unable to write felix registers!")
            
            if (aligned!="1"): #if 1 not in list_aligned 
                logger.info("Lost decoding alignment! Skipping to next setting!")
                return {"time": meastime, "SE counter": -1, "BER limit": -1}
            
            try:
                time.sleep(meastime)
                output = int(requests.get(url_felix + "/flxconfigget/"+str(device)+"/LINK_" + optical_link + "_ERRORS_COUNT", headers={"content-type": "application/json"}).json()["result"].strip().split("=")[1], 16)
            except:
                logger.error("Readout of FELIX register failed!") 
                raise Exception("Readout of FELIX register failed!")             
            if (output!=0):
                counter.append(output)
                # requests.get(url + "/flxconfigset/" + "LINK_" + optical_link + "_ERRORS_CLEAR_COUNTERS/1", headers={"content-type": "application/json"}).json()
            time.sleep(2)
        
            logger.info("Soft errors on optical link " + str(optical_link) + " egroup " + str(egroup) + ": " + str(output))
            
            averageErrors = Optoboard.upperLimit(output)
            BER_limit = averageErrors/(1.28*10**9*62.12/100*meastime)
            return {"time": meastime, "SE counter": output, "BER limit": BER_limit} 
        
        else:
            logger.warning("Only working for outdated firmware with short soft error registers!")
            subprocess.Popen(["flx-reset", "SOFT_RESET"], stdout=subprocess.PIPE)
            aligned = subprocess.Popen(["flx-config", "list"], stdout=subprocess.PIPE)
            aligned = subprocess.Popen(["grep", aligned_link], stdin=aligned.stdout, stdout=subprocess.PIPE).stdout.readlines()[0].decode().split()[4][-(egroup+1)]

            if (aligned!="1"): 
                logger.warning("Lost decoding alignment! Skipping to next setting!")
                return {"time": meastime, "SE counter": -1, "BER limit": -1}
            
            start_time = time.time()
            while (time.time()-start_time < meastime):
                try:
                    output = subprocess.Popen(["flx-config", "list"], stdout=subprocess.PIPE)
                    output = subprocess.Popen(["grep", felix_link], stdin=output.stdout, stdout=subprocess.PIPE).stdout.readlines()[0].decode().split()[4]
                except:
                    logger.error("Readout of FELIX register failed!") 
                    raise Exception("Readout of FELIX register failed!")
                if (output!="0x0"):
                    counter.append(int(output,16))
                    subprocess.Popen(["flx-reset", "SOFT_RESET"], stdout=subprocess.PIPE)
                time.sleep(2)

            total_errors = sum(counter)
            logger.info("Soft errors on optical link " + str(optical_link) + " egroup " + str(egroup) + ": " + str(total_errors))
            averageErrors = Optoboard.upperLimit(total_errors)
            BER_limit = averageErrors/(1.28*10**9*62.12/100*meastime)
            return {"time": meastime, "SE counter": total_errors, "BER limit": BER_limit}'''
    
    def softErrorScan(self, D_OL_EG, gbcr_name, meastime, directory="softErrorScan_result", plot=False, HFmin=0, HFmax=15, MFmin=0, MFmax=15, jump=1):
        """Perform a 2D soft error scan as a function of the parameters of the equalizer of the GBCR for one or multiple egroups of the same GBCR.

        Args:
            D_OL_EG (list of strings): example ["0_00_1"], where 0 is the device number, 00 is for felix channel 0 and 1 is egroup 1
            meastime (int): measuring time in seconds
            filename (str, optional): name of the file where the results are saved (default "bertScan_result"). Set to None for no result file.
            plot (bool, optional): create plot of scanned equalizer registers (default False)
            HFmin (int, optional): CTLE high frequency lower bound (default 0)
            HFmax (int, optional): CTLE high frequency upper bound (default 15)
            MFmin (int, optional): CTLE mid frequency lower bound (default 0)
            MFmax (int, optional): CTLE mid frequency upper bound (default 15)
            jump (int, optional): level of detail of the scan of the GBCR parameters (default 1)
        
        Raises:
            NameError: Initialisation failes
            Exception: FELIX register readout fails
            ValueError: GBCR version not supported
        """

        workspace = os.environ.get("WORKSPACE")
        if workspace is not None:
            directory = str(workspace) + "/" + directory
        if os.path.exists(directory):
            logger.error("Directory already exists! Try again with a different directory name.")
            raise NameError("Directory already exists! Try again with a different directory name.")
        else:
            os.makedirs(directory)
       
        try:
            gbcr_obj = getattr(self, gbcr_name)
        except:
            logger.error("Initialisation of the GBCR object failed!") 
            raise NameError("The initialisation of the chips for the soft error scan failed.")

        for i, entry in enumerate(D_OL_EG):
            device = entry.split("_")[0]
            oplink = entry.split("_")[1]
            egroup = entry.split("_")[2]
                    
            gbcrChannel = str(6 - int(egroup))
            data = []

            for hfSetting in range(HFmin,HFmax+1,jump):
                for mfSetting in range(MFmin,MFmax+1,jump):

                    if gbcr_obj.gbcr_v == 2 or gbcr_obj.gbcr_v == 3:

                        gbcr_obj.set_equalizer(int(egroup),mfSetting,hfSetting)
                
                    else:
                        raise ValueError("Wrong GBCR version!")

                    counter = self.softErrorCounter([entry], meastime)
                    print("why: ", counter)
                    
                    data.append(json.dumps({"CTLEHFSR": hfSetting, "CTLEMFSR": mfSetting, "results": counter}))

            file_name = directory + "/" + entry
            with open(file_name + ".txt", "a") as f:
                f.writelines([f"{line}\n" for line in data])
                f.close()
            
            if plot:
                logger.info("Plotting the results of the Soft error scan")
                self.plotSoftErrorHeatMap(file_name, entry, data)

    def flx_info_link(self, url="http://felix:8000"):
        """Check optical alignment via flx-info link through FELIX endpoint
            url (str): url of the server with FELIX backend, url of the server with FELIX backend if the SR is not successful
        Return:
            optical_alignment (dict): dictionary with integer keys and boolean values parsing the return of flx-info link
        """
        try:
            url_felix = requests.get(os.environ.get("SR_URL")+"/get/?key=demi/lhep/itk-demo-optoboard/felix/url", headers={"content-type": "application/json"}).json()["demi/lhep/itk-demo-optoboard/felix/url"]
        except:
            url_felix = url
            logger.info("Using provided URL for FELIX backend") 

        optical_alignment = requests.get(url_felix + "/getlink", headers={"content-type": "application/json"}).json()

        return optical_alignment

        
    def dump_opto_config(self,filename='custom_config'):
        '''Function that creates copy of register configuration file and 
           overwrites with any user-made changes
           
           Args:
           filename (str): name given to config file that's produced when function is executed. The files are saved in 
                            the directory "custom-configs"
        '''
        #retrieve register maps
        lpgbt1_reg_map = LpgbtRegisterMapV1()
        vtrx13_reg_map = VTRxplusRegisterMapV1_3()
        gbcr2_reg_map = GBCRRegisterMap_V2()
        gbcr3_reg_map = GBCRRegisterMap_V3()
        
        
        new_config_file = self.config_file
        gbcr_obj = getattr(self, "gbcr1")
        
        lpgbt1_reg_val = self.lpgbt1.read_multi()
        lpgbt2_reg_val = self.lpgbt2.read_multi()
        lpgbt3_reg_val = self.lpgbt3.read_multi()
        lpgbt4_reg_val = self.lpgbt4.read_multi()

        for register in new_config_file["lpgbt"]:
            for subregister in inspect.getmembers(getattr(lpgbt1_reg_map,register)):
                if inspect.isclass(subregister[1])==False:
                    continue
                subregister = getattr(getattr(lpgbt1_reg_map, register), subregister[0])
                if hasattr(subregister,'offset')==False:
                    continue
                read_reg1 = lpgbt1_reg_val[register] >> getattr(subregister,'offset') & (2**getattr(subregister,'length')-1)
                read_reg2 = lpgbt2_reg_val[register] >> getattr(subregister,'offset') & (2**getattr(subregister,'length')-1)
                read_reg3 = lpgbt3_reg_val[register] >> getattr(subregister,'offset') & (2**getattr(subregister,'length')-1)
                read_reg4 = lpgbt4_reg_val[register] >> getattr(subregister,'offset') & (2**getattr(subregister,'length')-1)
                new_config_file["lpgbt"][str(register)][subregister.__name__] = [read_reg1,read_reg2,read_reg3,read_reg4]
        
        if gbcr_obj.gbcr_v == 2:
            
            gbcr1_reg_val = self.gbcr1.read_multi()
            gbcr2_reg_val = self.gbcr2.read_multi()
            gbcr3_reg_val = self.gbcr3.read_multi()
            gbcr4_reg_val = self.gbcr4.read_multi()

            for register in new_config_file["gbcr"]:
                if any(map(register.__contains__, ['Reg', '__'])):
                    for subregister in inspect.getmembers(getattr(gbcr2_reg_map,register)):
                        if inspect.isclass(subregister[1])==False:
                            continue
                        subregister = getattr(getattr(gbcr2_reg_map, register), subregister[0])
                        if hasattr(subregister,'offset')==False:
                            continue
                        read_reg1 = gbcr1_reg_val[register] >> getattr(subregister,'offset') & (2**getattr(subregister,'length')-1)
                        read_reg2 = gbcr2_reg_val[register] >> getattr(subregister,'offset') & (2**getattr(subregister,'length')-1)
                        read_reg3 = gbcr3_reg_val[register] >> getattr(subregister,'offset') & (2**getattr(subregister,'length')-1)
                        read_reg4 = gbcr4_reg_val[register] >> getattr(subregister,'offset') & (2**getattr(subregister,'length')-1)
                        new_config_file["gbcr"][str(register)][subregister.__name__] = [read_reg1,read_reg2,read_reg3,read_reg4]


        elif gbcr_obj.gbcr_v == 3:
            
            gbcr1_reg_val = self.gbcr1.read_multi()
            gbcr2_reg_val = self.gbcr2.read_multi()
            gbcr3_reg_val = self.gbcr3.read_multi()
            gbcr4_reg_val = self.gbcr4.read_multi()

            for register in new_config_file["gbcr"]:
                if any(map(register.__contains__, ['Reg', '__'])):
                    for subregister in inspect.getmembers(getattr(lpgbt1_reg_map,register)):
                        if inspect.isclass(subregister[1])==False:
                            continue
                        subregister = getattr(getattr(gbcr3_reg_map, register), subregister[0])
                        if hasattr(subregister,'offset')==False:
                            continue
                        read_reg1 = gbcr1_reg_val[register] >> getattr(subregister,'offset') & (2**getattr(subregister,'length')-1)
                        read_reg2 = gbcr2_reg_val[register] >> getattr(subregister,'offset') & (2**getattr(subregister,'length')-1)
                        read_reg3 = gbcr3_reg_val[register] >> getattr(subregister,'offset') & (2**getattr(subregister,'length')-1)
                        read_reg4 = gbcr4_reg_val[register] >> getattr(subregister,'offset') & (2**getattr(subregister,'length')-1)
                        new_config_file["gbcr"][str(register)][subregister.__name__] = [read_reg1,read_reg2,read_reg3,read_reg4]
        workspace = os.environ.get("WORKSPACE")
        if workspace is not None:
            file_name = str(workspace) + "/" + filename + ".json"
        else:
            file_name = filename + ".json"
        with open(str(file_name),"w") as output:
            json.dump(new_config_file,output,indent=2)
        logger.info("File created")
        return "Configuration file " + str(file_name) + " created"


    def dump_regs(self, filename=None):
        """Write all registers of all devices of the Optoboard to a json file.
        
        Args:
            filename (str, optional): provide a custom filename otherwise the default will be <optoboard_serial>_<time>.json
        """

        dump = {}

        for device_str, device in self.devices.items():
            logger.info("Reading %s for register dump..", device_str)
            dump[device_str] = device.read_multi()

        path = os.environ.get("WORKSPACE") + "/"

        if filename == None:
            current_time = datetime.now().strftime("%Y-%m-%d_%H%M%S")
            filename = f"{self.optoboard_serial}_{current_time}.json"

        with open(path + filename, 'w') as json_file:
            json.dump(dump, json_file, indent=4)

        logger.info("Register dump saved here: %s", path + filename)
        
    @staticmethod
    def plotHeatMap(filename, data):
        """Plot heat map of the BERT equalizer scan results.

        Args:
            filename (str): name of the plot file generated
            data (list): data from the scan
        """
        xPoints = np.array([json.loads(x)["CTLEHFSR"] for x in data])
        xPoints = np.sort(np.unique(xPoints))
        yPoints = np.array([json.loads(x)["CTLEMFSR"] for x in data])
        yPoints = np.sort(np.unique(yPoints))
        heatmap = np.array([json.loads(x)["results"]["BER_limit"] for x in data])
        heatmap = np.transpose(np.reshape(heatmap, (len(xPoints), len(yPoints))))

        plt.figure(figsize=(8,6))
        plt.imshow(heatmap, cmap = "hot", norm=colors.LogNorm(), origin="lower", extent=[xPoints[0]-0.5, xPoints[-1] + 0.5, yPoints[0]-0.5, yPoints[-1] + 0.5])
        plt.xticks(xPoints)
        plt.yticks(yPoints)
        plt.ylabel("CTLEMFSR")
        plt.xlabel("CTLEHFSR")
        plt.title("GBCR equalizer scan of the BER limit")
        plt.colorbar()
        plt.clim(10**(-12), 0.5)
        plotname = filename.split(".")[0] + "_plot.png"
        plt.savefig(plotname, format="png")

    @staticmethod
    def plotSoftErrorHeatMap(filename, D_OL_EG, data=None):
        """Plot heat map of the BERT equalizer scan results.

        Args:
            D_OL_EG (strings): example "0_00_1", where 0 is the device number, 00 is for felix channel 0 and 1 is egroup 1
            filename (str): name of the plot file generated
            data (list): data from the scan
            time (int): duration of the test in seconds, used for the BER limit calculation
        """
        if data is None:
            with open(filename) as f:
                data = f.readlines()
        
        xPoints = np.array([json.loads(x)["CTLEHFSR"] for x in data])
        xPoints = np.sort(np.unique(xPoints))
        yPoints = np.array([json.loads(x)["CTLEMFSR"] for x in data])
        yPoints = np.sort(np.unique(yPoints))
        
        heatmap = np.array([json.loads(x)["results"][D_OL_EG]["SE counter"] for x in data])
        heatmap = np.ma.masked_where(heatmap<0, heatmap)
        heatmap = np.transpose(np.reshape(heatmap, (len(xPoints), len(yPoints))))
        heatmap_bert = np.array([json.loads(x)["results"][D_OL_EG]["BER limit"] for x in data])
        heatmap_bert = np.ma.masked_where(heatmap_bert<0, heatmap_bert)
        heatmap_bert = np.transpose(np.reshape(heatmap_bert, (len(xPoints), len(yPoints))))
        
        #logger.info(heatmap) 
        #logger.info(heatmap_bert)

        plt.figure(figsize=(8,6))
        cmap = cm.get_cmap("summer").copy()
        cmap.set_bad('black',1.)
        plt.imshow(heatmap, cmap = cmap, origin="lower", extent=[xPoints[0]-0.5, xPoints[-1] + 0.5, yPoints[0]-0.5, yPoints[-1] + 0.5])
        plt.xticks(xPoints)
        plt.yticks(yPoints)
        plt.ylabel("CTLEMFSR")
        plt.xlabel("CTLEHFSR")
        plt.title("Soft errors scan")
        cbar = plt.colorbar()
        cbar.set_label('Number of soft errors')
        plotname = filename.split(".")[0] + "_plot.png"
        plt.savefig(plotname, format="png")

        plt.figure(figsize=(8,6))
        cmap = cm.get_cmap("summer").copy()
        cmap.set_bad('black',1.)
        plt.imshow(heatmap_bert, cmap = cmap, origin="lower", extent=[xPoints[0]-0.5, xPoints[-1] + 0.5, yPoints[0]-0.5, yPoints[-1] + 0.5])
        plt.xticks(xPoints)
        plt.yticks(yPoints)
        plt.ylabel("CTLEMFSR")
        plt.xlabel("CTLEHFSR")
        plt.title("Soft errors BER limit")
        cbar = plt.colorbar()
        cbar.set_label('BER limit')
        plotname = filename.split(".")[0] + "_BERLimit_plot.png"
        plt.savefig(plotname, format="png")   
        