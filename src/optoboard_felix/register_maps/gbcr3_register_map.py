"""GBCR3 Constants."""
"""Channels here follow convention of GBCR CH = 6 - LPGBT CH"""

from enum import IntEnum, unique


class GBCRRegisterMap:
    """Class containing all GBCR-related constants"""

    class CH6UPLINK0:
        """Uplink channel 1 settings (1/4)"""
        address = 0x00

        @staticmethod
        def __str__():
            return "CH6UPLINK0"

        @staticmethod
        def __int__():
            return 0x00


        class CH6DIS:
            """Disable channel"""

            offset = 6
            length = 1
            bit_mask = 64
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)


        class CH6DISLPF:
            """disable DC offset cancellation"""

            offset = 5
            length = 1
            bit_mask = 32
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)


        class CH6MUX:
            """select the output signal
                5'b01111 retimed signal of the voter 
                5'b10111 voted signal 
                5'b11011 equalizer signal A 
                5'b11101 equalizer signal B 
                5'b11110 equalizer signal C 
            """

            offset = 0
            length = 5
            bit_mask = 31
            default = 8

            @staticmethod
            def validate(value):
                return value in range(32)


    class CH6UPLINK1:
        """Uplink channel 1 settings (2/4)"""

        address = 0x01

        @staticmethod
        def __str__():
            return "CH6UPLINK1"

        @staticmethod
        def __int__():
            return 0x01

        class CH6CTLEHF1SR:
            """Set the high Frequency CTLE peaking strength"""

            offset = 4
            length = 4
            bit_mask = 240
            default = 11

            @staticmethod
            def validate(value):
                return value in range(16)

        class CH6CTLEHF2SR:
            """Set the high Frequency CTLE peaking strength"""

            offset = 0
            length = 4
            bit_mask = 15
            default = 11

            @staticmethod
            def validate(value):
                return value in range(16)


    class CH6UPLINK2:
        """Uplink channel 1 settings (3/4)"""

        address = 0x02

        @staticmethod
        def __str__():
            return "CH6UPLINK2"

        @staticmethod
        def __int__():
            return 0x02

        class CH6CTLEHF3SR:
            """Set the high Frequency CTLE peaking strength"""

            offset = 4
            length = 4
            bit_mask = 240
            default = 11

            @staticmethod
            def validate(value):
                return value in range(16)

        class CH6CTLEMFSR:
            """Set the middle Frequency CTLE peaking strength"""

            offset = 0
            length = 4
            bit_mask = 15
            default = 11

            @staticmethod
            def validate(value):
                return value in range(16)


    class CH6UPLINK3:
        """Uplink channel 1 settings (4/4)"""

        address = 0x03

        @staticmethod
        def __str__():
            return "CH6UPLINK3"

        @staticmethod
        def __int__():
            return 0x03

        class CH6DISEQLF:
            """disable the low Frequency CTLE Stage"""

            offset = 7
            length = 1
            bit_mask = 128
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)

        class CH6CMLAMPLSEL:
            """Set the output amplitude"""

            offset = 4
            length = 3
            bit_mask = 112
            default = 7

            @staticmethod
            def validate(value):
                return value in range(8)

        class CH6CLKDELAY:
            """Set the output amplitude"""

            offset = 0
            length = 4
            bit_mask = 15
            default = 5

            @staticmethod
            def validate(value):
                return value in range(16)



    class CH5UPLINK0:
        """Uplink channel 1 settings (1/4)"""
        address = 0x04

        @staticmethod
        def __str__():
            return "CH5UPLINK0"

        @staticmethod
        def __int__():
            return 0x04


        class CH5DIS:
            """Disable channel"""

            offset = 6
            length = 1
            bit_mask = 64
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)


        class CH5DISLPF:
            """disable DC offset cancellation"""

            offset = 5
            length = 1
            bit_mask = 32
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)


        class CH5MUX:
            """select the output signal
                5'b01111 retimed signal of the voter 
                5'b10111 voted signal 
                5'b11011 equalizer signal A 
                5'b11101 equalizer signal B 
                5'b11110 equalizer signal C 
            """

            offset = 0
            length = 5
            bit_mask = 31
            default = 8

            @staticmethod
            def validate(value):
                return value in range(32)


    class CH5UPLINK1:
        """Uplink channel 1 settings (2/4)"""

        address = 0x05

        @staticmethod
        def __str__():
            return "CH5UPLINK1"

        @staticmethod
        def __int__():
            return 0x05

        class CH5CTLEHF1SR:
            """Set the high Frequency CTLE peaking strength"""

            offset = 4
            length = 4
            bit_mask = 240
            default = 11

            @staticmethod
            def validate(value):
                return value in range(16)

        class CH5CTLEHF2SR:
            """Set the high Frequency CTLE peaking strength"""

            offset = 0
            length = 4
            bit_mask = 15
            default = 11

            @staticmethod
            def validate(value):
                return value in range(16)


    class CH5UPLINK2:
        """Uplink channel 1 settings (3/4)"""

        address = 0x06

        @staticmethod
        def __str__():
            return "CH5UPLINK2"

        @staticmethod
        def __int__():
            return 0x06

        class CH5CTLEHF3SR:
            """Set the high Frequency CTLE peaking strength"""

            offset = 4
            length = 4
            bit_mask = 240
            default = 11

            @staticmethod
            def validate(value):
                return value in range(16)

        class CH5CTLEMFSR:
            """Set the middle Frequency CTLE peaking strength"""

            offset = 0
            length = 4
            bit_mask = 15
            default = 11

            @staticmethod
            def validate(value):
                return value in range(16)


    class CH5UPLINK3:
        """Uplink channel 1 settings (4/4)"""

        address = 0x07

        @staticmethod
        def __str__():
            return "CH5UPLINK3"

        @staticmethod
        def __int__():
            return 0x07

        class CH5DISEQLF:
            """disable the low Frequency CTLE Stage"""

            offset = 7
            length = 1
            bit_mask = 128
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)

        class CH5CMLAMPLSEL:
            """Set the output amplitude"""

            offset = 4
            length = 3
            bit_mask = 112
            default = 7

            @staticmethod
            def validate(value):
                return value in range(8)

        class CH5CLKDELAY:
            """Set the output amplitude"""

            offset = 0
            length = 4
            bit_mask = 15
            default = 5

            @staticmethod
            def validate(value):
                return value in range(16)



    class CH4UPLINK0:
        """Uplink channel 1 settings (1/4)"""
        address = 0x08

        @staticmethod
        def __str__():
            return "CH4UPLINK0"

        @staticmethod
        def __int__():
            return 0x08


        class CH4DIS:
            """Disable channel"""

            offset = 6
            length = 1
            bit_mask = 64
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)


        class CH4DISLPF:
            """disable DC offset cancellation"""

            offset = 5
            length = 1
            bit_mask = 32
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)


        class CH4MUX:
            """select the output signal
                5'b01111 retimed signal of the voter 
                5'b10111 voted signal 
                5'b11011 equalizer signal A 
                5'b11101 equalizer signal B 
                5'b11110 equalizer signal C 
            """

            offset = 0
            length = 5
            bit_mask = 31
            default = 8

            @staticmethod
            def validate(value):
                return value in range(32)


    class CH4UPLINK1:
        """Uplink channel 1 settings (2/4)"""

        address = 0x09

        @staticmethod
        def __str__():
            return "CH4UPLINK1"

        @staticmethod
        def __int__():
            return 0x09

        class CH4CTLEHF1SR:
            """Set the high Frequency CTLE peaking strength"""

            offset = 4
            length = 4
            bit_mask = 240
            default = 11

            @staticmethod
            def validate(value):
                return value in range(16)

        class CH4CTLEHF2SR:
            """Set the high Frequency CTLE peaking strength"""

            offset = 0
            length = 4
            bit_mask = 15
            default = 11

            @staticmethod
            def validate(value):
                return value in range(16)


    class CH4UPLINK2:
        """Uplink channel 1 settings (3/4)"""

        address = 0x0a

        @staticmethod
        def __str__():
            return "CH4UPLINK2"

        @staticmethod
        def __int__():
            return 0x0a

        class CH4CTLEHF3SR:
            """Set the high Frequency CTLE peaking strength"""

            offset = 4
            length = 4
            bit_mask = 240
            default = 11

            @staticmethod
            def validate(value):
                return value in range(16)

        class CH4CTLEMFSR:
            """Set the middle Frequency CTLE peaking strength"""

            offset = 0
            length = 4
            bit_mask = 15
            default = 11

            @staticmethod
            def validate(value):
                return value in range(16)


    class CH4UPLINK3:
        """Uplink channel 1 settings (4/4)"""

        address = 0x0b

        @staticmethod
        def __str__():
            return "CH4UPLINK3"

        @staticmethod
        def __int__():
            return 0x0b

        class CH4DISEQLF:
            """disable the low Frequency CTLE Stage"""

            offset = 7
            length = 1
            bit_mask = 128
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)

        class CH4CMLAMPLSEL:
            """Set the output amplitude"""

            offset = 4
            length = 3
            bit_mask = 112
            default = 7

            @staticmethod
            def validate(value):
                return value in range(8)

        class CH4CLKDELAY:
            """Set the output amplitude"""

            offset = 0
            length = 4
            bit_mask = 15
            default = 5

            @staticmethod
            def validate(value):
                return value in range(16)



    class CH3UPLINK0:
        """Uplink channel 1 settings (1/4)"""
        address = 0x0c

        @staticmethod
        def __str__():
            return "CH3UPLINK0"

        @staticmethod
        def __int__():
            return 0x0c


        class CH3DIS:
            """Disable channel"""

            offset = 6
            length = 1
            bit_mask = 64
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)


        class CH3DISLPF:
            """disable DC offset cancellation"""

            offset = 5
            length = 1
            bit_mask = 32
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)


        class CH3MUX:
            """select the output signal
                5'b01111 retimed signal of the voter 
                5'b10111 voted signal 
                5'b11011 equalizer signal A 
                5'b11101 equalizer signal B 
                5'b11110 equalizer signal C 
            """

            offset = 0
            length = 5
            bit_mask = 31
            default = 8

            @staticmethod
            def validate(value):
                return value in range(32)


    class CH3UPLINK1:
        """Uplink channel 1 settings (2/4)"""

        address = 0x0d

        @staticmethod
        def __str__():
            return "CH3UPLINK1"

        @staticmethod
        def __int__():
            return 0x0d

        class CH3CTLEHF1SR:
            """Set the high Frequency CTLE peaking strength"""

            offset = 4
            length = 4
            bit_mask = 240
            default = 11

            @staticmethod
            def validate(value):
                return value in range(16)

        class CH3CTLEHF2SR:
            """Set the high Frequency CTLE peaking strength"""

            offset = 0
            length = 4
            bit_mask = 15
            default = 11

            @staticmethod
            def validate(value):
                return value in range(16)


    class CH3UPLINK2:
        """Uplink channel 1 settings (3/4)"""

        address = 0x0e

        @staticmethod
        def __str__():
            return "CH3UPLINK2"

        @staticmethod
        def __int__():
            return 0x0e

        class CH3CTLEHF3SR:
            """Set the high Frequency CTLE peaking strength"""

            offset = 4
            length = 4
            bit_mask = 240
            default = 11

            @staticmethod
            def validate(value):
                return value in range(16)

        class CH3CTLEMFSR:
            """Set the middle Frequency CTLE peaking strength"""

            offset = 0
            length = 4
            bit_mask = 15
            default = 11

            @staticmethod
            def validate(value):
                return value in range(16)


    class CH3UPLINK3:
        """Uplink channel 1 settings (4/4)"""

        address = 0x0f

        @staticmethod
        def __str__():
            return "CH3UPLINK3"

        @staticmethod
        def __int__():
            return 0x0f

        class CH3DISEQLF:
            """disable the low Frequency CTLE Stage"""

            offset = 7
            length = 1
            bit_mask = 128
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)

        class CH3CMLAMPLSEL:
            """Set the output amplitude"""

            offset = 4
            length = 3
            bit_mask = 112
            default = 7

            @staticmethod
            def validate(value):
                return value in range(8)

        class CH3CLKDELAY:
            """Set the output amplitude"""

            offset = 0
            length = 4
            bit_mask = 15
            default = 5

            @staticmethod
            def validate(value):
                return value in range(16)



    class CH2UPLINK0:
        """Uplink channel 1 settings (1/4)"""
        address = 0x10

        @staticmethod
        def __str__():
            return "CH2UPLINK0"

        @staticmethod
        def __int__():
            return 0x10


        class CH2DIS:
            """Disable channel"""

            offset = 6
            length = 1
            bit_mask = 64
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)


        class CH2DISLPF:
            """disable DC offset cancellation"""

            offset = 5
            length = 1
            bit_mask = 32
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)


        class CH2MUX:
            """select the output signal
                5'b01111 retimed signal of the voter 
                5'b10111 voted signal 
                5'b11011 equalizer signal A 
                5'b11101 equalizer signal B 
                5'b11110 equalizer signal C 
            """

            offset = 0
            length = 5
            bit_mask = 31
            default = 8

            @staticmethod
            def validate(value):
                return value in range(32)


    class CH2UPLINK1:
        """Uplink channel 1 settings (2/4)"""

        address = 0x11

        @staticmethod
        def __str__():
            return "CH2UPLINK1"

        @staticmethod
        def __int__():
            return 0x11

        class CH2CTLEHF1SR:
            """Set the high Frequency CTLE peaking strength"""

            offset = 4
            length = 4
            bit_mask = 240
            default = 11

            @staticmethod
            def validate(value):
                return value in range(16)

        class CH2CTLEHF2SR:
            """Set the high Frequency CTLE peaking strength"""

            offset = 0
            length = 4
            bit_mask = 15
            default = 11

            @staticmethod
            def validate(value):
                return value in range(16)


    class CH2UPLINK2:
        """Uplink channel 1 settings (3/4)"""

        address = 0x12

        @staticmethod
        def __str__():
            return "CH2UPLINK2"

        @staticmethod
        def __int__():
            return 0x12

        class CH2CTLEHF3SR:
            """Set the high Frequency CTLE peaking strength"""

            offset = 4
            length = 4
            bit_mask = 240
            default = 11

            @staticmethod
            def validate(value):
                return value in range(16)

        class CH2CTLEMFSR:
            """Set the middle Frequency CTLE peaking strength"""

            offset = 0
            length = 4
            bit_mask = 15
            default = 11

            @staticmethod
            def validate(value):
                return value in range(16)


    class CH2UPLINK3:
        """Uplink channel 1 settings (4/4)"""

        address = 0x13

        @staticmethod
        def __str__():
            return "CH2UPLINK3"

        @staticmethod
        def __int__():
            return 0x13

        class CH2DISEQLF:
            """disable the low Frequency CTLE Stage"""

            offset = 7
            length = 1
            bit_mask = 128
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)

        class CH2CMLAMPLSEL:
            """Set the output amplitude"""

            offset = 4
            length = 3
            bit_mask = 112
            default = 7

            @staticmethod
            def validate(value):
                return value in range(8)

        class CH2CLKDELAY:
            """Set the output amplitude"""

            offset = 0
            length = 4
            bit_mask = 15
            default = 5

            @staticmethod
            def validate(value):
                return value in range(16)



    class CH1UPLINK0:
        """Uplink channel 1 settings (1/4)"""
        address = 0x14

        @staticmethod
        def __str__():
            return "CH1UPLINK0"

        @staticmethod
        def __int__():
            return 0x14


        class CH1DIS:
            """Disable channel"""

            offset = 6
            length = 1
            bit_mask = 64
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)


        class CH1DISLPF:
            """disable DC offset cancellation"""

            offset = 5
            length = 1
            bit_mask = 32
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)


        class CH1MUX:
            """select the output signal
                5'b01111 retimed signal of the voter 
                5'b10111 voted signal 
                5'b11011 equalizer signal A 
                5'b11101 equalizer signal B 
                5'b11110 equalizer signal C 
            """

            offset = 0
            length = 5
            bit_mask = 31
            default = 8

            @staticmethod
            def validate(value):
                return value in range(32)


    class CH1UPLINK1:
        """Uplink channel 1 settings (2/4)"""

        address = 0x15

        @staticmethod
        def __str__():
            return "CH1UPLINK1"

        @staticmethod
        def __int__():
            return 0x15

        class CH1CTLEHF1SR:
            """Set the high Frequency CTLE peaking strength"""

            offset = 4
            length = 4
            bit_mask = 240
            default = 11

            @staticmethod
            def validate(value):
                return value in range(16)

        class CH1CTLEHF2SR:
            """Set the high Frequency CTLE peaking strength"""

            offset = 0
            length = 4
            bit_mask = 15
            default = 11

            @staticmethod
            def validate(value):
                return value in range(16)


    class CH1UPLINK2:
        """Uplink channel 1 settings (3/4)"""

        address = 0x16

        @staticmethod
        def __str__():
            return "CH1UPLINK2"

        @staticmethod
        def __int__():
            return 0x16

        class CH1CTLEHF3SR:
            """Set the high Frequency CTLE peaking strength"""

            offset = 4
            length = 4
            bit_mask = 240
            default = 11

            @staticmethod
            def validate(value):
                return value in range(16)

        class CH1CTLEMFSR:
            """Set the middle Frequency CTLE peaking strength"""

            offset = 0
            length = 4
            bit_mask = 15
            default = 11

            @staticmethod
            def validate(value):
                return value in range(16)


    class CH1UPLINK3:
        """Uplink channel 1 settings (4/4)"""

        address = 0x17

        @staticmethod
        def __str__():
            return "CH1UPLINK3"

        @staticmethod
        def __int__():
            return 0x17

        class CH1DISEQLF:
            """disable the low Frequency CTLE Stage"""

            offset = 7
            length = 1
            bit_mask = 128
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)

        class CH1CMLAMPLSEL:
            """Set the output amplitude"""

            offset = 4
            length = 3
            bit_mask = 112
            default = 7

            @staticmethod
            def validate(value):
                return value in range(8)

        class CH1CLKDELAY:
            """Adjust the sampling clock phase (only yseful in retiming mode)"""

            offset = 0
            length = 4
            bit_mask = 15
            default = 5

            @staticmethod
            def validate(value):
                return value in range(16)

# --------------------------------------------------------------------

    class CH1DOWNLINK0:

        address = 0x18

        @staticmethod
        def __str__():
            return "CH1DOWNLINK0"

        @staticmethod
        def __int__():
            return 0x18

        class TX1SC2:
            """Set the capacitance in CTLE structure"""

            offset = 4
            length = 4
            bit_mask = 240
            default = 1

            @staticmethod
            def validate(value):
                return value in range(16)

        class TX1SC1:
            """Set the capacitance in CTLE structure"""

            offset = 0
            length = 4
            bit_mask = 15
            default = 7

            @staticmethod
            def validate(value):
                return value in range(16)


    class CH1DOWNLINK1:

        address = 0x19

        @staticmethod
        def __str__():
            return "CH1DOWNLINK1"

        @staticmethod
        def __int__():
            return 0x19

        class TX1AMP:
            """Set the capacitance in CTLE structure"""

            offset = 5
            length = 3
            bit_mask = 224
            default = 7

            @staticmethod
            def validate(value):
                return value in range(8)

        class TX1SR1:
            """Set the resistance in CTLE structure"""

            offset = 0
            length = 5
            bit_mask = 31
            default = 25

            @staticmethod
            def validate(value):
                return value in range(32)


    class CH1DOWNLINK2:

        address = 0x1a

        @staticmethod
        def __str__():
            return "CH1DOWNLINK2"

        @staticmethod
        def __int__():
            return 0x1a

        class TX1SR2:
            """Set the resistance in CTLE structure"""

            offset = 2
            length = 5
            bit_mask = 124
            default = 25

            @staticmethod
            def validate(value):
                return value in range(32)

        class TX1DISPREEMPH:
            """Disable the preemphasis function when true"""

            offset = 1
            length = 1
            bit_mask = 2
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)

        class TX1DISBIAS:
            """Disable the tx channel when true"""

            offset = 0
            length = 1
            bit_mask = 1
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)


    class CH2DOWNLINK0:

        address = 0x1b

        @staticmethod
        def __str__():
            return "CH2DOWNLINK0"

        @staticmethod
        def __int__():
            return 0x1b

        class TX2SC2:
            """Set the capacitance in CTLE structure"""

            offset = 4
            length = 4
            bit_mask = 240
            default = 1

            @staticmethod
            def validate(value):
                return value in range(16)

        class TX2SC1:
            """Set the capacitance in CTLE structure"""

            offset = 0
            length = 4
            bit_mask = 15
            default = 7

            @staticmethod
            def validate(value):
                return value in range(16)


    class CH2DOWNLINK1:

        address = 0x1c

        @staticmethod
        def __str__():
            return "CH2DOWNLINK1"

        @staticmethod
        def __int__():
            return 0x1c

        class TX2AMP:
            """Adjust the amplitude of the output signal"""

            offset = 5
            length = 3
            bit_mask = 224
            default = 7

            @staticmethod
            def validate(value):
                return value in range(8)

        class TX2SR1:
            """Set the resistance in CTLE structure"""

            offset = 0
            length = 5
            bit_mask = 31
            default = 25

            @staticmethod
            def validate(value):
                return value in range(32)


    class CH2DOWNLINK2:

        address = 0x1d

        @staticmethod
        def __str__():
            return "CH2DOWNLINK2"

        @staticmethod
        def __int__():
            return 0x1d

        class TX2SR2:
            """Set the resistance in CTLE structure"""

            offset = 2
            length = 5
            bit_mask = 124
            default = 25

            @staticmethod
            def validate(value):
                return value in range(32)

        class TX2DISPREEMPH:
            """Disable the preemphasis function when true"""

            offset = 1
            length = 1
            bit_mask = 2
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)

        class TX2DISBIAS:
            """Disable the tx channel when true"""

            offset = 0
            length = 1
            bit_mask = 1
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)

    #----------------------------------------------
    # phaseShifter is only useful when RX channels work in retiming mode

    class PHASESHIFTER0:
        """Phase shifter clock settings"""
        address = 0x1e

        @staticmethod
        def __str__():
            return "PHASESHIFTER0"

        @staticmethod
        def __int__():
            return 0x1e

        class CLKRXENABLE:
            """enable input 1.28GHz when true. It can be disabled if RX channel works in equalizer mode"""

            offset = 5
            length = 1
            bit_mask = 32
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)


        class CLKTXDELAY:
            """Set clock delay"""

            offset = 1
            length = 4
            bit_mask = 30
            default = 0

            @staticmethod
            def validate(value):
                return value in range(16)


        class DISCLKTX:
            """Disable test clock output when true"""

            offset = 0
            length = 1
            bit_mask = 1
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)


    class PHASESHIFTER1:
        """Phase shifter DLL settings"""
        address = 0x1f

        @staticmethod
        def __str__():
            return "PHASESHIFTER1"

        @staticmethod
        def __int__():
            return 0x1f


        class DLLCHARGEPUMPCURRENT:
            """Set the Charge pump current"""

            offset = 3
            length = 4
            bit_mask = 120
            default = 8

            @staticmethod
            def validate(value):
                return value in range(16)


        class DLLFORCEDOWN:
            """Force down the charge pump output"""

            offset = 2
            length = 1
            bit_mask = 4
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)


        class DLLENABLE:
            """Enables dll. No need when Rx channels are in equalizer mode"""

            offset = 1
            length = 1
            bit_mask = 2
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)


        class DLLCAPRESET:
            """dll cap reset, same as in gbcr2"""

            offset = 0
            length = 1
            bit_mask = 1
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)


    @unique
    class Reg(IntEnum):
        CH6UPLINK0 = 0x00
        CH6UPLINK1 = 0x01
        CH6UPLINK2 = 0x02
        CH6UPLINK3 = 0x03
        CH5UPLINK0 = 0x04
        CH5UPLINK1 = 0x05
        CH5UPLINK2 = 0x06
        CH5UPLINK3 = 0x07
        CH4UPLINK0 = 0x08
        CH4UPLINK1 = 0x09
        CH4UPLINK2 = 0x0a
        CH4UPLINK3 = 0x0b
        CH3UPLINK0 = 0x0c
        CH3UPLINK1 = 0x0d
        CH3UPLINK2 = 0x0e
        CH3UPLINK3 = 0x0f
        CH2UPLINK0 = 0x10
        CH2UPLINK1 = 0x11
        CH2UPLINK2 = 0x12
        CH2UPLINK3 = 0x13
        CH1UPLINK0 = 0x14
        CH1UPLINK1 = 0x15
        CH1UPLINK2 = 0x16
        CH1UPLINK3 = 0x17
        CH1DOWNLINK0 = 0x18
        CH1DOWNLINK1 = 0x19
        CH1DOWNLINK2 = 0x1a
        CH2DOWNLINK0 = 0x1b
        CH2DOWNLINK1 = 0x1c
        CH2DOWNLINK2 = 0x1d
        PHASESHIFTER0 = 0x1e
        PHASESHIFTER1 = 0x1f
