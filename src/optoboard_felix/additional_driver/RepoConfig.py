import os
from pyconfigdb.configdb import ConfigDB
import json
from optoboard_felix.driver.log import logger

def construct_config_files():
        logger.info("Loading configuration from configdb...")
        try:
            db = ConfigDB(
                dbKey=os.environ.get("CONFIGDB_KEY"),
                srUrl=os.environ.get("SR_URL"),
                headers={"content-type": "application/json", "accept": "application/json"} 
            )
        except:
            logger.error("Could not connect to the configdb!")
            raise ConnectionError("Could not connect to the configdb!")
        try:  
            runkey_in_db = db.read_all(table="tag",stage=False)
            optoGUIConf = {}
            runkeys_config = {}
            for rk_info in runkey_in_db:
                runkeys_config_curr = {}
                runkey_configGUI_curr = {}
                rk_name = rk_info["name"]
                runkey_payloads = db.search(rk_name, object_type="OB")
                for i in range(len(runkey_payloads)):
                    for j in range(2):
                        if runkey_payloads[i]["payloads"][j]["type"] == "opto_meta.json":
                            runkey_configGUI_curr["OB"+str(i)] =  json.loads(runkey_payloads[i]["payloads"][j]["data"])
                        elif runkey_payloads[i]["payloads"][j]["type"] == "opto_config.json":
                            runkeys_config_curr["OB"+str(i)] = json.loads(runkey_payloads[i]["payloads"][j]["data"])
                        else:
                            logger.error("Payload type not recognized!")
                            raise ValueError("Payload type not recognized!")
                        
                optoGUIConf[rk_name] = runkey_configGUI_curr
                runkeys_config[rk_name] = runkeys_config_curr
            return optoGUIConf, runkeys_config
        except:
            logger.error("The configuration file could not be loaded! The runkey name might be wrong or it doesn't exist in the configdb")
            raise FileNotFoundError("/The runkey name was not found in the configdb!")
