"""
Creates a JSON config file with all writeable registers from register_maps.
Not needed for the user, internal use only.

Run inside additional_driver directory
"""

import json
# import sys
import inspect

# sys.path.append('../')

from optoboard_felix.register_maps.lpgbt_register_map_v1 import LpgbtRegisterMapV1
from optoboard_felix.register_maps.vtrx_register_map_v1_2 import VTRxplusRegisterMapV1_2
# from optoboard_felix.vtrx_register_maps.vtrx_register_map_v1_3 import VTRxplusRegisterMapV1_3
from optoboard_felix.register_maps.gbcr2_register_map import GBCRRegisterMap

lpgbt_reg_map = LpgbtRegisterMapV1()     # initialise class

vtrx_reg_map = VTRxplusRegisterMapV1_2()	# quad LD v1.2
# vtrx_reg_map = VTRxplusRegisterMapV1_3()	# quad LD v1.3

gbcr_register_map = GBCRRegisterMap()

first_read_only_lpgbt_register = 0x150

config = {}
config['lpgbt'] = {}

for register in dir(lpgbt_reg_map):

	if any(map(register.__contains__, ['Reg', '__'])):
		continue

	address = getattr(getattr(lpgbt_reg_map, register), 'address')

	if address < first_read_only_lpgbt_register and not 'RESERVED' in register:

		config['lpgbt'][register] = {}

		for setting in inspect.getmembers(getattr(lpgbt_reg_map, register)):

			# print(setting[0])

			if inspect.isclass(setting[1]) and not '__' in setting[0]:
				config['lpgbt'][register][setting[0]] = [0, 0, 0, 0]

config['gbcr'] = {}

for register in dir(gbcr_register_map):

	if any(map(register.__contains__, ['Reg', '__'])):
		continue

	address = getattr(getattr(gbcr_register_map, register), 'address')

	config['gbcr'][register] = {}

	for setting in inspect.getmembers(getattr(gbcr_register_map, register)):

		if inspect.isclass(setting[1]) and not '__' in setting[0]:

			default = getattr(getattr(getattr(gbcr_register_map, register), setting[0]), 'default')

			config['gbcr'][register][setting[0]] = [default, default, default, default]


config['vtrx'] = {}

for register in dir(vtrx_reg_map):

	if any(map(register.__contains__, ['Reg', '__'])):
		continue

	address = getattr(getattr(vtrx_reg_map, register), 'address')

	config['vtrx'][register] = {}

	for setting in inspect.getmembers(getattr(vtrx_reg_map, register)):

		# print(setting[0])

		if inspect.isclass(setting[1]) and not '__' in setting[0]:

			if hasattr(setting[1], 'default'):
				config['vtrx'][register][setting[0]] = getattr(getattr(getattr(vtrx_reg_map, register), setting[0]), 'default')



with open('config.json', 'w') as f:
	json.dump(config, f)
