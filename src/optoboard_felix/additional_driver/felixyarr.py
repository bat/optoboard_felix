"""FELIX YARR module."""

import logging, logging.handlers
import subprocess
import os
from time import sleep

from optoboard_felix.driver.log import logger, LoggerFormat

class Felix:
    """Class allowing to execute basic operations with Felix. 
    
    This class is mostly thought for internal use and it's not required to successfully initialize this object in order to communicate with the Optoboard.

    Note: only for internal developement use at Bern, not recommended for users.
    """
    def __init__(self, FELIXFOLDER):
        """Initialization of the Felix object.

        Args:
            FELIXFOLDER (str): path to the folder containing the Felix software in use

        Attributes:
            FELIXFOLDER (str): path to the folder containing the Felix software in use
        """
        self.FELIXFOLDER = FELIXFOLDER
        if (not os.path.isdir(FELIXFOLDER)):
            raise Exception("Failing initializing the Felix instance")

    @staticmethod
    def felix_diagnosis():
        """
        Check the status of the connection to FELIX.

        Each 3 corresponds to an aligned channel (according to the felix configuration), each 1 to an aligned RD53.

        In case of issues, check the `config_encoding_decoding.sh` file:
        the `LINK` number corresponds to the felix channel, the decoding and encoding egroups are associated to the uplinks and downlinks.

        Set `flx-config set GBT_DATA_RXFORMAT2=0xFFF` in order to set the correct FEC protocol on all the 12 channels (`0xFFF=1111 1111 1111`).
        """
        logger.info("Each 3 corresponds to an aligned channel (according to the felix configuration), each 1 to an aligned rd53.")
        logger.info("In case of issues, check the config_encoding_decoding file: the LINK number corresponds to the felix channel, the decoding and encoding egroups are associated to the uplinks and downlinks.")
        logger.info("In case of issues, check that the correct FEC setting is selected.")
        output = subprocess.Popen(["flx-config", "list"], stdout=subprocess.PIPE).stdout.readlines()
        for line in output:
            line = line.decode()
            if "ALIGNED" in line:
                felix_status = line.split()[3:5]
                logger.info(str(felix_status[0] + " " + felix_status[1]))

        output2 = subprocess.Popen(["flx-info", "link"], stdout=subprocess.PIPE).stdout.readlines()

        print("\n")
        for line in output2:
            line = line.decode()
            if ("Aligned" in line) or ("Channel" in line):
                logger.info(line)

    @staticmethod
    def felix_polarityReboot():
        """
        Reset the polarity of the FELIX channels.
        """
        logger.info("Set the polarity of the felix channels. Can be useful to reach alignment.")
        output = subprocess.Popen(["flx-config", "list"], stdout=subprocess.PIPE).stdout.readlines()
        for line in output:
            line = line.decode()
            if "GBT_RXPOLARITY" in line:
                felix_polarity = line.split()[3:5]
                logger.info(str(felix_polarity[0] + " " + felix_polarity[1]))
        logger.info("Setting all the polarities first to 0 then to 1.")
        subprocess.Popen(["flx-config", "set", "GBT_RXPOLARITY=0x000"])
        subprocess.Popen(["flx-config", "set", "GBT_RXPOLARITY=0xFFF"])
        #subprocess.Popen(["fgpolarity", "-r", "reset"], stdout=subprocess.PIPE)
        #subprocess.Popen(["fgpolarity", "-r", "set"], stdout=subprocess.PIPE)

    def start_felixcore(self):
        """
        Start felixcore with the follwing subprocess call:

        `x86_64-centos7-gcc8-opt/felixcore/felixcore --elinks 0,4,8,12,16,20,24`

        Returns:
            felixProcess (str): subprocess output of felixcore launch
        """
        felixcoreProcessString = [self.FELIXFOLDER + "x86_64-centos7-gcc8-opt/felixcore/felixcore", "--elinks", "0,4,8,12,16,20,24"]
        felixProcess = subprocess.Popen(felixcoreProcessString, stdout=subprocess.PIPE)
        sleep(3)

        return felixProcess

class Yarr:
    """Class allowing to execute basic operations with Yarr. 
    
    This class is mostly thought for internal use and it's not required to successfully initialize this object in order to communicate with the Optoboard.
    """  

    def __init__(self, YARRFOLDER):
        """Initialization of the Yarr object.

        Args:
            YARRFOLDER (str): path to the folder containing the Yarr software in use

        Attributes:
            YARRFOLDER (str): path to the folder containing the Yarr software in use        
        """
        self.YARRFOLDER = YARRFOLDER
        if (not  os.path.isdir(YARRFOLDER)):
            raise Exception("Failing initializing the Yarr instance")
        self.setup_yarr()

    def start_yarrscan(self, chip_type = "single_rd53a"):
        """Function that starts a Yarr scan

        Args:
            chip_type (str): front-end type, options={"single_rd53a", "quad_rd53a", "quad_rd53a"}, default= "single_rd53a"

        Returns:
            yarrProcess: subprocess.Popen object
        """

        if chip_type == "single_rd53a":
            start_yarr = str(self.YARRFOLDER + "build/bin/scanConsole -c " + os.getcwd() + "/" + "configs/example_rd53a_setup.json -r " + self.YARRFOLDER + "configs/controller/felix.json -s " + self.YARRFOLDER + "configs/scans/rd53a/std_digitalscan.json")
        elif chip_type == "quad_rd53a":
            start_yarr = str(self.YARRFOLDER + "build/bin/scanConsole -c " + os.getcwd() + "/" + "configs/Connectivity_file.json -r " + self.YARRFOLDER + "configs/controller/felix.json -s " + self.YARRFOLDER + "configs/scans/rd53a/std_digitalscan.json")
            ##start_yarr = str(self.YARRFOLDER + "build/bin/scanConsole -c " + os.getcwd() + "/" + "configs/quad_rd53a_Setup.json -r " + self.YARRFOLDER + "configs/controller/felix.json -s " + self.YARRFOLDER + "configs/scans/rd53a/std_digitalscan.json")
        elif chip_type == "quad_rd53b":
            start_yarr = str(self.YARRFOLDER + "build/bin/scanConsole -c " + os.getcwd() + "/" + "configs/ITkPix_config/connectivity.json -r " + self.YARRFOLDER + "configs/controller/felix.json -s " + self.YARRFOLDER + "configs/scans/rd53b/std_digitalscan.json")
        else:
            logger.error("%d is an invalid argument", chip_type)
            raise Exception("Invalid argument \"chip_type\"!")
        
        yarrProcess = subprocess.Popen(start_yarr.split())

        return yarrProcess