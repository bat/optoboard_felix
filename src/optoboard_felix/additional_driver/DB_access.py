from optoboard_felix.driver.log import logger

try:
    from requests import post
except(ImportError, ModuleNotFoundError):
    logger.warning("Problem with the import of requests! Some function might not work properly!")

import socket
import json
import os

def configDbUnavailable(CONFIGDB_ADDRESS): 
    """
    Check if the configuration database is available.

    Args:
        CONFIGDB_ADRESS (str): address of the configuration database

    Returns:
        result (bool): False if database unavailable, True if available
    """
    address = CONFIGDB_ADDRESS[:CONFIGDB_ADDRESS.rfind(":")]
    if address.find("://") != -1: 
        address = address[address.find("://")+3:]
    port = CONFIGDB_ADDRESS[CONFIGDB_ADDRESS.rfind(":")+1:]
    a_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    location = (address, int(port))
    result_of_check = a_socket.connect_ex(location)
    if result_of_check == 0:
        return False
    return True

def getConfigDataset(config_path, CONFIGDB_ADDRESS):
    """
    Get a configuration file from the configuration database.

    Args:
        CONFIGDB_ADRESS (str): address of the configuration database

    Returns:
        response (dict): configuration returned
    """
    logger.warning("Fetching dataset from database!")
    bodyName = {'name': config_path, 'table': 'configuration'}
    idResponse = post(CONFIGDB_ADDRESS + "/tag/get", data=json.dumps(bodyName), headers={"content-type": "application/json"}).json()['id']
    bodyTest = {'id': idResponse}
    config_response = post(CONFIGDB_ADDRESS + "/configuration/get", data=json.dumps(bodyTest), headers={"content-type": "application/json"}).json()
    return json.loads(config_response['data'])

def checkAvailableDatasetInDB(config_path, inDB, CONFIGDB_ADDRESS):
    """
    Check if a configuration file is available.

    Args:
        CONFIGDB_ADRESS (str): string with the address of the configuration database

    Returns:
        statuscode (int): 1 if available, 0 else
    """
    if inDB:
        if configDbUnavailable(CONFIGDB_ADDRESS): 
            return 0
        else:
            bodyName = {'name': config_path, 'table': 'configuration'}
            statuscode = post(CONFIGDB_ADDRESS + "/tag/get", data=json.dumps(bodyName), headers={"content-type": "application/json"}).json()['statuscode']  
            if statuscode == 1:
                return 1
            else: 
                return 0      
    else:
        try:
            f = open(os.getcwd() + config_path,'r')
            return 1 
        except:
            return 0
