# pylint: disable=line-too-long, invalid-name, import-error

import argparse
import json
import os
import sys
from IPython import embed
import importlib.resources
import requests
from pyconfigdb.configdb import ConfigDB
from optoboard_felix.additional_driver.RepoConfig import construct_config_files
from collections import OrderedDict

from optoboard_felix.driver.Optoboard import Optoboard
from optoboard_felix.driver.CommWrapper import CommWrapper
from optoboard_felix.driver.log import logger


def main():
    """Initialize an Optoboard List.
    Can be used to initialize a single optoboard from CLI or a list of optoboards using an external configuration file

    Args:
        optoListPath (str): Absolute path to json file with the list of Optoboards and their settings. Contains: config_path, optoboard_serial, vtrx_v, flx_G, flx_d, configure, test_mode, 
        config_path (str): Relative local path or DB path to configuration file, if not provided it takes a local default file depending on the version of the components on the Optoboard.
        optoboard_serial (str): Optoboard serial number
        vtrx_v (str): VTRx+ quad laser driver version (1.2 or 1.3)
        flx_G (int): FELIX link group
        flx_d (int): FELIX device if flx_cid is not provided
        flx_cid (int): FELIX CID
        configure (int): if 1, configure all chips on the Optoboard
        test_mode (int): 0 is false, 1 is true
        commToolName (str): tool to communicate with the Optoboard
        woflxcore (int): for back-compatibility, choose between ic-over-netio (0) and flpgbtconf (1)
        interactive: provide argument -i to start an interactive python console
    Raises:
        ValueError: Optoboard serial is not in driver/components.json
        Exception: local config not available
        Exception: configuration database unavailable
        Exception: could not start felix-core
    """
    parser = argparse.ArgumentParser(
        description="""Initialise Optoboard instances. Either provide a path to a json file containing all the necessary information (example in src/configs/optoConfigList_example.json) 
                        or config path or optoboard_serial + vtrx_v + flx_G + flx_d""")

    parser.add_argument("-P", "--optoListPath", 
                        type=str,
                        default="",
                        help="Absolute path to optoboard list file, used to initialized multiple optoboards in one shot. (example: %(default)s)")
    
    parser.add_argument("-p", "--config_path", 
                        type=str,
                        default="",
                        help="""Absolute path or DB path to configuration file, if not provided it takes a local default file depending on the version of the components on the Optoboard. 
                                When using this argument, make sure to have information like the optoboard_serial in the config file (default: %(default)s)""")

    parser.add_argument("-s", "--optoboard_serial",
                        type=str,
                        help="Optoboard serial number",
                        required=("--config_path" not in sys.argv) and ("--optoListPath" not in sys.argv) and ("--runkey" not in sys.argv))
                        
    parser.add_argument("-v", "--vtrx_v",
                        type=str,
                        default="1.3",
                        choices=["1.2", "1.3"],
                        help="VTRx+ quad laser driver version (1.2 or 1.3)")

    parser.add_argument("-G", "--flx_G",
                        type=int,
                        default=0,
                        choices=[G for G in range(0,24)],
                        help="FELIX link group (default: %(default)s)")

    parser.add_argument("-d", "--flx_d",
                        type=int,
                        default=0,
                        choices=[0, 1, 2, 3],
                        help="FELIX device (default: %(default)s)")
    
    parser.add_argument("--flx_cid",
                        type=int,
                        help="FELIX CID")
    
    parser.add_argument("-c", "--configure",
                        type=int,
                        default=0,
                        choices=[0, 1],
                        help="if 1, configure all chips on the Optoboard (default: %(default)s)")

    parser.add_argument("-D", "--debug",
                        type=int,
                        default=0,
                        choices=[0, 1],
                        help="0 is false, 1 is true; type (default: %(default)s)")
    
    parser.add_argument("-t", "--test_mode",
                        type=int,
                        default=0,
                        choices=[0, 1],
                        help="0 is false, 1 is true (default: %(default)s)")

    

    parser.add_argument("--woflxcore",
                        type=int,
                        default=None,
                        choices=[0, 1],
                        help="communicate with subprocesses, does not rely on felix-core (default: %(default)s)")

    parser.add_argument("-T", "--commToolName",
                        type=str,
                        default=None,
                        choices=["flpgbtconf", "flpgbtconfAPI", "ic-over-netio", "itk-ic-over-netio-next", "lpgbt-com"],
                        help="communicate with subprocesses, does not rely on felix-core (default: %(default)s)")

    
    parser.add_argument("-i", "--interative",
                        action="store_true",
                        help="Activate the iterative python mode")
    
    parser.add_argument("--runkey",
                        type=str,
                        default=None,
                        help="runkey name to get the configuration from the database")

    args = vars(parser.parse_args())
    if args["flx_d"] is None and args["flx_cid"] is None:
        parser.error("Either --flx_d or --flx_cid is required.")


    commToolName = args["commToolName"]
    woflxcore = args["woflxcore"]
    interative = args["interative"]
    flx_cid = args["flx_cid"]
    if flx_cid is None:
        flx_cid = args["flx_d"]


    if args["runkey"] is not None and os.environ.get("OB_USE_CONFIGDB") == "1":
        optoListConfig, runkeys_config = construct_config_files()
        optoListConfig = optoListConfig[args["runkey"]]
        runkeys_config = runkeys_config[args["runkey"]]

    elif args["optoListPath"] == "":
        runkeys_config = None
        optoListConfig = {"OB0": {"config_path" : str(args["config_path"]), 
                            "optoboard_serial" : str(args["optoboard_serial"]), 
                            "vtrx_v" : str(args["vtrx_v"]),
                            "flx_cid" : int(flx_cid),
                            "flx_G" : int(args["flx_G"]),
                            "debug" : bool(int(args["debug"])),
                            "test_mode" : bool(int(args["test_mode"])),
                            "configure" : bool(int(args["configure"])),
                            "commToolName": ""
                            }
                            }
    else:
        runkeys_config = None
        with open(args["optoListPath"]) as f:
            try:
                optoListConfig = json.load(f)
                
                for key, config in optoListConfig.items():          
                    if "vtrx_v" not in config:
                        config["vtrx_v"] = "1.3"

                    if "test_mode" not in config:
                        config["test_mode"] = False

                    if "debug" not in config:
                        config["debug"] = False

            except:
                raise Exception("Unavailable local config list!")
        
    optoList, components_opto, opto = InitOpto(optoListConfig=optoListConfig, commToolName=commToolName, woflxcore=woflxcore, runkeys_config=runkeys_config)
    if interative:  
        logger.info("optoList dictionary available; opto object corresponds to the last initialized optoboard")
        embed()

def InitOpto(optoListConfig, commToolName = None, woflxcore = None, runkeys_config = None):
    """
    Function to initialize a list of Optoboards.
    The optoListConfig should have the shape 
    {"OB0": {"config_path" : str(args["config_path"]), 
                            "optoboard_serial" : str(args["optoboard_serial"]), 
                            "vtrx_v" : str(args["vtrx_v"]),
                            "flx_d" : int(args["flx_d"]) if "flx_cid" not in args,
                            "flx_cid" : int(args["flx_cid"]),
                            "flx_G" : int(args["flx_G"]),
                            "debug" : bool(int(args["debug"])),
                            "test_mode" : bool(int(args["test_mode"])),
                            "configure" : bool(int(args["configure"])),
                            "commToolName": ""
                            }
                            }
    Args:
        optoListConfig (dict): Dictionary with the configuration of the optoboards to be initialized
        commToolName (str): tool to communicate with the Optoboard
        woflxcore (int): for back-compatibility, choose between ic-over-netio and flpgbtconf
    Returns:
        optoList (dict): Dictionary with the initialized optoboards
        components_opto (dict): Dictionary with the components mounted on all the initialized optoboards
        """
    logger.info("Initialising Optoboards")

    optoList = {} 
    components_opto = {}
    try:
        components_path = "../driver/components.json"
        components_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), components_path)
        with open(components_path, 'r') as file:
            components = json.load(file)
    except:
        raise FileNotFoundError("Unavailable local components file!")
    assert isinstance(optoListConfig, dict), "optoListConfig should be a dictionary"
    assert isinstance(list(optoListConfig.keys())[0], str), "optoListConfig keys should be strings"
    entry = list(optoListConfig.values())[0]
    assert isinstance(entry, dict), "optoListConfig values should be dictionaries"
    assert "optoboard_serial" in entry.keys(), "optoListConfig values should have an optoboard_serial key"
    assert "vtrx_v" in entry.keys(), "optoListConfig values should have a vtrx_v key"
    assert "flx_d" in entry.keys() or "flx_cid" in entry.keys(), "optoListConfig values should have a flx_d key or flx_cid key"
    assert "flx_G" in entry.keys(), "optoListConfig values should have a flx_G key"
    if "flx_d" in entry.keys():
        assert int(entry["flx_d"]) in [0, 1, 2, 3], "FELIX device should be 0 or 1"
    if "flx_cid" in entry.keys():
        assert isinstance(entry["flx_cid"],int) or  isinstance(entry["flx_cid"],str), "FELIX CID should be an integer"
    assert int(entry["flx_G"]) in [G for G in range(0,24)], "FELIX link group should be between 0 and 23"
    assert entry["vtrx_v"] in ["1.2", "1.3"], "VTRx+ version should be 1.2 or 1.3"
    assert isinstance(entry["optoboard_serial"], str), "optoboard_serial should be a string"
        
    for optoboard_position in optoListConfig:
        optoConfig = optoListConfig[optoboard_position]
        optoConfig["flx_cid"] = int(optoConfig["flx_cid"]) if "flx_cid" in optoConfig.keys() else int(optoConfig["flx_d"])
        components_opto[optoboard_position] = {}
        if commToolName is None:
            try:
                commToolName = optoConfig["commToolName"]
            except:
                raise Exception("No communication tool provided!")
        logger.info("Using %s for IC communication with optoboard %s", commToolName, optoboard_position)
        # For back-compatibility
        if woflxcore is not None:
            if woflxcore:
                commToolName = "flpgbtconf"
            else:
                commToolName = "ic-over-netio"

        logger.warning("Using %s for IC communication with optoboard %s", commToolName, optoboard_position)


        # Provide the configuration file
        logger.info("Current working directory: %s", os.getcwd())
        if os.environ.get("OB_USE_CONFIGDB") == "1":
            config_file = runkeys_config[str(optoboard_position)]
            config_provided = False
            components_opto[optoboard_position]["components"] = OrderedDict(components[optoConfig["optoboard_serial"]])

        else:
            if optoConfig["config_path"] == "":
                logger.info("No config path provided, using default configuration file")
                config_provided = False
                try:
                    components_opto[optoboard_position]["components"] = OrderedDict(components[optoConfig["optoboard_serial"]])

                except KeyError:
                    logger.error("Provided serial %s is not in the database!", optoConfig["optoboard_serial"])
                    raise ValueError("Invalid serial provided")

                config_path = "../configs/optoboard" + str(components_opto[optoboard_position]["components"]["optoboard_v"]) + "_lpgbtv" + str(components_opto[optoboard_position]["components"]["lpgbt_v"]) + "_gbcr" + str(components_opto[optoboard_position]["components"]["gbcr_v"]) + "_default.json"
                config_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), config_path)
                optoConfig["config_path"] = config_path
            else:
                config_provided = True

            
            with open(optoConfig["config_path"]) as f:
                try:
                    logger.info("Initialising config from %s", optoConfig["config_path"])
                    config_file = json.load(f)
                    
                    

                except:
                    raise FileNotFoundError("Unavailable local configuration file!")
            
        try:
            if config_provided:
                if "flx_cid" not in config_file["Optoboard"].keys() and "flx_d" in config_file["Optoboard"].keys():
                        config_file["Optoboard"]["flx_cid"] = config_file["Optoboard"]["flx_d"]
                if "flx_cid" not in config_file["Optoboard"].keys() and "flx_d" not in config_file["Optoboard"].keys():
                        raise ValueError("FELIX CID not provided")
                components_opto[optoboard_position]["components"] = OrderedDict(components[config_file["Optoboard"]["serial"]])

        
        except KeyError:
            logger.error("Provided serial %s is not in the database!", optoConfig["optoboard_serial"])
            raise ValueError("Invalid serial provided")
        


        # Initialise communication wrapper and Optoboard object
        if config_provided:
            Communication_wrapper = CommWrapper(flx_G=config_file["Optoboard"]["flx_G"], flx_cid=config_file["Optoboard"]["flx_cid"], lpgbt_master_addr=components_opto[optoboard_position]["components"]["lpgbt_master_addr"], lpgbt_v=components_opto[optoboard_position]["components"]["lpgbt_v"], commToolName=commToolName, test_mode=optoConfig["test_mode"])
            opto = Optoboard(config_file, config_file["Optoboard"]["serial"], config_file["Optoboard"]["vtrx_v"], components_opto[optoboard_position]["components"], Communication_wrapper, optoConfig["debug"])
        else:
            Communication_wrapper = CommWrapper(flx_G=optoConfig["flx_G"], flx_cid=optoConfig["flx_cid"], lpgbt_master_addr=components_opto[optoboard_position]["components"]["lpgbt_master_addr"], lpgbt_v=components_opto[optoboard_position]["components"]["lpgbt_v"], commToolName=commToolName, test_mode=optoConfig["test_mode"])
            opto = Optoboard(config_file, optoConfig["optoboard_serial"], optoConfig["vtrx_v"], components_opto[optoboard_position]["components"], Communication_wrapper, optoConfig["debug"])
            
        optoList[optoboard_position] = opto
        logger.info("The optoboard object 'opto' in position %s is now available!", optoboard_position)

    for optoboard_position in optoList:

        try:
            if optoListConfig[optoboard_position]["configure"]:
                logger.info("Starting configuration of Optoboard with serial: %s", optoConfig["optoboard_serial"])
                optoList[optoboard_position].configure()
        except:
            logger.info("configure key was not found for optoboard %s in optoListConfig, it will not be automatically configured", optoConfig["optoboard_serial"])

    return optoList, components_opto, opto

if __name__ == "__main__":
    main()
